﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FormulaTour1600
{
    class RaceResultsController
    {
        /// <summary>
        /// Removes the result from the database
        /// </summary>
        /// <param name="result">RaceResult Entity</param>
        public void DeleteResult(RaceResult result)
        {
            using (var ctx = new FT1600Entities())
            {
                var resultDelete = ctx.RaceResults.Find(result.Id);
                ctx.RaceResults.Remove(resultDelete);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Get the results obtained by the driver
        /// </summary>
        /// <param name="driver">Driver entity</param>
        /// <returns>List of RaceResult Entities</returns>
        public List<RaceResult> GetDriverResults(Driver driver)
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceResults.Where(rr => rr.Driver_Id == driver.Id).ToList();
            }
        }

        public List<RaceResult> GetDriversResults(int year)
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceResults.Where(rr => rr.Race.Season.SeasonYear == year).ToList();
            }
        }

        /// <summary>
        /// Get the drivers' standings
        /// </summary>
        /// <param name="raceClass">RaceClass Entity</param>
        /// <param name="season">Season Entity</param>
        /// <returns>List of Drivers</returns>
        public List<Driver> GetDriversStandings(RaceClass raceClass, Season season)
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceResults
                            .Where(rr => rr.Race.Season.Id == season.Id)
                            .GroupBy(rr => rr.Driver)
                            .Select(rr => rr.Key)
                            .Include(d => d.RaceResults.Select(rr => rr.RacePosition))
                            .ToList();
            }
        }

        /// <summary>
        /// Get the results from a race
        /// </summary>
        /// <param name="race">Race Entity</param>
        /// <returns>List of Results</returns>
        public List<RaceResult> GetResults(Race race)
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceResults.Include(rr => rr.RacePosition)
                                      .Include(rr => rr.Driver.RaceClass)
                                      .Include(rr => rr.Race.RaceSession)
                                      .Where(rr => rr.Race_Id == race.Id).ToList();
            }
        }

        public List<RaceResult> GetResults()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceResults
                            .Include(rr => rr.Driver.RaceClass)
                            .Include(rr => rr.Driver.Engine)
                            .Include(rr => rr.RacePosition)
                            .ToList();
            }
        }

        /// <summary>
        /// Insert a result
        /// </summary>
        /// <param name="result">RaceResult Entity</param>
        public void InsertResult(int raceId, int driverId, int positionId, bool hasPolePosition)
        {
            using (var ctx = new FT1600Entities())
            {
                var newResult = ctx.RaceResults.Create();

                newResult.Race_Id = raceId;
                newResult.Driver_Id = driverId;
                newResult.Position_Id = positionId;
                newResult.HasPolePosition = hasPolePosition;

                ctx.RaceResults.Add(newResult);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Updates a race result
        /// </summary>
        /// <param name="result">RaceResult Entity</param>
        public void UpdateResult(RaceResult result)
        {
            using (var ctx = new FT1600Entities())
            {
                var original = ctx.RaceResults.Find(result.Id);

                ctx.Entry(original).CurrentValues.SetValues(result);

                ctx.SaveChanges();
            }
        }
    }
}
