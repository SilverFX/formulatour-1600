﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class RacesController
    {
        /// <summary>
        /// Return the race corresponding to the event and session
        /// </summary>
        /// <param name="eventId">EventID</param>
        /// <param name="seasonId">SeasonID</param>
        /// <param name="sessionId">RaceSessionID</param>
        /// <returns></returns>
        public Race GetRace(int eventId, int seasonId, int sessionId)
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Races.Where(r => r.Event_Id == eventId && r.Season_Id == seasonId && r.Session_Id == sessionId).SingleOrDefault();
            }
        }

        /// <summary>
        /// Get the list of Races
        /// </summary>
        /// <returns>List of Race Entities</returns>
        public List<Race> GetRaces()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Races
                            .Include(r=> r.RaceEvent)
                            .Include(r=> r.RaceSession)
                            .Distinct()
                            .ToList();
            }
        }

        public Race InsertRace(int eventId, int seasonId, int? sessionId)
        {
            using (var ctx = new FT1600Entities())
            {
                var newRace = ctx.Races.Create();
                newRace.Event_Id = eventId;
                newRace.Session_Id = sessionId.Value;
                newRace.Season_Id = seasonId;

                ctx.Races.Add(newRace);
                ctx.SaveChanges();

                return newRace;
            }
        }
        /// <summary>
        /// Updates the information for the race
        /// </summary>
        /// <param name="race"></param>
        /// <param name="applyToAll"></param>
        public void UpdateRace(Race race, bool applyToAll = false)
        {
            using (var ctx = new FT1600Entities())
            {

                //var original = applyToAll
                //    ? ctx.Races.Where(r => r.Event_Id == race.Event_Id)
                //    : ctx.Races.Where(r => r.Id == race.Id).FirstOrDefault();
                //if (applyToAll)
                //{
                //}

                //original.RaceTrack_Id = race.RaceTrack_Id;
                //original.Date = race.Date;

                //ctx.SaveChanges();
            }
        }
    }
}
