﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class RaceSessionsController
    {
        /// <summary>
        /// Get the sessions
        /// </summary>
        /// <returns>List of RaceSession Entities</returns>
        public List<RaceSession> GetSessions()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceSessions.OrderBy(rs => rs.ShortName).ToList();
            }
        }
    }
}
