﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class RacePositionsController
    {
        /// <summary>
        /// Get all the race positions
        /// </summary>
        /// <returns>List of RacePosition Entities</returns>
        public List<RacePosition> GetAllRacePositions()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RacePositions.OrderBy(rp => rp.FinalPosition.Length).ThenBy(rp => rp.FinalPosition).ToList();
            }
        }

        /// <summary>
        /// Get the list of available positions
        /// </summary>
        /// <param name="race">Race Entity</param>
        /// <returns>List of Position</returns>
        public List<RacePosition> GetAvailablePositionsForRace(Race race)
        {
            using (var ctx = new FT1600Entities())
            {
                var results = ctx.RaceResults
                                    .Where(rr => rr.Race_Id == race.Id)
                                    .Where(rr => !rr.RacePosition.FinalPosition.Equals("DNF"))
                                    .Where(rr => !rr.RacePosition.FinalPosition.Equals("DNS"))
                                    .Where(rr => !rr.RacePosition.FinalPosition.Equals("DSQ"))
                                    .Select(rr => rr.Position_Id).ToList();
                return ctx.RacePositions.Where(rp => !results.Contains(rp.Id)).ToList();
            }
        }

        public void UpdateRacePosition(RacePosition updatedRacePosition)
        {
            using (var ctx = new FT1600Entities())
            {
                var dbRacePosition = ctx.RacePositions.Find(updatedRacePosition.Id);

                if (dbRacePosition != null)
                {
                    ctx.Entry(dbRacePosition).CurrentValues.SetValues(updatedRacePosition);
                    ctx.SaveChanges();
                }
            }
        }
    }
}
