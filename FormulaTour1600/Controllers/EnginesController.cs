﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class EnginesController
    {
        public List<Engine> GetEngines()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Engines.ToList();
            }
        }
    }
}
