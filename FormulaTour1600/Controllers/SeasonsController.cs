﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class SeasonsController
    {
        /// <summary>
        /// Add a season
        /// </summary>
        /// <param name="season"></param>
        public void AddSeason(Season season)
        {
            using (var ctx = new FT1600Entities())
            {
                var newSeason = ctx.Seasons.Create();
                newSeason.Description = season.Description;
                newSeason.SeasonYear = season.SeasonYear;

                ctx.Seasons.Add(newSeason);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Delete a season
        /// </summary>
        /// <param name="season"></param>
        public void DeleteSeason(Season season)
        {
            using (var ctx = new FT1600Entities())
            {
                var deleteSeason = ctx.Seasons.Find(season.Id);
                ctx.Seasons.Remove(deleteSeason);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Get the seasons
        /// </summary>
        /// <returns>List of Season Entities</returns>
        public List<Season> GetSeasons()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Seasons.OrderBy(s => s.SeasonYear).ToList();
            }
        }

        /// <summary>
        /// Update a season
        /// </summary>
        /// <param name="updatedSeason"></param>
        public void UpdateSeason(Season updatedSeason)
        {
            using (var ctx = new FT1600Entities())
            {
                var dbSeason = ctx.Drivers.Find(updatedSeason.Id);

                if (dbSeason != null)
                {
                    ctx.Entry(dbSeason).CurrentValues.SetValues(updatedSeason);
                    ctx.SaveChanges();
                }
            }
        }
    }
}
