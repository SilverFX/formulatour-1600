﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FormulaTour1600
{
    class DriversController
    {

        /// <summary>
        /// Deactivate a driver
        /// </summary>
        /// <remarks>
        /// It is better to deactivate a driver rather than deleting
        /// him since he could comeback to a future event.
        /// </remarks>
        /// <param name="driver"></param>
        public void DeactivateDriver(Driver driver)
        {
            using (var ctx = new FT1600Entities())
            {
                var dbDriver = ctx.Drivers.SingleOrDefault(d => d.Id == driver.Id);

                dbDriver.IsActive = false;

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Get the current registered drivers
        /// </summary>
        /// <returns>List of Driver Entities</returns>
        public List<Driver> GetActiveDrivers()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Drivers
                            .Include(d => d.RaceClass)
                            .Include(d => d.Engine)
                            .Where(d => d.IsActive == true)
                            .ToList();
            }
        }

        /// <summary>
        /// Get the list of drivers not yet assigned to the race
        /// </summary>
        /// <param name="race">Race Entity</param>
        /// <returns>List of Driver Entities</returns>
        public List<Driver> GetAvailableDrivers(Race race)
        {
            using (var ctx = new FT1600Entities())
            {
                var assignedDrivers = ctx.RaceResults.Where(rr => rr.Race_Id == race.Id).Select(rr => rr.Driver_Id).ToList();
                return ctx.Drivers.Where(d => !assignedDrivers.Contains(d.Id)).Where(d => d.IsActive == true).ToList();
            }
        }

        /// <summary>
        /// Get the list of the drivers
        /// </summary>
        /// <returns>List of Driver Entities</returns>
        public List<Driver> GetDrivers()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.Drivers
                          .Include(d => d.RaceClass)
                          .Include(d => d.Engine)
                          .OrderBy(d => d.Number)
                          .ToList();
            }
        }

        /// <summary>
        /// Insert a new driver into the database
        /// </summary>
        /// <param name="newDriver"></param>
        public int InsertDriver(Driver newDriver)
        {
            using (var ctx = new FT1600Entities())
            {
                // Prevent adding other RaceClasses/Engines into the database
                if (newDriver.RaceClass != null) ctx.RaceClasses.Attach(newDriver.RaceClass);
                if (newDriver.Engine != null) ctx.Engines.Attach(newDriver.Engine);
                newDriver.IsActive = true;

                ctx.Drivers.Add(newDriver);

                ctx.SaveChanges();

                return newDriver.Id;
            }
        }
        /// <summary>
        /// Updates the driver
        /// </summary>
        /// <param name="updatedDriver">Driver entity</param>
        public void UpdateDriver(Driver updatedDriver)
        {
            using (var ctx = new FT1600Entities())
            {
                var dbDriver = ctx.Drivers.Find(updatedDriver.Id);

                ctx.Entry(dbDriver).CurrentValues.SetValues(updatedDriver);

                ctx.SaveChanges();
            }
        }
    }
}