﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    class RaceClassesController
    {
        public List<RaceClass> GetRaceClasses()
        {
            using (var ctx = new FT1600Entities())
            {
                return ctx.RaceClasses.OrderBy(rc => rc.Name).ToList();
            }
        }
    }
}
