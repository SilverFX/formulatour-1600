﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;

namespace FormulaTour1600
{
    class RaceEventsController
    {
        /// <summary>
        /// Add a new event into the database
        /// </summary>
        /// <param name="newEvent">RaceEvent Entity</param>
        public void AddRaceEvent(RaceEvent newEvent)
        {
            using (var ctx = new FT1600Entities())
            {
                ctx.RaceEvents.Add(newEvent);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Remove an event
        /// </summary>
        /// <param name="raceEvent">RaceEvent Entity</param>
        public void DeleteRaceEvent(RaceEvent raceEvent)
        {
            using (var ctx = new FT1600Entities())
            {
                var deleteEvent = ctx.RaceEvents.Find(raceEvent.Id);
                ctx.RaceEvents.Remove(deleteEvent);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Get the race events
        /// </summary>
        /// <returns>List of RaceEvent Entities</returns>
        public BindingList<RaceEvent> GetEvents()
        {
            using (var ctx = new FT1600Entities())
            {
                ctx.RaceEvents.Load();
                return ctx.RaceEvents.Local.ToBindingList();
            }
        }

        /// <summary>
        /// Updates an Event
        /// </summary>
        /// <param name="updatedEvent">RaceEvent Entity</param>
        public void UpdateRaceEvent(RaceEvent updatedEvent)
        {
            using (var ctx = new FT1600Entities())
            {
                var dbRaceEvent = ctx.RaceEvents.Find(updatedEvent.Id);

                ctx.Entry(dbRaceEvent).CurrentValues.SetValues(updatedEvent);
                ctx.SaveChanges();
            }
        }
    }
}
