﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace FormulaTour1600
{
    public class ExcelExport
    {
        #region ----- VARIABLES -----
        private const string DEFAULT_TITLE = "Classement Général";
        private const string NO_RESULT = " - ";
        private const int TITLE_HEADER = 1;
        private const int RACE_HEADER_ROW = 2;
        private const int HEADER_ROW = 3;
        private const int DATA_COL = 6;
        private const int SORT_ROW = 3;
        private const int RESULT_ROW = 6; // NOT USED FOR MAIN PURPOSE
        private Color _eventColor = Color.ForestGreen;
        private Color _raceColor = Color.SteelBlue;

        private string _excelFilename = string.Empty;
        private Excel.Range _xlRange;
        private Excel.Worksheet _wSheet;
        private Excel.Workbook _wBook;
        private Excel.Application _xlApp;
        private object missing = Missing.Value;

        private List<RacePosition> _racePositions = new List<RacePosition>();
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public ExcelExport()
        {
            _xlApp = new Excel.Application
            {
                Visible = false,
                DisplayAlerts = false
            };
            _wBook = _xlApp.Workbooks.Add(missing);

            // Used to simulate the PRO AM standing behavior
            _racePositions = new RacePositionsController().GetAllRacePositions();
        }

        #region ------------------------- NOT USED -------------------------
        /// <summary>
        /// Create the Excel Report for the Results
        /// </summary>
        /// <param name="filename">Name of the file</param>
        /// <param name="dctDataResults">Data Dictionary</param>
        public void CreateResult(string filename, Dictionary<RaceResult, List<RaceResult>> dctDataResults, int year, bool allChecked)
        {
            // Clear existing worksheets
            for (int i = _wBook.Worksheets.Count; i > 1; i--)
            {
                _wBook.Worksheets[i].Delete();
            }

            int sheetCount = 0;

            foreach (KeyValuePair<RaceResult, List<RaceResult>> pair in dctDataResults)
            {
                int resultRow = RESULT_ROW;
                sheetCount++;
                _wSheet = (sheetCount > 1)
                    ? _wBook.Worksheets.Add(After: _wBook.Sheets[_wBook.Sheets.Count])
                    : _wBook.Worksheets[sheetCount];

                //_wSheet.Name = pair.Key.ShortEventName.ToString();
                //_wSheet.Tab.Color = (pair.Key.RaceType.ToLower() == "course")
                //    ? Color.DarkSeaGreen
                //    : Color.Yellow;

                #region --- CREATE HEADERS ---
                _wSheet.Cells[1, 2] = pair.Key.ToString();
                SetFontSize((Excel.Range)_wSheet.Cells[1, 2], 18);
                //_wSheet.Cells[2, 2] = pair.Key.RaceType;


                _wSheet.Cells[4, 2] = "< Nom du circuit >";
                _wSheet.Cells[5, 2] = "< Date >";
                SetBold((Excel.Range)_wSheet.Cells[5, 2], false);
                SetItalic((Excel.Range)_wSheet.Cells[5, 2], true);

                _wSheet.Cells[7, 1] = "Pos.";
                _wSheet.Cells[7, 2] = "No.";
                SetHAlignment((Excel.Range)_wSheet.Cells[7, 2], Excel.XlHAlign.xlHAlignRight);
                _wSheet.Cells[7, 3] = "Pilote";
                _wSheet.Cells[7, 4] = "Provenance";
                _wSheet.Cells[7, 5] = "Voiture";

                //if (pair.Key.RaceType.ToLower() == "course")
                //{
                //    _wSheet.Cells[7, 6] = "Meilleur tour";
                //    _wSheet.Cells[7, 7] = "Points";
                //}
                //else
                //{
                //    _wSheet.Cells[7, 7] = "Meilleur tour";
                //}

                _xlRange = _wSheet.Range[_wSheet.Cells[1, 7], _wSheet.Cells[7, 7]];
                SetBold(_xlRange.EntireRow, true);
                SetHAlignment(_xlRange, Excel.XlHAlign.xlHAlignRight);
                SetItalic(_xlRange.EntireRow, false);
                SetItalic((Excel.Range)_wSheet.Cells[2, 2], true);
                SetBold((Excel.Range)_wSheet.Cells[2, 2], false);
                #endregion

                foreach (RaceResult result in pair.Value)
                {
                    int rowIndex = resultRow + 2;

                    _wSheet.Cells[rowIndex, 1] = result.RacePosition.FinalPosition;
                    SetBold((Excel.Range)_wSheet.Cells[rowIndex, 1], true);
                    SetHAlignment((Excel.Range)_wSheet.Cells[rowIndex, 1], Excel.XlHAlign.xlHAlignCenter);

                    _wSheet.Cells[rowIndex, 2] = result.Driver.Number;
                    SetHAlignment((Excel.Range)_wSheet.Cells[rowIndex, 2], Excel.XlHAlign.xlHAlignRight);
                    _wSheet.Cells[rowIndex, 3] = result.Driver.Name;
                    _wSheet.Cells[rowIndex, 4] = result.Driver.Hometown;
                    _wSheet.Cells[rowIndex, 5] = result.Driver.Car;
                    SetHAlignment((Excel.Range)_wSheet.Cells[rowIndex, 6], Excel.XlHAlign.xlHAlignCenter);

                    //if (pair.Key.RaceType.ToLower() == "course")
                    //{
                    //    _wSheet.Cells[rowIndex, 6] = result.BestLap;
                    //    _wSheet.Cells[rowIndex, 7] = result.Points.ToString();
                    //    SetBold((Excel.Range)_wSheet.Cells[rowIndex, 7], true);
                    //}
                    //else
                    //{
                    //    _wSheet.Cells[rowIndex, 7] = result.BestLap;
                    //}

                    SetHAlignment((Excel.Range)_wSheet.Cells[rowIndex, 7], Excel.XlHAlign.xlHAlignCenter);
                    _xlRange = _wSheet.Range[_wSheet.Cells[rowIndex, 1], _wSheet.Cells[rowIndex, 7]];
                    _xlRange.Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone;

                    // AutoFit of specific columns
                    for (int columnIndex = 1; columnIndex < 8; columnIndex++)
                    {
                        switch (columnIndex)
                        {
                            case 1:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                ((Excel.Range)_wSheet.Cells[rowIndex, columnIndex]).EntireColumn.AutoFit();
                                break;
                            default:
                                break;
                        }
                    }

                    if (resultRow % 2 == 0)
                    {
                        _xlRange.Interior.Color = Color.LightGray;
                    }
                    else
                    {
                        _xlRange.Interior.ColorIndex = 0;
                    }

                    resultRow++;
                }
            }

            try
            {
                // Only create standing if all races have been checked
                if (allChecked)
                {
                    // Create standing sheets
                    CreateStandings(filename, dctDataResults.Keys.ToList(), year);
                    CreateStandings(filename, dctDataResults.Keys.ToList(), year, "Classement Général PRO-AM");
                }
                _wBook.Worksheets[1].Select();

                // Save the file
                _wBook.SaveAs(
                    filename,
                    Excel.XlFileFormat.xlWorkbookNormal,
                    missing,
                    missing,
                    missing,
                    missing,
                    Excel.XlSaveAsAccessMode.xlNoChange,
                    missing,
                    missing,
                    missing,
                    missing,
                    missing
                );

                Console.WriteLine("EXCEL EXPORT DONE!");
                _xlApp.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                ReleaseObject(_wSheet);
                ReleaseObject(_wBook);
                ReleaseObject(_xlApp);
            }
        }
        #endregion

        /// <summary>
        /// Creates the standing sheet
        /// </summary>
        /// <param name="filename">FileName of the Workbook</param>
        /// <param name="races">List of the Races</param>
        /// <param name="results">All the results</param>
        /// <param name="drivers">All drivers that are present in the results table</param>
        public void CreateStandingsFile(string filename, List<Race> races, List<RaceResult> results, List<Driver> drivers)
        {
            Excel.Range summaryRange, sheetRange;

            // Clear existing worksheets
            for (int i = _wBook.Worksheets.Count; i > 1; i--)
            {
                _wBook.Worksheets[i].Delete();
            }

            //
            // TODO : Change the sheet count to dynamic
            //
            int sheetCount = 2;

            for (int sheet = 1; sheet <= sheetCount; sheet++)
            {
                _wSheet = (sheet > 1)
                   ? _wBook.Worksheets.Add(After: _wBook.Sheets[_wBook.Sheets.Count])
                   : _wBook.Worksheets[sheet];

                _wSheet.Name = (sheet == 1) ? "CLASSEMENT GÉNÉRAL" : "CLASSEMENT PRO AM";
                _wSheet.Tab.Color = (sheet == 1) ? Color.LimeGreen : Color.Yellow;

                #region ----- STATIC HEADERS -----
                //
                // Main title
                //
                _wSheet.Cells[TITLE_HEADER, 1] = (sheet == 1) ? "CLASSEMENT GÉNÉRAL / STANDING" : "CLASSEMENT GÉNÉRAL / STANDING PRO AM (KENT)";

                //
                // Static Headers
                //
                const int POSITION_COL = 1;
                Excel.Range positionRange = _wSheet.Cells[HEADER_ROW, POSITION_COL];
                _wSheet.Cells[HEADER_ROW, POSITION_COL] = "Pos.";
                SetColor(_wSheet.Cells[HEADER_ROW, POSITION_COL], _raceColor);
                SetFontColor(_wSheet.Cells[HEADER_ROW, POSITION_COL], Color.White);
                SetHAlignment((Excel.Range)_wSheet.Cells[HEADER_ROW, POSITION_COL], Excel.XlHAlign.xlHAlignCenter);

                const int NUMBER_COL = 2;
                _wSheet.Cells[HEADER_ROW, NUMBER_COL] = "No.";
                SetColor(_wSheet.Cells[HEADER_ROW, NUMBER_COL], _raceColor);
                SetFontColor(_wSheet.Cells[HEADER_ROW, NUMBER_COL], Color.White);
                SetHAlignment((Excel.Range)_wSheet.Cells[HEADER_ROW, NUMBER_COL], Excel.XlHAlign.xlHAlignCenter);

                const int NAME_COL = 3;
                _wSheet.Cells[HEADER_ROW, NAME_COL] = "Nom";
                SetColor(_wSheet.Cells[HEADER_ROW, NAME_COL], _raceColor);
                SetFontColor(_wSheet.Cells[HEADER_ROW, NAME_COL], Color.White);

                const int CHASSIS_COL = 4;
                _wSheet.Cells[HEADER_ROW, CHASSIS_COL] = "Chassis/Engine";
                SetColor(_wSheet.Cells[HEADER_ROW, CHASSIS_COL], _raceColor);
                SetFontColor(_wSheet.Cells[HEADER_ROW, CHASSIS_COL], Color.White);

                const int CLASS_COL = 5;
                _wSheet.Cells[HEADER_ROW, CLASS_COL] = "Classe";
                SetColor(_wSheet.Cells[HEADER_ROW, CLASS_COL], _raceColor);
                SetFontColor(_wSheet.Cells[HEADER_ROW, CLASS_COL], Color.White);
                #endregion

                #region ----- DRIVERS LAYOUT -----
                //
                // Row and col number assignments
                //
                int drvRow = HEADER_ROW + 1;
                int drvCount = 1;
                int summaryCol = 0;

                //
                // Iterate through the Drivers List
                //
                var filteredDrivers = (sheet == 1) ? drivers : drivers.Where(d => d.RaceClass.Name == "PRO / PRO AM");
                foreach (Driver driver in filteredDrivers)
                {
                    //
                    // Position Column
                    //
                    int drvTotal = 0;
                    int column = DATA_COL;
                    sheetRange = _wSheet.Cells[drvRow, POSITION_COL];
                    sheetRange.Value2 = drvCount;

                    SetBold(sheetRange, true);
                    SetColor(sheetRange, Color.LightGray);
                    SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignRight);
                    sheetRange.EntireColumn.AutoFit();

                    //
                    // Number Column
                    //
                    sheetRange = _wSheet.Cells[drvRow, NUMBER_COL];
                    sheetRange.Value2 = driver.Number;
                    sheetRange.EntireColumn.AutoFit();

                    //
                    // Name Column
                    //
                    sheetRange = _wSheet.Cells[drvRow, NAME_COL];
                    sheetRange.Value2 = driver.Name;
                    sheetRange.EntireColumn.AutoFit();

                    //
                    // Chassis Column
                    //
                    sheetRange = _wSheet.Cells[drvRow, CHASSIS_COL];
                    sheetRange.Value2 = (driver.Engine != null) ? string.Concat(driver.Car, " / ", driver.Engine.Name) : driver.Car;
                    sheetRange.EntireColumn.AutoFit();

                    //
                    // Class Column
                    //
                    sheetRange = _wSheet.Cells[drvRow, CLASS_COL];
                    sheetRange.Value2 = driver.RaceClass.Name;
                    sheetRange.EntireColumn.AutoFit();

                    #region ----- RACE HEADERS AND VALUES -----
                    foreach (Race race in races)
                    {
                        var filteredResults = (sheet == 1)
                             ? results
                             : results.Where(r => r.Race_Id == race.Id)
                                         .Where(r => r.Driver.RaceClass.Name.Equals("PRO / PRO AM"))
                                         .OrderBy(r => r.RacePosition.FinalPosition.Length)
                                         .ThenBy(r => r.RacePosition.FinalPosition)
                                         .ToList();

                        sheetRange = ((Excel.Range)_wSheet.Cells[HEADER_ROW, column]);
                        _wSheet.Cells[RACE_HEADER_ROW, column] = race.RaceEvent.Name;
                        SetColor((Excel.Range)_wSheet.Cells[RACE_HEADER_ROW, column], Color.Lime);
                        SetItalic((Excel.Range)_wSheet.Cells[RACE_HEADER_ROW, column], true);

                        //
                        // Single Race Sessions
                        //
                        _wSheet.Cells[HEADER_ROW, column] = race.RaceSession.ShortName;
                        SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignCenter);
                        SetBold(sheetRange, true);
                        SetFontSize(sheetRange, 10);
                        SetColor(sheetRange, _raceColor);
                        SetFontColor(sheetRange, Color.White);


                        RaceResult result = (from aResult in filteredResults
                                             where driver.Id == aResult.Driver_Id
                                             && race.Id == aResult.Race_Id
                                             select aResult).SingleOrDefault();

                        sheetRange = _wSheet.Cells[drvRow, column];
                        if (result == null)
                        {
                            sheetRange.Value2 = NO_RESULT;
                            SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignRight);
                        }
                        else
                        {
                            int score = 0;

                            if (result.HasPolePosition.Value)
                            {
                                SetColor(sheetRange, Color.Yellow);

                                sheetRange.Value2 = (sheet == 1)
                                    ? (result.RacePosition.Points + 1)
                                    : _racePositions.Where(rp => rp.FinalPosition == (filteredResults.IndexOf(result) + 1).ToString()).Select(rp => rp.Points).SingleOrDefault() + 1;
                                score = (sheet == 1)
                                    ? (result.RacePosition.Points + 1)
                                    : _racePositions.Where(rp => rp.FinalPosition == (filteredResults.IndexOf(result) + 1).ToString()).Select(rp => rp.Points).SingleOrDefault() + 1;
                            }
                            else
                            {
                                if (sheet == 1)
                                {
                                    sheetRange.Value2 = (result.RacePosition.Points == 0) ? result.RacePosition.FinalPosition : result.RacePosition.Points.ToString();
                                    score = result.RacePosition.Points;
                                }
                                else
                                {
                                    sheetRange.Value2 = (result.RacePosition.Points == 0)
                                        ? (result.RacePosition.FinalPosition)
                                        : _racePositions.Where(rp => rp.FinalPosition == (filteredResults.IndexOf(result) + 1).ToString()).Select(rp => rp.Points.ToString()).SingleOrDefault();
                                    score = (result.RacePosition.Points == 0)
                                        ? result.RacePosition.Points
                                        : Int32.Parse(_racePositions.Where(rp => rp.FinalPosition == (filteredResults.IndexOf(result) + 1).ToString()).Select(rp => rp.Points.ToString()).SingleOrDefault());
                                }

                                string value = sheetRange.Value2.ToString();
                                switch (value)
                                {
                                    case "DNS":
                                    case "DNQ":
                                    case "DSQ":
                                    case NO_RESULT:
                                        SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignCenter);
                                        SetItalic(sheetRange, true);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            drvTotal += score;
                        }

                        sheetRange.EntireColumn.AutoFit();
                        column++;
                    }
                    #endregion

                    _wSheet.Cells[HEADER_ROW, column] = "TOTAL";
                    sheetRange = (Excel.Range)_wSheet.Cells[HEADER_ROW, column];
                    SetBold(sheetRange, true);
                    SetColor(sheetRange, _raceColor);
                    SetFontColor(sheetRange, Color.White);

                    summaryRange = _wSheet.Cells[drvRow, column];
                    summaryRange.NumberFormat = "@";
                    summaryRange.Value2 = drvTotal;
                    SetBold(summaryRange, true);
                    SetHAlignment(summaryRange, Excel.XlHAlign.xlHAlignCenter);
                    summaryRange.EntireColumn.AutoFit();

                    drvRow++;
                    drvCount++;
                    summaryCol = column;
                    #endregion
                }

                #region ----- MERGE HEADERS -----
                //
                // Variables declarations for the header merges
                //
                string previousText = string.Empty;
                Excel.Range startRange = _wSheet.Cells[RACE_HEADER_ROW, DATA_COL]; // initialize first start range
                Excel.Range endRange;

                //
                // Check the entire row and merge/fit accordingly
                //
                for (int index = DATA_COL; index <= summaryCol; index++)
                {
                    string currentText = _wSheet.Cells[RACE_HEADER_ROW, index].Value2;
                    previousText = _wSheet.Cells[RACE_HEADER_ROW, index - 1].Value2;

                    if ((string.IsNullOrEmpty(currentText)) || (!currentText.Equals(previousText) && !string.IsNullOrEmpty(previousText)))
                    {
                        endRange = _wSheet.Cells[RACE_HEADER_ROW, index - 1];
                        var mergeRange = _wSheet.Range[startRange, endRange];
                        mergeRange.Merge();
                        SetHAlignment(mergeRange, Excel.XlHAlign.xlHAlignCenter);
                        SetColor(mergeRange, _eventColor);
                        SetItalic(mergeRange, true);
                        startRange = _wSheet.Cells[RACE_HEADER_ROW, index];
                    }
                }

                //
                // Merge the title
                //
                startRange = _wSheet.Cells[TITLE_HEADER, 1];
                endRange = _wSheet.Cells[TITLE_HEADER, summaryCol];
                var mergeTitleRange = _wSheet.Range[startRange, endRange];
                mergeTitleRange.Merge();
                SetBold(mergeTitleRange, true);
                SetHAlignment(mergeTitleRange, Excel.XlHAlign.xlHAlignCenter);
                SetItalic(mergeTitleRange, true);
                SetFontSize(mergeTitleRange, 14);

                // Refit the position column
                positionRange.EntireColumn.AutoFit();

                #endregion

                #region ----- ASSIGN BORDERS ------
                //
                // Assign the same border for every used cells
                //
                Excel.Range range = _wSheet.UsedRange;
                Excel.Range cell = range.Cells;
                Excel.Borders border = cell.Borders;

                border.LineStyle = Excel.XlLineStyle.xlContinuous;
                border.Weight = Excel.XlBorderWeight.xlThin;
                #endregion

                #region ----- SORTING DATA -----
                dynamic sortRng = _wSheet.Range[_wSheet.Cells[SORT_ROW, 2], _wSheet.Cells[drvRow - 1, summaryCol]];

                sortRng.Sort(
                    sortRng.Columns[summaryCol - 1, Type.Missing],
                    Excel.XlSortOrder.xlDescending, // Key 1
                    missing,
                    missing,
                    Excel.XlSortOrder.xlDescending, // Key 2
                    missing,
                    Excel.XlSortOrder.xlDescending, // Key 3
                    Excel.XlYesNoGuess.xlGuess,
                    missing,
                    missing,
                    Excel.XlSortOrientation.xlSortColumns,
                    Excel.XlSortMethod.xlPinYin,
                    Excel.XlSortDataOption.xlSortNormal,
                    Excel.XlSortDataOption.xlSortNormal,
                    Excel.XlSortDataOption.xlSortNormal
                );
                #endregion
            }
            #region ----- GENERATE EXCEL FILE -----
            try
            {
                _wBook.Worksheets[1].Select();

                // Save the file
                _wBook.SaveAs(
                    filename,
                    Excel.XlFileFormat.xlWorkbookNormal,
                    missing,
                    missing,
                    missing,
                    missing,
                    Excel.XlSaveAsAccessMode.xlNoChange,
                    missing,
                    missing,
                    missing,
                    missing,
                    missing
                );

                Console.WriteLine("EXCEL EXPORT DONE!");
                _xlApp.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                ReleaseObject(_wSheet);
                ReleaseObject(_wBook);
                ReleaseObject(_xlApp);
            }
            #endregion
        }
        #region ------------------------------------------ NOT USED FOR NOW ------------------------------------------
        /// <summary>
        /// Creates the Standings Sheet
        /// </summary>
        /// <param name="filename">Name of the WorkBook</param>
        /// <param name="argResultsList">List of Results</param>
        /// <param name="year">Season Year</param>
        /// <param name="sheetName">Sheet name</param>
        private void CreateStandings(string filename, List<RaceResult> argResultsList, int year, string sheetName = DEFAULT_TITLE)
        {
            int summaryCol = 0;
            int classCol = 0;

            Excel.Range summaryRange, classRange;

            _wSheet = _wBook.Worksheets.Add(After: _wBook.Sheets[_wBook.Sheets.Count]);
            _wSheet.Name = sheetName;
            _wSheet.Tab.Color = Color.DimGray;

            DriversController driversController = new DriversController();
            RaceResultsController raceResultsController = new RaceResultsController();

            List<Driver> drvList = driversController.GetDrivers();

            // TODO : Match drivers from driverList
            // that are in Results
            List<Driver> driverList = driversController.GetDrivers();
            //List<RaceResult> resultList = raceResultsController.GetResults();

            //var test = from driver in driverList
            //           join result in resultList on driver.Id equals result.DriverId
            //           group result.DriverId by driver.Id into g
            //           select new Driver() as { Id = g.Key, DriverName =  };

            List<RaceResult> allResults = raceResultsController.GetDriversResults(year);

            #region --- CREATE HEADERS ---
            _wSheet.Range["A5"].Value2 = "Pos.";
            _wSheet.Range["A5"].Columns.AutoFit();
            SetColor(_wSheet.Range["A5"], Color.Blue);
            SetFontColor(_wSheet.Range["A5"], Color.White);

            _wSheet.Range["B2"].Value2 = string.Format("{0} - {1}", sheetName, year);
            _wSheet.Range["B2"].Font.Size = 18;

            _wSheet.Range["B3"].Value2 = "en date du :";
            _wSheet.Range["B3"].Font.Size = 14;

            _wSheet.Range["B4"].Value2 = DateTime.Today.ToLongDateString();

            _wSheet.Range["B5"].Value2 = "No.";
            SetColor(_wSheet.Range["B5"], Color.Blue);
            SetFontColor(_wSheet.Range["B5"], Color.White);
            SetHAlignment(_wSheet.Range["B5"], Excel.XlHAlign.xlHAlignRight);

            _wSheet.Range["C5"].Value2 = "Pilote";

            SetFontSize(_wSheet.get_Range("A5", "C5"), 12);
            SetBold(_wSheet.get_Range("A1", "C5"), true);
            SetBold(_wSheet.Range["B3"], false);
            SetItalic(_wSheet.Range["B3"], true);

            int lastCol = 4;
            Excel.Range sheetRange;

            //
            // HINT : It is the layout for the races
            //
            for (int i = 0; i < argResultsList.Count; i++)
            {
                sheetRange = ((Excel.Range)_wSheet.Cells[5, lastCol]);
                sheetRange.Orientation = 75;
                SetBold(sheetRange, true);
                SetFontSize(sheetRange, 12);
                //sheetRange.Value2 = argResultsList[i].FullName;

                Excel.Borders border = sheetRange.Borders;
                border.LineStyle = Excel.XlLineStyle.xlContinuous;
                border.Weight = Excel.XlBorderWeight.xlHairline;

                sheetRange.Columns.AutoFit();

                // Starting Cell = D5 ( row = 5 / column = 4 )
                // This creates the header using the name of the Events
                lastCol++;
            }

            sheetRange = _wSheet.Cells[5, lastCol];
            sheetRange.Value2 = "Classe";
            SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignCenter);
            SetBold(sheetRange, true);
            SetFontSize(sheetRange, 12);

            classCol = lastCol;
            summaryCol = lastCol + 1;
            sheetRange = _wSheet.Cells[5, summaryCol];
            sheetRange.Value2 = "Points";
            SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignCenter);
            SetBold(sheetRange, true);
            SetFontSize(sheetRange, 12);
            #endregion

            #region --- CREATE DRIVER LAYOUT ---
            int row = 6;

            int drvCount = 1;

            IEnumerable<Driver> filteredDriverList = drvList;
            if (sheetName != DEFAULT_TITLE)
            {
                filteredDriverList = drvList.Where(drv => drv.RaceClass.Name.ToUpper() != "PRO");
            }

            foreach (Driver driver in filteredDriverList)
            {
                int drvTotal = 0;
                sheetRange = _wSheet.Cells[row, 1];
                sheetRange.Value2 = drvCount;
                SetBold(sheetRange, true);
                SetHAlignment(sheetRange, Excel.XlHAlign.xlHAlignCenter);

                sheetRange = _wSheet.Cells[row, 2];
                sheetRange.Value2 = driver.Number;

                sheetRange = _wSheet.Cells[row, 3];
                sheetRange.Value2 = driver.Name;
                sheetRange.EntireColumn.AutoFit();

                // Set values corresponding to the driver and event
                int col = 4;
                foreach (RaceResult result in argResultsList)
                {
                    RaceResult resultValue = (from oneResult in allResults
                                              where oneResult.Driver.Id.Equals(driver.Id) && oneResult.Race.Id.Equals(result.Race.Id)
                                              select oneResult).FirstOrDefault();

                    sheetRange = _wSheet.Cells[row, col];
                    if (resultValue == null)
                    {
                        sheetRange.Value2 = " - ";
                    }
                    else
                    {
                        drvTotal += resultValue.RacePosition.Points;
                        if (resultValue.RacePosition.Points != 0)
                        {
                            SetBold(sheetRange, true);
                            SetColor(sheetRange, Color.Yellow);
                            sheetRange.Value2 = resultValue.RacePosition.Points.ToString();
                        }
                        else
                        {
                            SetColor(sheetRange, Color.Black);
                            sheetRange.Value2 = "";
                        }
                        sheetRange.Value2 = resultValue.RacePosition.Points.ToString();
                    }

                    sheetRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    Excel.Borders border = sheetRange.Borders;
                    border.LineStyle = Excel.XlLineStyle.xlContinuous;
                    border.Weight = Excel.XlBorderWeight.xlThin;

                    col++;
                }

                classRange = _wSheet.Cells[row, classCol];
                classRange.Value2 = driver.RaceClass;
                SetItalic(classRange, true);
                classRange.EntireColumn.AutoFit();

                summaryRange = _wSheet.Cells[row, summaryCol];
                summaryRange.NumberFormat = "@";
                summaryRange.Value2 = drvTotal;
                SetBold(summaryRange, true);
                SetHAlignment(summaryRange, Excel.XlHAlign.xlHAlignCenter);

                row++;
                drvCount++;
            }

            #endregion

            #region --- SORTING ---
            dynamic colorSortRng = _wSheet.Range[_wSheet.Cells[SORT_ROW, 1], _wSheet.Cells[row - 1, summaryCol]];
            dynamic sortRng = _wSheet.Range[_wSheet.Cells[SORT_ROW, 2], _wSheet.Cells[row - 1, summaryCol]];

            sortRng.Sort(
                sortRng.Columns[summaryCol - 1], Excel.XlSortOrder.xlDescending, // Key 1
                missing, missing, Excel.XlSortOrder.xlAscending, // Key 2
                missing, Excel.XlSortOrder.xlAscending, // Key 3
                Excel.XlYesNoGuess.xlGuess, missing, missing,
                Excel.XlSortOrientation.xlSortColumns,
                Excel.XlSortMethod.xlPinYin,
                Excel.XlSortDataOption.xlSortNormal,
                Excel.XlSortDataOption.xlSortNormal,
                Excel.XlSortDataOption.xlSortNormal
            );

            foreach (Excel.Range cell in colorSortRng.Cells)
            {
                if (cell.Row % 2 == 0)
                {
                    if (cell.Interior.ColorIndex != 6)
                    {
                        SetColor(cell, Color.LightGray);
                    }
                }
                else
                {
                    if (cell.Interior.ColorIndex != 6)
                    {
                        SetColor(cell, Color.Empty);
                    }
                }
            }
            #endregion
        }
        #endregion
        /// <summary>
        /// Set/Unset bold
        /// </summary>
        /// <param name="rng">Excel Range</param>
        /// <param name="yesNo">true or false</param>
        private void SetBold(Excel.Range rng, bool yesNo)
        {
            rng.Font.Bold = yesNo;
        }

        /// <summary>
        /// Set the color for the Excel Range
        /// </summary>
        /// <param name="rng">Range to apply the color</param>
        private void SetColor(Excel.Range rng, Color color)
        {
            if (color != Color.Empty)
            {
                rng.Interior.Color = color;
            }
            else
            {
                rng.Interior.ColorIndex = 0;
            }
        }

        private void SetFontColor(Excel.Range rng, Color color)
        {
            if (color != Color.Empty)
            {
                rng.Font.Color = color;
            }
            else
            {
                rng.Font.Color.ColorIndex = 0;
            }
        }

        /// <summary>
        /// Set/Unset Italic
        /// </summary>
        /// <param name="rng">Excel Range</param>
        /// <param name="yesNo">true or false</param>
        private void SetItalic(Excel.Range rng, bool yesNo)
        {
            rng.Font.Italic = yesNo;
        }

        /// <summary>
        /// Set the Font to the specified Font Size
        /// </summary>
        /// <param name="rng">Excel Range</param>
        /// <param name="fontSize">Font Size</param>
        private void SetFontSize(Excel.Range rng, int fontSize)
        {
            rng.Font.Size = fontSize;
        }

        /// <summary>
        /// Set the range to the specified horizontal alignment
        /// </summary>
        /// <param name="rng"></param>
        /// <param name="horizontal"></param>
        private void SetHAlignment(Excel.Range rng, Excel.XlHAlign horizontal)
        {
            rng.HorizontalAlignment = horizontal;
        }

        /// <summary>
        /// Release the object from memory
        /// </summary>
        /// <param name="obj">Object to remove</param>
        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
