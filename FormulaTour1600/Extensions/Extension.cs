﻿using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public class Extension
    {
        private static Bitmap _defaultImg = Properties.Resources.NoPhoto;

        /// <summary>
        /// Custom method to open a new form
        /// </summary>
        /// <param name="frm"></param>
        public static void OpenForm(Form frm)
        {
            frm.Hide();
            frm.ShowDialog();
            frm.Close();
        }

        /// <summary>
        /// Determine if the photo is the same
        /// </summary>
        /// <param name="photo">First photo</param>
        /// <param name="bmp2">Second photo</param>
        /// <returns></returns>
        public static bool CompareWithDefault(Bitmap photo)
        {
            bool result = true;

            //Test to see if we have the same size of image
            if (photo.Size != _defaultImg.Size)
            {
                //   cr = CompareResult.ciSizeMismatch;
                result = false;
            }
            else
            {
                //Convert each image to a byte array
                ImageConverter ic = new ImageConverter();

                byte[] btImage1 = new byte[1];
                btImage1 = (byte[])ic.ConvertTo(photo, btImage1.GetType());

                byte[] btImage2 = new byte[1];
                btImage2 = (byte[])ic.ConvertTo(_defaultImg, btImage2.GetType());

                //Compute a hash for each image
                SHA256Managed shaM = new SHA256Managed();
                byte[] hash1 = shaM.ComputeHash(btImage1);
                byte[] hash2 = shaM.ComputeHash(btImage2);

                //Compare the hash values
                for (int i = 0; i < hash1.Length && i < hash2.Length && result; i++)
                {
                    if (hash1[i] != hash2[i])
                    {
                        result = false;
                    }
                }
            }

            return result;
        }
        /// <summary>
        /// Check if the file is locked
        /// </summary>
        /// <param name="file">Filename</param>
        /// <returns>True or false</returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Check for a number
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int? ToNullableInt(string str)
        {
            int i;
            if (int.TryParse(str, out i)) return i;
            return null;
        }
    }
}
