﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public static class DatabaseAccess
    {
        /// <summary>
        /// Get a DataTable
        /// </summary>
        /// <param name="query">SQL Query</param>
        /// <param name="parameters">Indeterminate number of parameters</param>
        /// <returns>DataTable</returns>
        public static DataTable GetDataTable(string query, params SqlParameter[] parameters)
        {
            DataTable table = new DataTable();

            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        table.Load(reader);
                    }
                }
            }

            return table;
        }

        /// <summary>
        /// Apply changes (insert)
        /// </summary>
        /// <param name="query">SQL Query (string)</param>
        /// <param name="result">Output parameter (int) -1 is for updates/deletes</param>
        /// <param name="parameters">Array of SqlParameters</param>
        public static void ApplyChanges(string query, out int result, params SqlParameter[] parameters)
        {
            result = 0;

            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }

                    try
                    {
                        result = (int)cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        Console.WriteLine(ex.StackTrace);
                    }
                }
            }
        }

        /// <summary>
        /// Apply Update/Delete
        /// </summary>
        /// <param name="query">SQL Query</param>
        /// <param name="parameters">Array of SqlParameters</param>
        public static void ApplyChanges(string query, params SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Get the connection string
        /// </summary>
        /// <returns>Connection String</returns>
        private static string GetConnectionString()
        {
            string connString = string.Empty;
            var allConnStrings = ConfigurationManager.ConnectionStrings;

            foreach (ConnectionStringSettings css in allConnStrings)
            {
                connString = css.ConnectionString.Contains("FT1600.mdf") ? css.ConnectionString : null;

                // MS Access Database (old one)
                // connString = css.ConnectionString.Contains("FormulaTour.accdb") ? css.ConnectionString : null;
            }
            return connString;
        }
    }
}