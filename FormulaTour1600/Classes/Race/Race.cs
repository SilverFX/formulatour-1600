﻿using System;

namespace FormulaTour1600
{
    public class Race
    {
        public int Id { get; set; }
        public RaceEvent Event { get; set; }
        public Session Session { get; set; }
        public Season Season { get; set; }
        public Track Track { get; set; }
        public DateTime? EventDate { get; set; }

        /// <summary>
        /// Constructor with nested object initializer
        /// </summary>
        public Race()
        {
            Event = new RaceEvent();
            Session = new Session();
            Season = new Season();
            Track = new Track();
        }

        /// <summary>
        /// Return a copy of the instance
        /// </summary>
        /// <returns>Race Obj</returns>
        public Race GetCopy()
        {
            return (Race)MemberwiseClone();
        }
    }
}