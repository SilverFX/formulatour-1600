﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FormulaTour1600
{
    public class RaceAccess
    {
        /// <summary>
        /// Get the race object corresponding to 
        /// the event id and session id match
        /// </summary>
        /// <param name="eventId">Event Id (from ComboBox Event)</param>
        /// <param name="sessionId">Session Id (from ComboBox Session)</param>
        /// <returns>Race object</returns>
        public Race GetRace(int eventId, int seasonId, int sessionId)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            Race newRace = new Race();

            string query =
                @"SELECT Id, TrackId, Date
                  FROM Races
                  WHERE EventId = @eventId
                  AND SeasonId = @seasonId
                  AND SessionId = @sessionId";

            paramList.Add(new SqlParameter("@eventId", eventId));
            paramList.Add(new SqlParameter("@seasonId", seasonId));
            paramList.Add(new SqlParameter("@sessionId", sessionId));

            using (DataTable table = DatabaseAccess.GetDataTable(query, paramList.ToArray()))
            {
                if (table.Rows.Count > 0)
                {
                    DataRow row = table.Rows[0];

                    newRace.Id = (int)row["Id"];
                    newRace.Event.Id = eventId;
                    newRace.Season.Id = seasonId;
                    newRace.Session.Id = sessionId;
                    newRace.Track.Id = !row.IsNull("TrackId") ? (int)row["TrackId"] : 0;
                    newRace.EventDate = !row.IsNull("Date") ? (DateTime?)row["Date"] : null;
                }
            }

            return newRace;
        }

        /// <summary>
        /// Obtain the list of races
        /// </summary>
        /// <returns>List of Race</returns>
        public List<Race> GetRaces()
        {
            {
                List<Race> racesList = new List<Race>();

                string query =
                    @"SELECT DISTINCT
                        Races.Id as RaceID,
                        RaceEvents.Id as EventID,
                        RaceEvents.Name,
                        Sessions.Id as SessionID,
                        Sessions.Code
                    FROM Races
	                    LEFT JOIN RaceEvents ON Races.EventId = RaceEvents.Id
	                    LEFT JOIN Sessions ON Races.SessionId = Sessions.Id;";

                using (DataTable table = DatabaseAccess.GetDataTable(query))
                {
                    foreach (DataRow row in table.Rows)
                    {
                        Race newRace = new Race()
                        {
                            Id = (int)row["RaceID"],
                            Event =
                            {
                                Id = (int)row["EventID"],
                                Name = row["Name"].ToString(),
                            },
                            Session =
                            {
                                Id = (int)row["SessionID"],
                                Code = row["Code"].ToString()
                            }
                        };

                        racesList.Add(newRace);
                    }
                }
                return racesList;
            }
        }

        /// <summary>
        /// Inserts a new race into the table
        /// </summary>
        /// <param name="eventId">Event id (from ComboBox Event)</param>
        /// <param name="seasonId">Season Id (from ComboBox Season)</param>
        /// <param name="sessionId">Session id (from ComboBox Session)</param>
        /// <param name="result">Last Inserted ID</param>
        public void InsertRace(int eventId, int seasonId, int? sessionId, out int result)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"INSERT INTO Races (
                    EventId,
                    SessionId,
                    SeasonId
                )
                OUTPUT INSERTED.ID
                VALUES ( 
                    @eventId,
                    @sessionId,
                    @seasonId
                );";

            paramList.Add(new SqlParameter("@eventId", eventId));
            paramList.Add(new SqlParameter("@sessionId", sessionId));
            paramList.Add(new SqlParameter("@seasonId", seasonId));

            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }

        /// <summary>
        /// Updates the informations for the race
        /// </summary>
        /// <param name="race">Race obj</param>
        public void UpdateRace(Race race, bool applyToAll = false)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            StringBuilder query = new StringBuilder(
                @"UPDATE Races
                SET
                    TrackId = @TrackId,
                    Date =  @RaceDate"
                );

            paramList.Add(new SqlParameter("@TrackId", race.Track.Id));
            paramList.Add(new SqlParameter("@RaceDate", race.EventDate));

            if (applyToAll)
            {
                query.Append(" WHERE EventId = @EventId");
                paramList.Add(new SqlParameter("@EventId", race.Event.Id));
            }
            else
            {
                query.Append(" WHERE Id = @Id");
                paramList.Add(new SqlParameter("@Id", race.Id));
            }

            int result;
            DatabaseAccess.ApplyChanges(query.ToString(), out result, paramList.ToArray());
        }
    }
}