﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public class DriverDetailsAccess
    {
        public List<DriverDetail> GetDriverDetails(Driver driver)
        {
            List<DriverDetail> detailsList = new List<DriverDetail>();

            string query =
                @"SELECT 
	                EventName,
	                ShortName,
                    LongName,
	                FinalPosition,
                FROM (
	                SELECT 
		                RaceEvents.Name as EventName,
		                RaceSessions.ShortName,
                        RaceSessions.LongName,
		                RacePositions.FinalPosition,
		                RacePositions.Points,
	                FROM RaceResults
		                INNER JOIN Races ON RaceResults.RaceId = Races.Id
		                INNER JOIN RacePositions ON RaceResults.PositionId = RacePositions.Id
		                INNER JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
		                INNER JOIN RaceEvents ON Races.EventId = RaceEvents.Id
	                WHERE RaceResults.DriverId = @driverId
                ) As GetResults
                ORDER BY EventName, ShortName, LongName";

            using (DataTable table = DatabaseAccess.GetDataTable(query, new SqlParameter("@driverId", driver.Id)))
            {
                foreach (DataRow row in table.Rows)
                {
                    DriverDetail newDetails = new DriverDetail();

                    newDetails.EventName = row["EventName"].ToString();
                    newDetails.ShortSessionName = row["ShortName"].ToString();
                    newDetails.Position = row["FinalPosition"].ToString();
                    newDetails.Points = (int)row["TotalPoints"];

                    detailsList.Add(newDetails);
                }
            }

            return detailsList;
        }
    }
}
