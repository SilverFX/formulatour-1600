﻿namespace FormulaTour1600
{
    public class DriverDetail
    {
        public string EventName { get; set; }
        public string ShortSessionName { get; set; }
        public string LongSessionName { get; set; }
        public string Position { get; set; }
        public int Points { get; set; }
    }
}
