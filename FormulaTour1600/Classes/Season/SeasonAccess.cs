﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public class SeasonAccess
    {
        /// <summary>
        /// Get all seasons
        /// </summary>
        /// <returns>List of Seasons</returns>
        public List<Season> GetSeasons()
        {
            List<Season> seasonsList = new List<Season>();

            string query = 
                @"SELECT 
                    Id,
                    SeasonYear,
                    Description
                FROM Seasons
                ORDER BY SeasonYear;";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Season newSeason = new Season();

                    newSeason.Id = (int)row["Id"];
                    newSeason.SeasonYear = (int)row["SeasonYear"];
                    newSeason.Description = row["Description"].ToString();

                    seasonsList.Add(newSeason);
                }
            }

            return seasonsList;
        }

        /// <summary>
        /// Add a new season into the database
        /// </summary>
        /// <param name="season">Season object</param>
        public void AddSeason(Season season)
        {
            int result;

            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"INSERT INTO Seasons (
                    SeasonYear,
                    Description
                  )
                  OUTPUT INSERTED.Id
                  VALUES ( 
                    @year,
                    @description
                  );";

            paramList.Add(new SqlParameter("@year", season.SeasonYear));
            paramList.Add(new SqlParameter("@description", season.Description));

            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }

        /// <summary>
        /// Deletes the season from the database
        /// </summary>
        /// <param name="season">Season object</param>
        public void DeleteSeason(Season season)
        {
            string query = "DELETE FROM Seasons WHERE Id = @id";

            DatabaseAccess.ApplyChanges(query, new SqlParameter("@id", season.Id));
        }

        /// <summary>
        /// Updates the season
        /// </summary>
        /// <param name="season">Season object</param>
        public void UpdateSeason(Season season)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"UPDATE Seasons
                  SET SeasonYear = @year,
                      Description = @description
                  WHERE Id = @id";

            paramList.Add(new SqlParameter("@year", season.SeasonYear));
            paramList.Add(new SqlParameter("@description", season.Description));
            paramList.Add(new SqlParameter("@id", season.Id));

            int result;
            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }
    }
}
