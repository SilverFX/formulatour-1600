﻿namespace FormulaTour1600
{
    public class Season
    {
        public int Id { get; set; }
        public int SeasonYear { get; set; }
        public string Description { get; set; }
    }
}
