﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public class PositionAccess
    {
        /// <summary>
        /// Get the list of positions
        /// </summary>
        /// <returns>List of RacePosition</returns>
        public List<Position> GetAllRacePositions()
        {
            List<Position> positionsList = new List<Position>();

            string query = "SELECT Code, Points FROM Positions ORDER BY len(Code), Code";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Position newPosition = new Position()
                    {
                        Code = row["Code"].ToString(),
                        Points = (int)row["Points"]
                    };

                    positionsList.Add(newPosition);
                }
            }
            return positionsList;
        }

        /// <summary>
        /// Display only the unused position of the race 
        /// so that the position won't overlap
        /// </summary>
        /// <param name="raceId">RaceID of the currently selected race</param>
        /// <returns>Unused RacePositions for the specified Race</returns>
        public List<Position> GetAvailablePositions(int raceId)
        {
            List<Position> positionsList = new List<Position>();

            string query =
                    @"SELECT Code, Points 
                        FROM Positions
                        WHERE Code NOT IN
                        (
                            SELECT PositionCode
                            FROM Results
                            WHERE RaceId = @raceId
	                        AND NOT Code = 'DNS'
	                        AND NOT Code = 'DNF'
	                        AND NOT Code = 'DSQ'
                        )
                    ORDER BY len(Code), Code";


            using (DataTable table = DatabaseAccess.GetDataTable(query, new SqlParameter("@raceId", raceId)))
            {
                foreach (DataRow row in table.Rows)
                {
                    Position newPosition = new Position()
                    {
                        Code = row["Code"].ToString(),
                        Points = (int)row["Points"]
                    };

                    positionsList.Add(newPosition);
                }
            }

            return positionsList;
        }

        /// <summary>
        /// Updates the RacePosition score
        /// </summary>
        /// <param name="position">Position object</param>
        public void UpdateRacePosition(Position position)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"UPDATE RacePositions
                SET Points = @points
                WHERE Code = @Code";

            paramList.Add(new SqlParameter("@points", position.Points));
            paramList.Add(new SqlParameter("@code", position.Code));

            int result;
            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }
    }
}
