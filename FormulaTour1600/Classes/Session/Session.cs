﻿namespace FormulaTour1600
{
    public class Session
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
    }
}
