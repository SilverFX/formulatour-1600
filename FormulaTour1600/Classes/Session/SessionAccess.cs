﻿using System.Collections.Generic;
using System.Data;

namespace FormulaTour1600
{
    public class RaceSessionAccess
    {
        /// <summary>
        /// Get the list of the race sessions
        /// </summary>
        /// <returns></returns>
        public List<Session> GetSessions()
        {
            List<Session> sessionsList = new List<Session>();

            string query =
                @"SELECT 
                    Id,
                    Code,
                    Description,
                    CONCAT('[',Code,'] - ',Description) as 'Title'
                FROM Sessions
                ORDER BY Code";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Session newSession = new Session()
                    {
                        Id = (int)row["Id"],
                        Code = row["Code"].ToString(),
                        Description = row["Description"].ToString(),
                        Title = row["Title"].ToString()
                    };

                    sessionsList.Add(newSession);
                }
            }
            return sessionsList;
        }
    }
}
