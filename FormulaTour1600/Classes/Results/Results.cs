﻿using System;

namespace FormulaTour1600
{
    public class Result
    {

        public int Id { get; set; }
        public Race Race { get; set; }
        public Driver Driver { get; set; }
        public Position Position { get; set; }
        public bool HasPolePosition { get; set; }

        /// <summary>
        /// Constructor with nested object initializer
        /// </summary>
        public Result()
        {
            Race = new Race();
            Driver = new Driver();
            Position = new Position();
        }

        /// <summary>
        /// Return a copy of the object
        /// </summary>
        /// <returns>Result obj</returns>
        public Result GetCopy()
        {
            return (Result)MemberwiseClone();
        }
    }
}
