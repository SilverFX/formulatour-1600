﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FormulaTour1600
{
    public class ResultAccess
    {
        /// <summary>
        /// Get RaceResults
        /// </summary>
        /// <returns>List of RaceResults</returns>
        public List<Result> GetResults()
        {
            List<Result> resultList = new List<Result>();

            string query =
                @"SELECT 
	                Results.Id as ResultID,
	                RaceId,
                    RaceEvents.Id as EventID,
	                RaceEvents.Name as EventName,
	                Sessions.Code as Session,
	                DriverId,
	                PositionCode,
                    Points,
	                HasPolePosition
                FROM Results
	                LEFT JOIN Races ON RaceId = Races.Id
	                LEFT JOIN RaceEvents ON Races.EventId = RaceEvents.Id
	                LEFT JOIN Sessions ON Races.SessionId = Sessions.Id
                    LEFT JOIN Positions ON Positions.Code = PositionCode
                ORDER BY RaceId, len(PositionCode), PositionCode";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Result newResult = new Result()
                    {
                        Id = (int)row["ResultID"],
                        Race =
                        {
                            Id = (int)row["RaceId"],
                            Event =
                            {
                                Id = (int)row["EventID"],
                                Name = row["EventName"].ToString(),
                            }
                        },
                        Driver =
                        {
                            Id = (int)row["DriverId"]
                        },
                        Position =
                        {
                            Code = row["PositionCode"].ToString(),
                            Points = (int)row["Points"]
                        },
                        HasPolePosition = (bool)row["HasPolePosition"]
                    };

                    resultList.Add(newResult);
                }
            }
            return resultList;
        }

        /// <summary>
        /// Get the list of race results for the race
        /// </summary>
        /// <param name="raceId">ID of the Race</param>
        /// <returns>List of RaceResults</returns>
        public List<Result> GetResults(int? raceId = null)
        {
            List<Result> resultList = new List<Result>();

            List<SqlParameter> paramList = new List<SqlParameter>();

            StringBuilder query =
                new StringBuilder(
                    @"SELECT 
                        Results.Id,
                        RaceId,
                        PositionCode,
                        DriverId,
                        Drivers.Number,
                        Drivers.Name,
                        Hometown,
                        ClassId,
                        RaceClasses.Name as ClassName,
                        Car,
                        Points,
                        HasPolePosition,
                        Total = Points + CAST(HasPolePosition as INT)
                    FROM Results
                        LEFT JOIN Drivers ON Results.DriverId = Drivers.Id
                        LEFT JOIN RaceClasses ON RaceClasses.Id = Drivers.ClassId
                        LEFT JOIN Positions ON Results.PositionCode = Positions.Code"
                );

            if (raceId.HasValue)
            {
                query.Append(" WHERE RaceId = @raceId");
                paramList.Add(new SqlParameter("@raceId", raceId));
            }

            query.Append(" ORDER BY len(Positions.Code), Positions.Code;");

            using (DataTable table = DatabaseAccess.GetDataTable(query.ToString(), paramList.ToArray()))
            {
                foreach (DataRow row in table.Rows)
                {
                    Result newResult = new Result()
                    {
                        Id = (int)row["Id"],
                        Race =
                        {
                            Id = (int)row["RaceId"]
                        },
                        Driver =
                        {
                            Id = (int)row["DriverId"],
                            Number = row["Number"].ToString(),
                            Name = row["Name"].ToString(),
                            Hometown = row["Hometown"].ToString(),
                            RaceClass = { Id = (int)row["ClassId"], Name = row["ClassName"].ToString() },
                            Car = row["Car"].ToString(),
                            Points = (int)row["Total"]
                        },
                        Position =
                        {
                            Code = row["PositionCode"].ToString(),
                            Points = (int)row["Points"]
                        },
                        HasPolePosition = (bool)row["HasPolePosition"]
                    };

                    resultList.Add(newResult);
                }
            }
            return resultList;
        }

        public List<Result> GetDriversResults(int year)
        {
            List<Result> driverResultsList = new List<Result>();

            List<SqlParameter> parameters = new List<SqlParameter>();

            string query =
                @"SELECT 
                    DriverID,
                    DriverName,
                    RaceID,
                    CONCAT(EventName, ' - ', ShortName) as FullName,
                    ShortEventName,
                    LongName,
                    TypeName,
                    RacePoints =
                    CASE TypeName
                        WHEN 'COURSE' THEN Points
                        ELSE
                        CASE
                            WHEN FinalPosition = '1' THEN 1
                            ELSE 0
                        END
                    END
                FROM(
                    SELECT
                        Drivers.Id as DriverID,
                        Drivers.Name as DriverName,
                        Races.Id as RaceID,
                        RaceEvents.Name as EventName,
                        RaceEvents.ShortName as ShortEventName,
                        RaceSessions.ShortName,
                        RaceSessions.LongName,
                        RacePositions.FinalPosition,
                        RacePositions.Points,
                        RaceTypes.Name as TypeName
                    FROM RaceResults
                        LEFT JOIN Drivers ON Drivers.Id = RaceResults.DriverId
                        LEFT JOIN Races ON RaceResults.RaceId = Races.Id
                        LEFT JOIN RacePositions ON RaceResults.PositionId = RacePositions.Id
                        LEFT JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
                        LEFT JOIN RaceEvents ON Races.EventId = RaceEvents.Id
                        LEFT JOIN RaceTypes ON RaceSessions.RaceTypeId = RaceTypes.Id
                        LEFT JOIN Seasons ON Seasons.Id = Races.SeasonId
                    WHERE Seasons.SeasonYear = @seasonYear
                    ) As GetResults
                    ORDER BY DriverID, EventName, ShortEventName, LongName";

            parameters.Add(new SqlParameter("@seasonYear", year));

            using (DataTable table = DatabaseAccess.GetDataTable(query, parameters.ToArray()))
            {
                foreach (DataRow row in table.Rows)
                {
                    Result newResult = new Result();

                    //newResult.DriverId = (int)row["DriverID"];
                    //newResult.DriverName = row["DriverName"].ToString();
                    //newResult.FullName = row["FullName"].ToString();
                    //newResult.ShortEventName = row["ShortEventName"].ToString();
                    //newResult.RaceId = (int)row["RaceID"];
                    //newResult.Points = (int)row["RacePoints"];
                    //newResult.RaceType = row["TypeName"].ToString();

                    driverResultsList.Add(newResult);
                }
            }

            return driverResultsList;
        }

        public List<Result> GetSeasonsResults(int year)
        {
            List<Result> resultsList = new List<Result>();

            string query =
                @"SELECT DISTINCT
                    Results.Id as ResultID,
                    Races.Id as RaceID,
	                CONCAT(RaceEvents.Name, ' - ', Sessions.Code) as FullName,
                    CONCAT(RaceEvents.ShortName, ' - ', Sessions.Code) as ShortName
                FROM Results
                    LEFT JOIN Races ON Results.RaceId = Races.Id
                    LEFT JOIN RaceEvents ON Races.EventId = RaceEvents.Id
                    LEFT JOIN Sessions ON Races.SessionId = Sessions.Id
                    LEFT JOIN Seasons ON Races.SeasonId = Seasons.Id
                WHERE Seasons.SeasonYear = @season
                GROUP BY Races.Id, Results.Id, RaceEvents.Name, RaceEvents.ShortName, Seasons.SeasonYear, Sessions.Code
                ORDER BY FullName";

            using (DataTable table = DatabaseAccess.GetDataTable(query, new SqlParameter("@season", year)))
            {
                foreach (DataRow row in table.Rows)
                {
                    Result newResult = new Result()
                    {
                        Id = (int)row["ResultID"],
                        Race =
                        {
                            Id = (int)row["RaceID"],
                            Event =
                            {
                                Name = row["FullName"].ToString(),
                                ShortName = row["ShortName"].ToString()
                            }
                        }
                    };
                    //newResult.RaceId = (int)row["Id"];
                    //newResult.FullName = row["FullName"].ToString();
                    //newResult.ShortEventName = row["ShortName"].ToString();
                    //newResult.RaceType = row["RaceType"].ToString();

                    resultsList.Add(newResult);
                }
            }

            return resultsList;
        }
        /// <summary>
        /// Deletes the Race Result
        /// </summary>
        /// <param name="id">Id of RaceResult</param>
        public void DeleteResult(Result result)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"DELETE FROM Results
                  WHERE Id = @Id";

            DatabaseAccess.ApplyChanges(query, new SqlParameter("@Id", result.Id));
        }

        /// <summary>
        /// Insert a Result
        /// </summary>
        /// <param name="result">Result obj</param>
        public void InsertResult(Result result)
        {
            int insertedId;

            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"INSERT INTO Results (
                    RaceId,
                    DriverId,
                    PositionCode,
                    HasPolePosition
                )
                OUTPUT INSERTED.Id
                VALUES (
                    @raceId,
                    @driverId,
                    @positionId,
                    @hasPolePosition
                );";

            paramList.Add(new SqlParameter("@raceId", result.Race.Id));
            paramList.Add(new SqlParameter("@driverId", result.Driver.Id));
            paramList.Add(new SqlParameter("@positionId", result.Position.Code));
            paramList.Add(new SqlParameter("@hasPolePosition", result.HasPolePosition));

            DatabaseAccess.ApplyChanges(query, out insertedId, paramList.ToArray());
        }

        /// <summary>
        /// Update a RaceResult
        /// </summary>
        /// <param name="result">RaceResult Obj</param>
        public void UpdateResult(Result result)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"UPDATE Results
                SET
                    DriverId = @driverId,
                    PositionCode = @positionCode,
                    HasPolePosition = @hasPolePosition
                WHERE Id = @Id";

            paramList.Add(new SqlParameter("@driverId", result.Driver.Id));
            paramList.Add(new SqlParameter("@positionCode", result.Position.Code));
            paramList.Add(new SqlParameter("@hasPolePosition", result.HasPolePosition));
            paramList.Add(new SqlParameter("@Id", result.Id));

            DatabaseAccess.ApplyChanges(query, paramList.ToArray());
        }

        /// <summary>
        /// Updates/Inserts the RaceResult for the generation
        /// </summary>
        /// <param name="raceId">ID of Race</param>
        /// <param name="driverId">ID of Driver</param>
        /// <param name="positionId">ID of Position</param>
        /// <param name="bestLap">The BestLap</param>
        /// <param name="rowsAffected">Number of rows affected</param>
        public void UpdateOrInsertRaceResult(int raceId, int driverId, int positionId, string bestLap, out int rowsAffected)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            string query =
                @"IF EXISTS (SELECT * FROM RaceResults WHERE RaceId = @raceId AND DriverId = @driverId)
                    UPDATE RaceResults
                    SET PositionId = @positionId,
                        BestLap = @bestLap
                    WHERE RaceId = @raceId AND DriverId = @driverId
                ELSE
                    INSERT INTO RaceResults (
                        RaceId, 
                        DriverId,
                        PositionId,
                        BestLap
                    )
                    VALUES (
                        @raceId,
                        @driverId,
                        @positionId,
                        @bestLap
                    )";

            paramList.Add(new SqlParameter("@raceId", raceId));
            paramList.Add(new SqlParameter("@driverId", driverId));
            paramList.Add(new SqlParameter("@positionId", positionId));
            paramList.Add(new SqlParameter("@bestLap", bestLap));

            DatabaseAccess.ApplyChanges(query, out rowsAffected, paramList.ToArray());
        }

        /// <summary>
        /// Get the Results for a race
        /// </summary>
        /// <param name="raceId">ID of the race</param>
        /// <returns>List of results</returns>
        public List<Result> GetResultsB(int? raceId = null)
        {
            List<Result> resultList = new List<Result>();

            List<SqlParameter> paramList = new List<SqlParameter>();

            StringBuilder query =
                new StringBuilder(
                    @"SELECT 
                        RaceResults.Id,
                        RaceId,
                        PositionId,
                        FinalPosition,
                        DriverId,
                        DriverNumber,
                        DriverName,
                        RaceClasses.Id as ClassId,
	                    RaceClasses.Name as ClassName,
                        Points,
                        HasPolePosition
                    FROM RaceResults
	                    LEFT JOIN Races ON Races.Id = RaceResults.Id
	                    LEFT JOIN Drivers ON RaceResults.DriverId = Drivers.Id
	                    LEFT JOIN RacePositions ON RaceResults.PositionId = RacePositions.Id
	                    LEFT JOIN RaceClasses ON RaceClasses.Id = Drivers.ClassId"
                );

            if (raceId.HasValue)
            {
                query.Append(" WHERE RaceId = @raceId");
                paramList.Add(new SqlParameter("@raceId", raceId));
            }

            query.Append(" ORDER BY len(RacePositions.FinalPosition), FinalPosition;");

            using (DataTable table = DatabaseAccess.GetDataTable(query.ToString(), paramList.ToArray()))
            {
                if (table.Rows.Count > 0)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        Result newResult = new Result()
                        {
                            Id = (int)row["Id"],
                            Race = new Race()
                            {
                                Id = (int)row["RaceId"]
                            },
                            Driver = new Driver()
                            {
                                Id = (int)row["DriverId"],
                                Number = row["DriverNumber"].ToString(),
                                Name = row["DriverName"].ToString(),
                                RaceClass = new RaceClass()
                                {
                                    Id = (int)row["ClassId"],
                                    Name = row["ClassName"].ToString(),
                                },
                            },
                            Position = new Position()
                            {
                                //Code = (int)row["PositionId"],
                                //Points = row["FinalPosition"].ToString(),
                                Points = (int)row["Points"]
                            },
                            HasPolePosition = (bool)row["HasPolePosition"],
                        };


                        resultList.Add(newResult);
                    }
                }
            }

            return resultList;
        }
    }
}
