﻿namespace FormulaTour1600
{
    public class RaceEvent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        /// <summary>
        /// Return a copy of the object
        /// </summary>
        /// <returns>RaceEvent</returns>
        public RaceEvent GetCopy()
        {
            return (RaceEvent)MemberwiseClone();
        }
    }
}
