﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public class RaceEventAccess
    {
        /// <summary>
        /// Get the events from the list
        /// </summary>
        /// <returns>List of Race Events</returns>
        public List<RaceEvent> GetEvents()
        {
            List<RaceEvent> eventsList = new List<RaceEvent>();

            string query = "SELECT Id, Name FROM RaceEvents";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    RaceEvent newRaceEvent = new RaceEvent();

                    newRaceEvent.Id = (int)row["Id"];
                    newRaceEvent.Name = row["Name"].ToString();

                    eventsList.Add(newRaceEvent);
                }
            }

            return eventsList;
        }

        /// <summary>
        /// Add a RaceEvent
        /// </summary>
        /// <param name="newEvent">RaceEvent Obj</param>
        public void AddRaceEvent(RaceEvent newEvent)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            int result;

            string query = "INSERT INTO RaceEvents (Name) VALUES (@Name);";

            paramList.Add(new SqlParameter("@Name", newEvent.Name));
            paramList.Add(new SqlParameter("@ShortName", newEvent.ShortName));

            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }

        /// <summary>
        /// Deletes a RaceEvent from the database
        /// </summary>
        /// <param name="track">RaceEvent Obj</param>
        public void DeleteRaceEvent(RaceEvent raceEvent)
        {
            string query = "DELETE FROM RaceEvents WHERE Id = @Id";

            DatabaseAccess.ApplyChanges(query, new SqlParameter("@Id", raceEvent.Id));
        }

        /// <summary>
        /// Updates a racetrack
        /// </summary>
        /// <param name="track">RaceTrack Obj</param>
        public void UpdateRaceEvent(RaceEvent raceEvent)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            int result;

            string query =
                @"UPDATE RaceEvents
                SET Name = @Name,
                WHERE Id = @Id";

            paramList.Add(new SqlParameter("@Name", raceEvent.Name));
            paramList.Add(new SqlParameter("@Name", raceEvent.ShortName));
            paramList.Add(new SqlParameter("@Id", raceEvent.Id));

            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }
    }
}
