﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FormulaTour1600
{
    public class StandingsAccess
    {
        /// <summary>
        /// Get the list of all amateurs drivers only
        /// </summary>
        /// <returns></returns>
        public List<Driver> GetDriversStandings(string raceClass, int season)
        {
            List<Driver> driverList = new List<Driver>();

            List<SqlParameter> parameters = new List<SqlParameter>();

            StringBuilder query =
                new StringBuilder(
                    @"SELECT 
	                    Standings.Id,
	                    DriverNumber,
	                    DriverName,
	                    SUM(TOTAL) AS 'GrandTotal'
                    FROM
                    (
	                    SELECT
		                    Drivers.Id,
		                    DriverNumber,
		                    DriverName,
		                    SeasonYear,
                            ClassId,
		                    SUM(POINTS) as 'TOTAL'
	                    FROM Positions
		                    LEFT JOIN Results ON Results.PositionCode = Positions.Code
		                    LEFT JOIN Drivers ON Results.DriverId = Drivers.Id
		                    LEFT JOIN Races ON Results.RaceId = Races.Id
		                    LEFT JOIN Sessions ON Races.SessionId = Sessions.Id
		                    LEFT JOIN Seasons ON Races.SeasonId = Seasons.Id
	                    GROUP BY 
                            Drivers.Id,
                            DriverNumber,
                            DriverName,
                            ClassId,
                            SeasonYear
                    ) AS Standings
                    LEFT JOIN RaceClasses ON RaceClasses.Id = Standings.ClassId"
                );

            // Only filter the standings by classes when the class is set as PRO / PRO AM
            if (raceClass == "PRO / PRO AM")
            {
                query.Append(@" WHERE RaceClasses.Name = @raceClass AND SeasonYear = @seasonYear");
                parameters.Add(new SqlParameter("@raceClass", raceClass));
            }
            else
            {
                query.Append(@" WHERE SeasonYear = @seasonYear");
            }

            parameters.Add(new SqlParameter("@seasonYear", season));

            query.Append(
                @" GROUP BY 
                    Standings.Id, 
                    DriverName, 
                    DriverNumber, 
                    SeasonYear
                HAVING SUM(TOTAL) IS NOT NULL
                ORDER BY 'GrandTotal' DESC;"
            );

            using (DataTable table = DatabaseAccess.GetDataTable(query.ToString(), parameters.ToArray()))
            {
                foreach (DataRow row in table.Rows)
                {
                    Driver newDriver = new Driver();

                    newDriver.Id = (int)row["Id"];
                    newDriver.Number = row["DriverNumber"].ToString();
                    newDriver.Name = row["DriverName"].ToString();
                    newDriver.Points = int.Parse(row["GrandTotal"].ToString());

                    driverList.Add(newDriver);
                }
            }

            return driverList;
        }
    }
}