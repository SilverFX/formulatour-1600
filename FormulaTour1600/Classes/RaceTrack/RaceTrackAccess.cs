﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FormulaTour1600
{
    public class RaceTrackAccess
    {
        /// <summary>
        /// Get all the available tracks
        /// </summary>
        /// <returns>List of Tracks</returns>
        public List<Track> GetRaceTracks()
        {
            List<Track> trackList = new List<Track>();

            string query = @"SELECT Id, Name FROM RaceTracks";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Track newTrack = new Track();

                    newTrack.Id = (int)row["Id"];
                    newTrack.Name = row["Name"].ToString();

                    trackList.Add(newTrack);
                }
            }
            return trackList;
        }

        /// <summary>
        /// Add a RaceTrack
        /// </summary>
        /// <param name="track">RaceTrack Obj</param>
        public void AddRaceTrack(string trackName)
        {
            int result;

            string query = "INSERT INTO RaceTracks (Name) VALUES (@Name);";

            DatabaseAccess.ApplyChanges(query, out result, new SqlParameter("@Name", trackName));
        }

        /// <summary>
        /// Deletes a RaceTrack from the database
        /// </summary>
        /// <param name="track">RaceTrack Obj</param>
        public void DeleteRaceTrack(Track track)
        {
            string query = "DELETE FROM RaceTracks WHERE Id = @Id";

            DatabaseAccess.ApplyChanges(query, new SqlParameter("@Id", track.Id));
        }

        /// <summary>
        /// Updates a racetrack
        /// </summary>
        /// <param name="track">RaceTrack Obj</param>
        public void UpdateRaceTrack(int id, string withName)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            int result;

            string query = "UPDATE RaceTracks SET Name = @Name WHERE Id = @Id";

            paramList.Add(new SqlParameter("@Name", withName));
            paramList.Add(new SqlParameter("@Id", id));

            DatabaseAccess.ApplyChanges(query, out result, paramList.ToArray());
        }
    }
}
