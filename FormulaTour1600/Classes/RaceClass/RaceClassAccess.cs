﻿using System.Data;
using System.Collections.Generic;

namespace FormulaTour1600
{
    public class RaceClassAccess
    {
        /// <summary>
        /// Get Classes
        /// </summary>
        /// <returns>List of Classes</returns>
        public List<RaceClass> GetRaceClasses()
        {
            List<RaceClass> classesList = new List<RaceClass>();

            string query =
                @"SELECT
                    Id,
                    Name
                FROM RaceClasses
                ORDER BY Name";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    RaceClass newRaceClass = new RaceClass()
                    {
                        Id = (int)row["Id"],
                        Name = row["Name"].ToString()
                    };

                    classesList.Add(newRaceClass);
                }
            }

            return classesList;
        }
    }
}