﻿namespace FormulaTour1600
{
    public class Driver
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Hometown { get; set; }
        public string RacingTeam { get; set; }
        public string Car { get; set; }
        public RaceClass RaceClass { get; set; }
        public Engine Engine { get; set; }
        public string ImageLink { get; set; }
        public int Points { get; set; }

        /// <summary>
        /// Constructor with nested object initializer
        /// </summary>
        public Driver()
        {
            RaceClass = new RaceClass();
            Engine = new Engine();
        }

        /// <summary>
        /// Return a copy of the object
        /// </summary>
        /// <returns>Driver obj</returns>
        public Driver GetCopy()
        {
            return (Driver)MemberwiseClone();
        }
    }
}
