﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FormulaTour1600
{
    public class DriverAccess
    {
        /// <summary>
        /// Get the drivers list
        /// </summary>
        /// <returns></returns>
        public List<Driver> GetDrivers()
        {
            List<Driver> driversList = new List<Driver>();

            string query =
                @"SELECT
                    Drivers.Id,
                    Drivers.Number,
                    Drivers.Name as DriverName,
                    Hometown,
                    RacingTeam,
                    Car,
                    Chassis.Name as ChassisName,
                    Engines.Id as EngineId,
                    Engines.Name as EngineName,
                    RaceClasses.Id as ClassId,
                    RaceClasses.Name as ClassName,
                    ImageLink
                FROM Drivers
                    LEFT JOIN RaceClasses ON ClassId = RaceClasses.Id
                    LEFT JOIN Engines ON EngineId = Engines.Id
                ORDER BY len(Number), Number";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Driver newDriver = new Driver()
                    {
                        Id = (int)row["Id"],
                        Number = (!string.IsNullOrEmpty(row["Number"].ToString()))
                                                ? row["Number"].ToString()
                                                : " - ",
                        Name = row["DriverName"].ToString(),
                        Hometown = row["Hometown"].ToString(),
                        RacingTeam = row["RacingTeam"].ToString(),
                        Car = row["Car"].ToString(),
                        RaceClass = new RaceClass()
                        {
                            Id = (int)row["ClassId"],
                            Name = row["ClassName"].ToString()
                        },
                        Engine = (!row.IsNull("EngineId"))
                            ? new Engine()
                            {
                                Id = (int)row["EngineId"],
                                Name = row["EngineName"].ToString()
                            }
                            : null,
                        ImageLink = row["ImageLink"].ToString()
                    };

                    driversList.Add(newDriver);
                }
            }
            return driversList;
        }

        /// <summary>
        /// Get drivers that are in the RaceResults table
        /// </summary>
        /// <returns>List of Drivers</returns>
        public List<Driver> GetDriversFromResults()
        {
            List<Driver> driversList = new List<Driver>();

            string query =
                @"SELECT
                    Drivers.Id,
                    Drivers.Number,
                    Drivers.Name as DriverName,
                    Hometown,
                    Car,
                    RacingTeam,
                    Chassis.Name as ChassisName,
                    Engines.Id as EngineId,
                    Engines.Name as EngineName,
                    RaceClasses.Id as ClassId,
                    RaceClasses.Name as ClassName,
                    ImageLink
                FROM Drivers
                    LEFT JOIN RaceClasses ON ClassId = RaceClasses.Id
                    LEFT JOIN Engines ON EngineId = Engines.Id
                WHERE Drivers.Id IN (SELECT DriverId FROM Results)
                ORDER BY len(Number), Number";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Driver newDriver = new Driver()
                    {
                        Id = (int)row["Id"],
                        Number = (!string.IsNullOrEmpty(row["Number"].ToString()))
                            ? row["Number"].ToString()
                            : " - ",
                        Name = row["DriverName"].ToString(),
                        Hometown = row["Hometown"].ToString(),
                        RacingTeam = row["RacingTeam"].ToString(),
                        Car = row["Car"].ToString(),
                        RaceClass = new RaceClass()
                        {
                            Id = (int)row["ClassId"],
                            Name = row["ClassName"].ToString()
                        },
                        Engine = (!row.IsNull("EngineId"))
                            ? new Engine()
                            {
                                Id = (int)row["EngineId"],
                                Name = row["EngineName"].ToString()
                            }
                            : null,
                        ImageLink = row["ImageLink"].ToString()
                    };

                    driversList.Add(newDriver);
                }
            }
            return driversList;
        }

        /// <summary>
        /// Get the drivers that are not registered into the race results
        /// and that have a numbers linked to them
        /// </summary>
        /// <param name="raceId">ID of the Race</param>
        /// <returns>List of Drivers still not registered in the race</returns>
        public List<Driver> GetAvailableDrivers(int raceId)
        {
            List<Driver> driversList = new List<Driver>();

            string query =
                @"SELECT 
                    Drivers.Id,
                    Number,
                    Drivers.Name as DriverName,
                    Hometown,
                    RacingTeam,
                    Car,
                    Chassis.Name as ChassisName,
                    Engines.Id as EngineId,
                    Engines.Name as EngineName,
                    RaceClasses.Id as ClassId,
                    RaceClasses.Name as ClassName,
                    ImageLink
                  FROM Drivers
                    LEFT JOIN RaceClasses ON ClassId = RaceClasses.Id
                    LEFT JOIN Engines ON EngineId = Engines.Id
                  WHERE Drivers.Id NOT IN
                    (
                        SELECT DriverId 
                        FROM Results
                        WHERE RaceId = @raceId
                    )
                  AND Number <> ''
                  ORDER BY len(Number), Number;";

            using (DataTable table = DatabaseAccess.GetDataTable(query, new SqlParameter("@raceId", raceId)))
            {
                foreach (DataRow row in table.Rows)
                {
                    Driver newDriver = new Driver()
                    {
                        Id = (int)row["Id"],
                        Number = (!string.IsNullOrEmpty(row["Number"].ToString()))
                                                ? row["Number"].ToString()
                                                : " - ",
                        Name = row["DriverName"].ToString(),
                        Hometown = row["Hometown"].ToString(),
                        RacingTeam = row["RacingTeam"].ToString(),
                        Car = row["Car"].ToString(),
                        RaceClass = new RaceClass()
                        {
                            Id = (int)row["ClassId"],
                            Name = row["ClassName"].ToString()
                        },
                        Engine = (!row.IsNull("EngineId"))
                            ? new Engine()
                            {
                                Id = (int)row["EngineId"],
                                Name = row["EngineName"].ToString()
                            }
                            : null,
                        ImageLink = row["ImageLink"].ToString()
                    };

                    driversList.Add(newDriver);
                }
            }

            return driversList;
        }

        /// <summary>
        /// Get one driver
        /// </summary>
        /// <param name="driver">Driver obj</param>
        /// <returns>A Driver</returns>
        public Driver GetDriverFromID(int id)
        {
            Driver newDriver = new Driver();

            string query =
                @"SELECT
                    Drivers.Id,
                    Drivers.Number,
                    Drivers.Name as DriverName,
                    Hometown,
                    RacingTeam,
                    Car,
                    Chassis.Name as ChassisName,
                    Engines.Id as EngineId,
                    Engines.Name as EngineName,
                    RaceClasses.Id as ClassId,
                    RaceClasses.Name as ClassName,
                    ImageLink
                FROM Drivers
                    LEFT JOIN RaceClasses ON ClassId = RaceClasses.Id
                    LEFT JOIN Engines ON EngineId = Engines.Id
                WHERE Drivers.Id = @driverId";

            using (DataTable table = DatabaseAccess.GetDataTable(query, new SqlParameter("@driverId", id)))
            {
                if (table.Rows.Count > 0)
                {
                    DataRow row = table.Rows[0]; // gets first row

                    newDriver.Id = (int)row["Id"];
                    newDriver.Number = (!string.IsNullOrEmpty(row["Number"].ToString()))
                        ? row["Number"].ToString()
                        : " - ";
                    newDriver.Name = row["DriverName"].ToString();
                    newDriver.Hometown = row["Hometown"].ToString();
                    newDriver.RacingTeam = row["RacingTeam"].ToString();
                    newDriver.Car = row["Car"].ToString();
                    newDriver.RaceClass = new RaceClass()
                    {
                        Id = (int)row["ClassId"],
                        Name = row["ClassName"].ToString()
                    };
                    newDriver.Engine = (!row.IsNull("EngineId"))
                        ? new Engine() { Id = (int)row["EngineId"], Name = row["EngineName"].ToString() }
                        : null;
                    newDriver.ImageLink = row["ImageLink"].ToString();
                }
            }

            return newDriver;
        }

        /// <summary>
        /// Deletes a driver
        /// </summary>
        /// <param name="driver">A Driver</param>
        public void DeleteDriver(Driver driver)
        {
            string query = "DELETE FROM Drivers WHERE Id = @id";

            DatabaseAccess.ApplyChanges(query, new SqlParameter("@id", driver.Id));
        }

        /// <summary>
        /// Updates a driver
        /// </summary>
        /// <param name="driver">Driver object</param>
        public void UpdateDriver(Driver driver)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();

            StringBuilder query = new StringBuilder(
                @"UPDATE Drivers
                  SET 
                    Number = @Number,
                    Name = @Name,
                    Hometown = @Hometown,
                    RacingTeam = @RacingTeam,
                    Car = @Car");

            paramList.Add(new SqlParameter("@Number", driver.Number));
            paramList.Add(new SqlParameter("@Name", driver.Name));
            paramList.Add(new SqlParameter("@Hometown", driver.Hometown));
            paramList.Add(new SqlParameter("@RacingTeam", driver.RacingTeam));
            paramList.Add(new SqlParameter("@Car", driver.Car));

            if (driver.Engine != null)
            {
                query.Append(", EngineId = @EngineId");
                paramList.Add(new SqlParameter("@EngineId", driver.Engine.Id));
            }

            if (driver.ImageLink != null)
            {
                query.Append(", ImageLink = @ImageLink");
                paramList.Add(new SqlParameter("@ImageLink", driver.ImageLink));
            }

            query.Append(", ClassId = @RaceClass");
            paramList.Add(new SqlParameter("@RaceClass", driver.RaceClass.Id));

            query.Append(" WHERE Id = @Id");
            paramList.Add(new SqlParameter("@Id", driver.Id));

            DatabaseAccess.ApplyChanges(query.ToString(), paramList.ToArray());
        }

        /// <summary>
        /// Insert a driver into the database
        /// </summary>
        /// <param name="driver">Driver object</param>
        public void InsertDriver(Driver driver, out int result)
        {
            SqlParameter parameter = new SqlParameter();
            List<SqlParameter> paramList = new List<SqlParameter>();

            StringBuilder sbQuery = new StringBuilder(
                @"INSERT INTO Drivers (
                    Number,
                    Name,
                ");

            StringBuilder sbValues = new StringBuilder(
                @"VALUES(
                    @Number,
                    @Name,
                ");

            StringBuilder sbSelectValues = new StringBuilder(
                "@SELECT "
                );

            paramList.Add(new SqlParameter("@Number", driver.Number));
            paramList.Add(new SqlParameter("@Name", driver.Name));

            // Hometown
            if (!string.IsNullOrEmpty(driver.Hometown))
            {
                sbQuery.Append("Hometown,");
                sbValues.Append("@Hometown,");
                paramList.Add(new SqlParameter("@Hometown", driver.Hometown));
            }

            // Racing team
            if (!string.IsNullOrEmpty(driver.RacingTeam))
            {
                sbQuery.Append("RacingTeam,");
                sbValues.Append("@RacingTeam,");
                paramList.Add(new SqlParameter("@RacingTeam", driver.RacingTeam));
            }

            // Car
            if (!string.IsNullOrEmpty(driver.Car))
            {
                sbQuery.Append("Car,");
                sbValues.Append("@Car,");
                paramList.Add(new SqlParameter("@Car", driver.Car));
            }

            // Engine
            if (driver.Engine != null)
            {
                if (driver.Engine.Id != 0)
                {
                    sbQuery.Append("EngineId,");
                    sbValues.Append("@EngineId,");
                    paramList.Add(new SqlParameter("@EngineId", driver.Engine.Id));
                }
            }

            // ImageLink
            if (!string.IsNullOrEmpty(driver.ImageLink))
            {
                sbQuery.Append("ImageLink,");
                sbValues.Append("@ImageLink,");
                paramList.Add(new SqlParameter("@ImageLink", driver.ImageLink));
            }

            sbQuery.Append("ClassId) ");
            sbQuery.AppendLine("OUTPUT INSERTED.ID");
            sbValues.Append("@ClassId) ");
            paramList.Add(new SqlParameter("@ClassId", driver.RaceClass.Id));

            sbQuery.Append(sbValues);

            DatabaseAccess.ApplyChanges(sbQuery.ToString(), out result, paramList.ToArray());
        }
    }
}
