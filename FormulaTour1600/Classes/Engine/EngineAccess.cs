﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaTour1600
{
    public class EngineAccess
    {
        /// <summary>
        /// Get Engines
        /// </summary>
        /// <returns>List of Engines</returns>
        public List<Engine> GetEngines()
        {
            List<Engine> engineList = new List<Engine>();

            string query =
                @"SELECT
                    Id,
                    Name
                FROM Engines
                ORDER BY Name;";

            using (DataTable table = DatabaseAccess.GetDataTable(query))
            {
                foreach (DataRow row in table.Rows)
                {
                    Engine newEngine = new Engine()
                    {
                        Id = (int)row["Id"],
                        Name = row["Name"].ToString()
                    };

                    engineList.Add(newEngine);
                }
            }
            return engineList;
        }
    }
}
