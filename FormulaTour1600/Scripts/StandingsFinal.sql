﻿SELECT 
	Standings.Id,
	DriverNumber,
	DriverName,
	SUM(TOTAL) AS 'Grand Total'
FROM
(
	SELECT
		Drivers.Id,
		DriverNumber,
		DriverName,
		SeasonYear,
		ClassId, 
		SUM(POINTS) as 'TOTAL'
	FROM RacePositions
		LEFT JOIN RaceResults ON RaceResults.PositionId = RacePositions.Id
		LEFT JOIN Drivers ON RaceResults.DriverId = Drivers.Id
		LEFT JOIN Races ON RaceResults.RaceId = Races.Id
		LEFT JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
		LEFT JOIN RaceTypes ON RaceTypes.Id = RaceSessions.RaceTypeId
		LEFT JOIN Seasons ON Races.SeasonId = Seasons.Id
	WHERE RaceTypes.Name = 'COURSE'
	GROUP BY 
		Drivers.Id,
		DriverNumber,
		DriverName,
		ClassId,
		SeasonYear
	UNION ALL
	SELECT
		Drivers.Id,
		DriverNumber,
		DriverName,
		SeasonYear,
		ClassId,
		TOTAL = SUM(CASE WHEN FinalPosition = '1' AND RaceTypes.Name = 'QUALIFICATION' THEN 1 END)
	FROM RacePositions
		LEFT JOIN RaceResults ON RaceResults.PositionId = RacePositions.Id
		LEFT JOIN Drivers ON RaceResults.DriverId = Drivers.Id
		LEFT JOIN Races ON RaceResults.RaceId = Races.Id
		LEFT JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
		LEFT JOIN RaceTypes ON RaceTypes.Id = RaceSessions.RaceTypeId
		LEFT JOIN Seasons ON Races.SeasonId = Seasons.Id
	WHERE RaceTypes.Name = 'QUALIFICATION'
	GROUP BY 
		Drivers.Id,
		DriverNumber,
		DriverName,
		ClassId,
		SeasonYear,
		FinalPosition,
		RaceTypes.Name
) AS Standings
LEFT JOIN RaceClasses ON RaceClasses.Id = Standings.ClassId
WHERE RaceClasses.Name IS NOT NULL AND SeasonYear = 2017
GROUP BY Standings.Id, DriverName, DriverNumber, SeasonYear
ORDER BY 'Grand Total' DESC;