﻿SELECT 
	Id,
	DriverNumber,
	DriverName,
	GrandTotal
FROM (
	SELECT 
		Id,
		DriverNumber,
		DriverName,
		COALESCE(PolesNbr + TotalPoints, 
			CASE 
				WHEN PolesNbr = 0 THEN NULL 
				ELSE PolesNbr
			END,
		TotalPoints) As GrandTotal
	FROM (
		SELECT 
			Drivers.Id,
			DriverNumber,
			DriverName,
			COUNT(PolePosDriverId) AS PolesNbr,
			(
				SELECT SUM(POINTS) 
				FROM RacePositions
				INNER JOIN RaceResults ON RaceResults.PositionId = RacePositions.Id
				LEFT JOIN Races ON RaceResults.RaceId = Races.Id
				LEFT JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
				LEFT JOIN RaceTypes ON RaceTypes.Id = RaceSessions.RaceTypeId
				WHERE RaceResults.DriverId = Drivers.Id 
				AND RaceTypes.Name = 'COURSE'
			) As TotalPoints
		FROM Drivers
		LEFT JOIN Races ON Races.PolePosDriverId = Drivers.Id
		GROUP BY Drivers.Id, DriverNumber, DriverName
	) Standings
) FinalStandings
WHERE GrandTotal IS NOT NULL
ORDER BY GrandTotal DESC