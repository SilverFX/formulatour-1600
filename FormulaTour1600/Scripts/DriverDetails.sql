﻿SELECT 
	Name,
	ShortName,
	FinalPosition,
	TotalPoints = 
	CASE Description
		WHEN 'COURSE' THEN Points
		ELSE 
		CASE
			WHEN FinalPosition = '1' THEN 1
			ELSE 0
		END
	END
FROM (
	SELECT 
		RaceEvents.Name,
		RaceSessions.ShortName,
		RacePositions.FinalPosition,
		RacePositions.Points,
		RaceTypes.Description
	FROM RaceResults
		INNER JOIN Races ON RaceResults.RaceId = Races.Id
		INNER JOIN RacePositions ON RaceResults.PositionId = RacePositions.Id
		INNER JOIN RaceSessions ON Races.SessionId = RaceSessions.Id
		INNER JOIN RaceEvents ON Races.EventId = RaceEvents.Id
		INNER JOIN RaceTypes ON RaceSessions.RaceTypeId = RaceTypes.Id    
	WHERE RaceResults.DriverId = 79
) As GetResults
ORDER BY Name, ShortName