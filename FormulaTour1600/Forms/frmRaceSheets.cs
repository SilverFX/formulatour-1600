﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmStandingsDetails : Form
    {
        private int? _eventId;
        private int? _seasonId;
        private int? _sessionId;

        private RaceEvent _currentRaceEvent = new RaceEvent();
        private Season _currentSeason = new Season();
        private RaceSession _currentRaceSession = new RaceSession();
        private Race _currentRace = new Race();

        private RacesController _racesController = new RacesController();
        private RaceEventsController _raceEventsController = new RaceEventsController();
        private RaceResultsController _raceResultsController = new RaceResultsController();
        private RaceSessionsController _raceSessionsController = new RaceSessionsController();
        private SeasonsController _seasonsController = new SeasonsController();
        
        //private RaceEventAccess _raceEventAccess = new RaceEventAccess();
        //private SeasonAccess _seasonAccess = new SeasonAccess();
        //private RaceSessionAccess _raceSessionAccess = new RaceSessionAccess();
        //private RaceAccess _raceAccess = new RaceAccess();
        //private ResultAccess _raceResultsAccess = new ResultAccess();

        private List<RaceEvent> _raceEventsList = new List<RaceEvent>();
        private List<Season> _seasonsList = new List<Season>();
        private List<RaceSession> _raceSessionsList = new List<RaceSession>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FrmStandingsDetails(int? eventId = null, int? seasonId = null, int? sessionId = null)
        {
            InitializeComponent();

            _eventId = eventId;
            _seasonId = seasonId;
            _sessionId = sessionId;

            _currentRace = (eventId.HasValue && seasonId.HasValue && sessionId.HasValue)
                ? _racesController.GetRace(eventId.Value, seasonId.Value, sessionId.Value)
                : null;

            _raceEventsList = _raceEventsController.GetEvents().ToList();
            _raceSessionsList = _raceSessionsController.GetSessions();
            _seasonsList = _seasonsController.GetSeasons();

            cboRaceEvents.DataSource = new BindingSource(_raceEventsList, null);
            cboRaceEvents.DisplayMember = "Name";
            cboRaceEvents.ValueMember = "Id";
            cboRaceEvents.SelectedIndex = (eventId.HasValue)
                ? _raceEventsList.FindIndex(re => re.Id == _eventId)
                : -1;
            CboRaceEvents_SelectionChangeCommitted(cboRaceEvents, new EventArgs());
        }

        /// <summary>
        /// ComboBox RaceEvent Selection Change Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboRaceEvents_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.SelectedItem != _currentRaceEvent)
            {
                cboSession.SelectedIndex = -1;
                CboSession_SelectionChangeCommitted(cboSession, new EventArgs());
            }

            _currentRaceEvent = (thisComboBox.SelectedIndex > -1)
                ? (RaceEvent)thisComboBox.SelectedItem
                : null;

            cboSeason.Enabled = (thisComboBox.SelectedIndex > -1);
        }

        /// <summary>
        /// ComboBox Season Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_seasonsList, null);
                thisComboBox.DisplayMember = "SeasonYear";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex = (_seasonId.HasValue)
                    ? _seasonsList.FindIndex(sea => sea.Id == _seasonId)
                    : (_seasonsList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year) > 0)
                        ? _seasonsList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year)
                        : -1;
                CboSeason_SelectionChangeCommitted(thisComboBox, new EventArgs());
            }
        }

        /// <summary>
        /// ComboBox Season Selected Index Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            _currentSeason = (thisComboBox.SelectedIndex > -1)
                ? (Season)thisComboBox.SelectedItem
                : null;

            cboSession.Enabled = (thisComboBox.SelectedIndex > -1);
        }

        /// <summary>
        /// ComboBox Session Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_raceSessionsList, null);
                thisComboBox.DisplayMember = "LongName";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex = (_sessionId.HasValue)
                    ? _raceSessionsList.FindIndex(rs => rs.Id == _sessionId)
                    : -1;
                CboSession_SelectionChangeCommitted(thisComboBox, new EventArgs());
            }
        }

        /// <summary>
        /// ComboBox Session Selection Changed Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            _currentRaceSession = (thisComboBox.SelectedIndex > -1)
                ? (RaceSession)thisComboBox.SelectedItem
                : null;

            _currentRace = (thisComboBox.SelectedIndex > -1)
                ? _racesController.GetRace(_currentRaceEvent.Id, _currentSeason.Id, _currentRaceSession.Id)
                : null;

            if (_currentRace != null)
            {
                UpdateListView();
            }
            else
            {
                lvwResults.Items.Clear();
            }
        }

        /// <summary>
        /// Fills the List View
        /// </summary>
        private void UpdateListView()
        {
            lvwResults.Items.Clear();

            try
            {
                List<RaceResult> results = _raceResultsController.GetResults(_currentRace);

                foreach (RaceResult result in results)
                {
                    ListViewItem lviRow = new ListViewItem(result.RacePosition.FinalPosition)
                    {
                        Tag = result,
                        Name = result.Id.ToString()
                    };
                    lviRow.SubItems.Add(result.Driver.Number.ToString());
                    lviRow.SubItems.Add(result.Driver.Name);

                    lvwResults.Items.Add(lviRow);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Prevent column resizing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwResults_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvwResults.Columns[e.ColumnIndex].Width;
        }

        /// <summary>
        /// ListView Double Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwResults_DoubleClick(object sender, EventArgs e)
        {
            DriversController driversController = new DriversController();

            Driver selectedDriver = ((RaceResult)lvwResults.SelectedItems[0].Tag).Driver;

            using (var frm = new frmDriverDetails(selectedDriver, true))
            {
                frm.ShowDialog();
            }
        }
    }
}
