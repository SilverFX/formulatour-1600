﻿namespace FormulaTour1600
{
    partial class FrmStandings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStandings));
            this.label1 = new System.Windows.Forms.Label();
            this.cboClass = new System.Windows.Forms.ComboBox();
            this.lvwStandings = new System.Windows.Forms.ListView();
            this.clhPos = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhDriverNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhDriver = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPoints = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.tltQuit = new System.Windows.Forms.ToolTip(this.components);
            this.cboSeason = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(163, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 34);
            this.label1.TabIndex = 1;
            this.label1.Text = "Classe:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboClass
            // 
            this.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboClass.Enabled = false;
            this.cboClass.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboClass.FormattingEnabled = true;
            this.cboClass.Location = new System.Drawing.Point(217, 14);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(134, 25);
            this.cboClass.TabIndex = 11;
            this.cboClass.SelectionChangeCommitted += new System.EventHandler(this.CboClass_SelectionChangeCommitted);
            // 
            // lvwStandings
            // 
            this.lvwStandings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clhPos,
            this.clhDriverNum,
            this.clhDriver,
            this.clhPoints});
            this.lvwStandings.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwStandings.FullRowSelect = true;
            this.lvwStandings.Location = new System.Drawing.Point(16, 47);
            this.lvwStandings.Name = "lvwStandings";
            this.lvwStandings.Size = new System.Drawing.Size(335, 412);
            this.lvwStandings.TabIndex = 12;
            this.lvwStandings.UseCompatibleStateImageBehavior = false;
            this.lvwStandings.View = System.Windows.Forms.View.Details;
            this.lvwStandings.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.LvwStandings_ColumnWidthChanging);
            this.lvwStandings.DoubleClick += new System.EventHandler(this.LvwStandings_DoubleClick);
            // 
            // clhPos
            // 
            this.clhPos.Text = "Pos.";
            this.clhPos.Width = 39;
            // 
            // clhDriverNum
            // 
            this.clhDriverNum.Text = "No.";
            this.clhDriverNum.Width = 40;
            // 
            // clhDriver
            // 
            this.clhDriver.Text = "Pilote";
            this.clhDriver.Width = 163;
            // 
            // clhPoints
            // 
            this.clhPoints.Text = "Points";
            this.clhPoints.Width = 73;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.pictureBox1.Location = new System.Drawing.Point(16, 466);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(265, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.Location = new System.Drawing.Point(287, 463);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(64, 86);
            this.btnQuit.TabIndex = 14;
            this.tltQuit.SetToolTip(this.btnQuit, "Quitter");
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // tltQuit
            // 
            this.tltQuit.IsBalloon = true;
            // 
            // cboSeason
            // 
            this.cboSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeason.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSeason.FormattingEnabled = true;
            this.cboSeason.Location = new System.Drawing.Point(74, 14);
            this.cboSeason.Name = "cboSeason";
            this.cboSeason.Size = new System.Drawing.Size(82, 25);
            this.cboSeason.TabIndex = 25;
            this.cboSeason.SelectedIndexChanged += new System.EventHandler(this.CboSeason_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 33);
            this.label6.TabIndex = 24;
            this.label6.Text = "Saison:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmStandings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 555);
            this.Controls.Add(this.cboSeason);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cboClass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvwStandings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmStandings";
            this.Text = "CLASSEMENT GÉNÉRAL : < CLASSE >";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboClass;
        private System.Windows.Forms.ListView lvwStandings;
        private System.Windows.Forms.ColumnHeader clhPos;
        private System.Windows.Forms.ColumnHeader clhDriverNum;
        private System.Windows.Forms.ColumnHeader clhDriver;
        private System.Windows.Forms.ColumnHeader clhPoints;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ToolTip tltQuit;
        private System.Windows.Forms.ComboBox cboSeason;
        private System.Windows.Forms.Label label6;
    }
}