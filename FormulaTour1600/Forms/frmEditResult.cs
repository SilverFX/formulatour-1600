﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmEditResult : Form
    {
        private RaceResult _currentResult = new RaceResult();

        private RacePositionsController _racePositionsController = new RacePositionsController();
        private RaceResultsController _raceResultsController = new RaceResultsController();
        private DriversController _driversController = new DriversController();

        //private ResultAccess _raceResultAccess = new ResultAccess();
        //private DriverAccess _driverAccess = new DriverAccess();

        private List<Driver> _driverList = new List<Driver>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="result"></param>
        public FrmEditResult(RaceResult result)
        {
            InitializeComponent();

            _currentResult = result;
            _driverList = _driversController.GetDrivers();

            List<RacePosition> positionList = _racePositionsController.GetAllRacePositions();
            List<Driver> driverList = _driversController.GetDrivers();

            cboPosition.DataSource = new BindingSource(positionList, null);
            cboPosition.DisplayMember = "FinalPosition";
            cboPosition.ValueMember = "FinalPosition";
            cboPosition.SelectedIndex = cboPosition.FindStringExact(result.RacePosition.FinalPosition);

            txtNumber.Text = result.Driver.Number.ToString();
            txtDriver.Text = result.Driver.Name;

            chkPole.Checked = result.HasPolePosition.Value;
        }

        #region -------------------------------------------------- BUTTON CLICK EVENTS --------------------------------------------------
        /// <summary>
        /// Save the changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            _raceResultsController.UpdateResult(GetResultFromInputs());
        }

        /// <summary>
        /// Delete the result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result =
                MessageBox.Show(
                    "Êtes vous certain de vouloir supprimer cet enregistrement?",
                    "Suppression",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );

            if (result == DialogResult.Yes)
            {
                _raceResultsController.DeleteResult(_currentResult);
                DialogResult = result;
            }
        }
        #endregion

        #region -------------------------------------------------- CUSTOM METHODS --------------------------------------------------
        /// <summary>
        /// Get the RaceResult corresponding to the inputs
        /// </summary>
        /// <returns>RaceResult Obj</returns>
        private RaceResult GetResultFromInputs()
        {
            RaceResult newResult = _currentResult;

            // Overrides the new result
            newResult.Position_Id = ((RacePosition)cboPosition.SelectedItem).Id;
            newResult.HasPolePosition = chkPole.Checked;

            return newResult;
        }
        #endregion
    }
}
