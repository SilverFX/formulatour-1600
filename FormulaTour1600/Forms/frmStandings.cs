﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmStandings : Form
    {
        private const string DEFAULT_TITLE = "CLASSEMENT GÉNÉRAL";
        private List<RaceClass> _raceClassList = new List<RaceClass>();
        private DriversController _driversController = new DriversController();
        private RaceClassesController _raceClassesController = new RaceClassesController();
        private SeasonsController _seasonsController = new SeasonsController();
        private RaceResultsController _raceResultsController = new RaceResultsController();

        private List<Season> _seasonsList = new List<Season>();
        private List<Driver> _driverList = new List<Driver>();


        /// <summary>
        /// Constructor
        /// </summary>
        public FrmStandings()
        {
            InitializeComponent();

            _seasonsList = _seasonsController.GetSeasons();
            _raceClassList = _raceClassesController.GetRaceClasses();

            cboSeason.DataSource = new BindingSource(_seasonsList, null);
            cboSeason.DisplayMember = "SeasonYear";
            cboSeason.ValueMember = "Id";
            cboSeason.SelectedIndex = cboSeason.FindString(DateTime.Now.Year.ToString());

            cboClass.SelectedIndex = 0;
            CboClass_SelectionChangeCommitted(cboClass, new EventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboClass.Enabled = true;

            cboClass.DataSource = new BindingSource(_raceClassList, null);
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Id";
            cboClass.SelectedIndex = -1;

            if (lvwStandings.Items.Count > 0) { lvwStandings.Items.Clear(); }
        }

        /// <summary>
        /// Changing the Race Class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboClass_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            var drivers = _raceResultsController.GetDriversStandings((RaceClass)thisComboBox.SelectedItem, (Season)cboSeason.SelectedItem);
            foreach (var driver in drivers)
            {
                var tests = driver.RaceResults;
            }

            try
            {
                //_driverList = _standingsAccess.GetDriversStandings(
                //    ((RaceClass)thisComboBox.SelectedItem).Name,
                //    ((Season)cboSeason.SelectedItem).SeasonYear
                //);

                UpdateListView();
                string.Concat(DEFAULT_TITLE, ((RaceClass)thisComboBox.SelectedItem).Name);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// Updates the list view
        /// </summary>
        private void UpdateListView()
        {
            int driverCount = 0;
            lvwStandings.Items.Clear();

            foreach (Driver driver in _driverList)
            {
                driverCount++;

                ListViewItem lviRow = new ListViewItem(driverCount.ToString())
                {
                    Tag = driver,
                    Name = driver.Id.ToString()
                };
                lviRow.SubItems.Add(driver.Number.ToString());
                lviRow.SubItems.Add(driver.Name);
                //lviRow.SubItems.Add(driver.Points.ToString());

                lvwStandings.Items.Add(lviRow);
            }
        }

        /// <summary>
        /// Prevent resizing of the columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwStandings_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            ListView thisListView = (ListView)sender;

            e.Cancel = true;
            e.NewWidth = thisListView.Columns[e.ColumnIndex].Width;
        }

        /// <summary>
        /// Displays the driver's information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwStandings_DoubleClick(object sender, EventArgs e)
        {
            Driver selectedDriver = (Driver)lvwStandings.SelectedItems[0].Tag;

            using (var frm = new frmDriverDetails(selectedDriver, true))
            {
                frm.ShowDialog();
            }
        }
    }
}
