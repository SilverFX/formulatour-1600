﻿namespace FormulaTour1600
{
    partial class frmRaces
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRaces));
            this.cboTrack = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpEventDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSeason = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboRaceEvents = new System.Windows.Forms.ComboBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.tltDelete = new System.Windows.Forms.ToolTip(this.components);
            this.chkNoDate = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSession = new System.Windows.Forms.ComboBox();
            this.chkApplyAll = new System.Windows.Forms.CheckBox();
            this.grpApplyToAll = new System.Windows.Forms.GroupBox();
            this.staInformations = new System.Windows.Forms.StatusStrip();
            this.tslblState = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.grpApplyToAll.SuspendLayout();
            this.staInformations.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboTrack
            // 
            this.cboTrack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTrack.Enabled = false;
            this.cboTrack.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTrack.FormattingEnabled = true;
            this.cboTrack.Location = new System.Drawing.Point(115, 159);
            this.cboTrack.Name = "cboTrack";
            this.cboTrack.Size = new System.Drawing.Size(225, 25);
            this.cboTrack.TabIndex = 54;
            this.cboTrack.SelectedIndexChanged += new System.EventHandler(this.CboTrack_SelectedIndexChanged);
            this.cboTrack.EnabledChanged += new System.EventHandler(this.CboTrack_EnabledChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 154);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 33);
            this.label5.TabIndex = 53;
            this.label5.Text = "Circuit actuel:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpEventDate
            // 
            this.dtpEventDate.Checked = false;
            this.dtpEventDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEventDate.Enabled = false;
            this.dtpEventDate.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEventDate.Location = new System.Drawing.Point(115, 123);
            this.dtpEventDate.Name = "dtpEventDate";
            this.dtpEventDate.Size = new System.Drawing.Size(154, 27);
            this.dtpEventDate.TabIndex = 52;
            this.dtpEventDate.Value = new System.DateTime(2017, 9, 5, 16, 5, 0, 0);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 33);
            this.label4.TabIndex = 51;
            this.label4.Text = "Date de course:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboSeason
            // 
            this.cboSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeason.Enabled = false;
            this.cboSeason.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSeason.FormattingEnabled = true;
            this.cboSeason.Location = new System.Drawing.Point(115, 51);
            this.cboSeason.Name = "cboSeason";
            this.cboSeason.Size = new System.Drawing.Size(72, 25);
            this.cboSeason.TabIndex = 50;
            this.cboSeason.SelectedIndexChanged += new System.EventHandler(this.CboSeason_SelectedIndexChanged);
            this.cboSeason.EnabledChanged += new System.EventHandler(this.CboSeason_EnabledChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 33);
            this.label6.TabIndex = 49;
            this.label6.Text = "Saison:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 34);
            this.label1.TabIndex = 45;
            this.label1.Text = "Événement:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboRaceEvents
            // 
            this.cboRaceEvents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRaceEvents.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRaceEvents.FormattingEnabled = true;
            this.cboRaceEvents.Location = new System.Drawing.Point(115, 15);
            this.cboRaceEvents.Name = "cboRaceEvents";
            this.cboRaceEvents.Size = new System.Drawing.Size(256, 25);
            this.cboRaceEvents.TabIndex = 47;
            this.cboRaceEvents.SelectionChangeCommitted += new System.EventHandler(this.CboRaceEvents_SelectionChangeCommitted);
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(262, 251);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(109, 42);
            this.btnQuit.TabIndex = 55;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(346, 159);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(25, 25);
            this.btnClear.TabIndex = 57;
            this.tltDelete.SetToolTip(this.btnClear, "Effacer la sélection du circuit");
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // tltDelete
            // 
            this.tltDelete.IsBalloon = true;
            // 
            // chkNoDate
            // 
            this.chkNoDate.Enabled = false;
            this.chkNoDate.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNoDate.Location = new System.Drawing.Point(275, 125);
            this.chkNoDate.Name = "chkNoDate";
            this.chkNoDate.Size = new System.Drawing.Size(96, 24);
            this.chkNoDate.TabIndex = 58;
            this.chkNoDate.Text = "Pas de date";
            this.chkNoDate.UseVisualStyleBackColor = true;
            this.chkNoDate.CheckedChanged += new System.EventHandler(this.ChkNoDate_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(6, 251);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 42);
            this.btnSave.TabIndex = 59;
            this.btnSave.Text = "&Enregistrer";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(121, 251);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(135, 41);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 60;
            this.picLogo.TabStop = false;
            this.picLogo.DoubleClick += new System.EventHandler(this.PicLogo_DoubleClick);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 33);
            this.label2.TabIndex = 46;
            this.label2.Text = "Session:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboSession
            // 
            this.cboSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSession.Enabled = false;
            this.cboSession.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSession.FormattingEnabled = true;
            this.cboSession.Location = new System.Drawing.Point(115, 87);
            this.cboSession.Name = "cboSession";
            this.cboSession.Size = new System.Drawing.Size(256, 25);
            this.cboSession.TabIndex = 48;
            this.cboSession.SelectionChangeCommitted += new System.EventHandler(this.CboSession_SelectionChangeCommitted);
            this.cboSession.EnabledChanged += new System.EventHandler(this.CboSession_EnabledChanged);
            // 
            // chkApplyAll
            // 
            this.chkApplyAll.Enabled = false;
            this.chkApplyAll.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkApplyAll.Location = new System.Drawing.Point(10, 24);
            this.chkApplyAll.Name = "chkApplyAll";
            this.chkApplyAll.Size = new System.Drawing.Size(349, 24);
            this.chkApplyAll.TabIndex = 61;
            this.chkApplyAll.Text = "Appliquer la date et le circuit à toutes les sessions";
            this.chkApplyAll.UseVisualStyleBackColor = true;
            // 
            // grpApplyToAll
            // 
            this.grpApplyToAll.Controls.Add(this.chkApplyAll);
            this.grpApplyToAll.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpApplyToAll.Location = new System.Drawing.Point(6, 190);
            this.grpApplyToAll.Name = "grpApplyToAll";
            this.grpApplyToAll.Size = new System.Drawing.Size(365, 57);
            this.grpApplyToAll.TabIndex = 63;
            this.grpApplyToAll.TabStop = false;
            this.grpApplyToAll.Text = "Modifications Générales";
            // 
            // staInformations
            // 
            this.staInformations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblState});
            this.staInformations.Location = new System.Drawing.Point(0, 298);
            this.staInformations.Name = "staInformations";
            this.staInformations.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.staInformations.Size = new System.Drawing.Size(378, 22);
            this.staInformations.TabIndex = 64;
            this.staInformations.Text = "statusStrip1";
            // 
            // tslblState
            // 
            this.tslblState.Font = new System.Drawing.Font("Microsoft NeoGothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblState.Name = "tslblState";
            this.tslblState.Size = new System.Drawing.Size(332, 17);
            this.tslblState.Spring = true;
            this.tslblState.Text = "< Message >";
            // 
            // tmrStatus
            // 
            this.tmrStatus.Interval = 5000;
            this.tmrStatus.Tick += new System.EventHandler(this.TmrStatus_Tick);
            // 
            // frmRaces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 320);
            this.Controls.Add(this.staInformations);
            this.Controls.Add(this.grpApplyToAll);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkNoDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.cboTrack);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpEventDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cboSeason);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboRaceEvents);
            this.Controls.Add(this.cboSession);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmRaces";
            this.Text = "GESTION DES COURSES";
            this.Load += new System.EventHandler(this.FrmRaces_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.grpApplyToAll.ResumeLayout(false);
            this.staInformations.ResumeLayout(false);
            this.staInformations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboTrack;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpEventDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboSeason;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboRaceEvents;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ToolTip tltDelete;
        private System.Windows.Forms.CheckBox chkNoDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboSession;
        private System.Windows.Forms.CheckBox chkApplyAll;
        private System.Windows.Forms.GroupBox grpApplyToAll;
        private System.Windows.Forms.StatusStrip staInformations;
        private System.Windows.Forms.ToolStripStatusLabel tslblState;
        private System.Windows.Forms.Timer tmrStatus;
    }
}