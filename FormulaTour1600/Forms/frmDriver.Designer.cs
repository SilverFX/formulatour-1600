﻿namespace FormulaTour1600
{
    partial class FrmDrivers
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDrivers));
            this.grpDriver = new System.Windows.Forms.GroupBox();
            this.chkRegistered = new System.Windows.Forms.CheckBox();
            this.txtEngine = new System.Windows.Forms.TextBox();
            this.cboEngine = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCar = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.txtRacingTeam = new System.Windows.Forms.TextBox();
            this.txtHometown = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.cboClass = new System.Windows.Forms.ComboBox();
            this.lblClass = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.cboDrivers = new System.Windows.Forms.ComboBox();
            this.txtDriverNum = new System.Windows.Forms.TextBox();
            this.staState = new System.Windows.Forms.StatusStrip();
            this.tslblState = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            this.grpPhoto = new System.Windows.Forms.GroupBox();
            this.btnResetPhoto = new System.Windows.Forms.Button();
            this.btnRemovePhoto = new System.Windows.Forms.Button();
            this.pboPhoto = new System.Windows.Forms.PictureBox();
            this.btnSelectPhoto = new System.Windows.Forms.Button();
            this.ofdPhoto = new System.Windows.Forms.OpenFileDialog();
            this.tltPhoto = new System.Windows.Forms.ToolTip(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpDriver.SuspendLayout();
            this.staState.SuspendLayout();
            this.grpPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDriver
            // 
            this.grpDriver.Controls.Add(this.chkRegistered);
            this.grpDriver.Controls.Add(this.txtEngine);
            this.grpDriver.Controls.Add(this.cboEngine);
            this.grpDriver.Controls.Add(this.label6);
            this.grpDriver.Controls.Add(this.txtCar);
            this.grpDriver.Controls.Add(this.label4);
            this.grpDriver.Controls.Add(this.txtClass);
            this.grpDriver.Controls.Add(this.txtRacingTeam);
            this.grpDriver.Controls.Add(this.txtHometown);
            this.grpDriver.Controls.Add(this.txtName);
            this.grpDriver.Controls.Add(this.cboClass);
            this.grpDriver.Controls.Add(this.lblClass);
            this.grpDriver.Controls.Add(this.label3);
            this.grpDriver.Controls.Add(this.label2);
            this.grpDriver.Controls.Add(this.label1);
            this.grpDriver.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDriver.Location = new System.Drawing.Point(13, 52);
            this.grpDriver.Name = "grpDriver";
            this.grpDriver.Size = new System.Drawing.Size(399, 247);
            this.grpDriver.TabIndex = 2;
            this.grpDriver.TabStop = false;
            this.grpDriver.Text = "Informations du pilote";
            // 
            // chkRegistered
            // 
            this.chkRegistered.Enabled = false;
            this.chkRegistered.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkRegistered.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRegistered.Location = new System.Drawing.Point(132, 213);
            this.chkRegistered.Name = "chkRegistered";
            this.chkRegistered.Size = new System.Drawing.Size(152, 27);
            this.chkRegistered.TabIndex = 15;
            this.chkRegistered.Text = "Inscrit";
            this.chkRegistered.UseVisualStyleBackColor = true;
            // 
            // txtEngine
            // 
            this.txtEngine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEngine.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEngine.Location = new System.Drawing.Point(132, 151);
            this.txtEngine.Name = "txtEngine";
            this.txtEngine.ReadOnly = true;
            this.txtEngine.Size = new System.Drawing.Size(120, 25);
            this.txtEngine.TabIndex = 11;
            // 
            // cboEngine
            // 
            this.cboEngine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEngine.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEngine.FormattingEnabled = true;
            this.cboEngine.Location = new System.Drawing.Point(132, 151);
            this.cboEngine.Name = "cboEngine";
            this.cboEngine.Size = new System.Drawing.Size(137, 25);
            this.cboEngine.TabIndex = 11;
            this.cboEngine.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "Moteur : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCar
            // 
            this.txtCar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCar.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCar.Location = new System.Drawing.Point(132, 120);
            this.txtCar.Name = "txtCar";
            this.txtCar.ReadOnly = true;
            this.txtCar.Size = new System.Drawing.Size(261, 25);
            this.txtCar.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Chassis : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtClass
            // 
            this.txtClass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClass.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClass.Location = new System.Drawing.Point(132, 182);
            this.txtClass.Name = "txtClass";
            this.txtClass.ReadOnly = true;
            this.txtClass.Size = new System.Drawing.Size(120, 25);
            this.txtClass.TabIndex = 13;
            // 
            // txtRacingTeam
            // 
            this.txtRacingTeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRacingTeam.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRacingTeam.Location = new System.Drawing.Point(132, 89);
            this.txtRacingTeam.Name = "txtRacingTeam";
            this.txtRacingTeam.ReadOnly = true;
            this.txtRacingTeam.Size = new System.Drawing.Size(261, 25);
            this.txtRacingTeam.TabIndex = 5;
            // 
            // txtHometown
            // 
            this.txtHometown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHometown.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHometown.Location = new System.Drawing.Point(132, 58);
            this.txtHometown.Name = "txtHometown";
            this.txtHometown.ReadOnly = true;
            this.txtHometown.Size = new System.Drawing.Size(261, 25);
            this.txtHometown.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(132, 27);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(261, 25);
            this.txtName.TabIndex = 1;
            this.txtName.TextChanged += new System.EventHandler(this.OnTextChanged);
            // 
            // cboClass
            // 
            this.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboClass.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboClass.FormattingEnabled = true;
            this.cboClass.Location = new System.Drawing.Point(132, 182);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(137, 25);
            this.cboClass.TabIndex = 12;
            this.cboClass.Visible = false;
            // 
            // lblClass
            // 
            this.lblClass.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClass.Location = new System.Drawing.Point(6, 182);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(131, 23);
            this.lblClass.TabIndex = 12;
            this.lblClass.Text = "Classe : ";
            this.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Équipe : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Provenance : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom du pilote : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNumber
            // 
            this.lblNumber.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.Location = new System.Drawing.Point(22, 13);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(128, 23);
            this.lblNumber.TabIndex = 1;
            this.lblNumber.Text = "Numéro : ";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboDrivers
            // 
            this.cboDrivers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDrivers.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDrivers.FormattingEnabled = true;
            this.cboDrivers.Location = new System.Drawing.Point(146, 13);
            this.cboDrivers.Name = "cboDrivers";
            this.cboDrivers.Size = new System.Drawing.Size(57, 25);
            this.cboDrivers.TabIndex = 2;
            this.cboDrivers.SelectionChangeCommitted += new System.EventHandler(this.CboDrivers_SelectionChangeCommitted);
            // 
            // txtDriverNum
            // 
            this.txtDriverNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDriverNum.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDriverNum.Location = new System.Drawing.Point(146, 13);
            this.txtDriverNum.MaxLength = 3;
            this.txtDriverNum.Name = "txtDriverNum";
            this.txtDriverNum.Size = new System.Drawing.Size(46, 25);
            this.txtDriverNum.TabIndex = 1;
            this.txtDriverNum.Visible = false;
            this.txtDriverNum.TextChanged += new System.EventHandler(this.OnTextChanged);
            this.txtDriverNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNum_KeyPress);
            // 
            // staState
            // 
            this.staState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblState});
            this.staState.Location = new System.Drawing.Point(0, 445);
            this.staState.Name = "staState";
            this.staState.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.staState.Size = new System.Drawing.Size(684, 22);
            this.staState.TabIndex = 9;
            // 
            // tslblState
            // 
            this.tslblState.Font = new System.Drawing.Font("Microsoft NeoGothic", 9F);
            this.tslblState.Name = "tslblState";
            this.tslblState.Size = new System.Drawing.Size(669, 17);
            this.tslblState.Spring = true;
            this.tslblState.Text = "< Message >";
            this.tslblState.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // btnSearch
            // 
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(261, 11);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 29);
            this.btnSearch.TabIndex = 13;
            this.btnSearch.Text = "&Rechercher un pilote";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // tmrStatus
            // 
            this.tmrStatus.Interval = 5000;
            this.tmrStatus.Tick += new System.EventHandler(this.TmrStatus_Tick);
            // 
            // grpPhoto
            // 
            this.grpPhoto.Controls.Add(this.btnResetPhoto);
            this.grpPhoto.Controls.Add(this.btnRemovePhoto);
            this.grpPhoto.Controls.Add(this.pboPhoto);
            this.grpPhoto.Controls.Add(this.btnSelectPhoto);
            this.grpPhoto.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPhoto.Location = new System.Drawing.Point(418, 12);
            this.grpPhoto.Name = "grpPhoto";
            this.grpPhoto.Size = new System.Drawing.Size(257, 287);
            this.grpPhoto.TabIndex = 10;
            this.grpPhoto.TabStop = false;
            this.grpPhoto.Text = "Photo";
            // 
            // btnResetPhoto
            // 
            this.btnResetPhoto.Enabled = false;
            this.btnResetPhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetPhoto.Image = ((System.Drawing.Image)(resources.GetObject("btnResetPhoto.Image")));
            this.btnResetPhoto.Location = new System.Drawing.Point(206, 239);
            this.btnResetPhoto.Name = "btnResetPhoto";
            this.btnResetPhoto.Size = new System.Drawing.Size(42, 42);
            this.btnResetPhoto.TabIndex = 13;
            this.tltPhoto.SetToolTip(this.btnResetPhoto, "Réinitialiser la photo");
            this.btnResetPhoto.UseVisualStyleBackColor = true;
            this.btnResetPhoto.Visible = false;
            // 
            // btnRemovePhoto
            // 
            this.btnRemovePhoto.Enabled = false;
            this.btnRemovePhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemovePhoto.Image = ((System.Drawing.Image)(resources.GetObject("btnRemovePhoto.Image")));
            this.btnRemovePhoto.Location = new System.Drawing.Point(158, 239);
            this.btnRemovePhoto.Name = "btnRemovePhoto";
            this.btnRemovePhoto.Size = new System.Drawing.Size(42, 42);
            this.btnRemovePhoto.TabIndex = 12;
            this.tltPhoto.SetToolTip(this.btnRemovePhoto, "Retirer la photo");
            this.btnRemovePhoto.UseVisualStyleBackColor = true;
            this.btnRemovePhoto.Visible = false;
            this.btnRemovePhoto.Click += new System.EventHandler(this.BtnRemovePhoto_Click);
            // 
            // pboPhoto
            // 
            this.pboPhoto.Location = new System.Drawing.Point(6, 24);
            this.pboPhoto.Name = "pboPhoto";
            this.pboPhoto.Size = new System.Drawing.Size(245, 209);
            this.pboPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pboPhoto.TabIndex = 0;
            this.pboPhoto.TabStop = false;
            this.pboPhoto.Paint += new System.Windows.Forms.PaintEventHandler(this.PboPhoto_Paint);
            // 
            // btnSelectPhoto
            // 
            this.btnSelectPhoto.Enabled = false;
            this.btnSelectPhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectPhoto.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectPhoto.Image")));
            this.btnSelectPhoto.Location = new System.Drawing.Point(110, 239);
            this.btnSelectPhoto.Name = "btnSelectPhoto";
            this.btnSelectPhoto.Size = new System.Drawing.Size(42, 42);
            this.btnSelectPhoto.TabIndex = 1;
            this.tltPhoto.SetToolTip(this.btnSelectPhoto, "Choisir une photo");
            this.btnSelectPhoto.UseVisualStyleBackColor = true;
            this.btnSelectPhoto.Click += new System.EventHandler(this.BtnSelectPhoto_Click);
            // 
            // ofdPhoto
            // 
            this.ofdPhoto.FileName = "openFileDialog1";
            this.ofdPhoto.RestoreDirectory = true;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(14, 400);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 42);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Enregistrer";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(129, 305);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(432, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(567, 305);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(109, 42);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "A&nnuler";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(567, 401);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(109, 42);
            this.btnQuit.TabIndex = 8;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Enabled = false;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(14, 353);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(109, 42);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "&Modifier";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(13, 305);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(109, 42);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Ajouter";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // FrmDrivers
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 467);
            this.Controls.Add(this.grpPhoto);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.staState);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grpDriver);
            this.Controls.Add(this.txtDriverNum);
            this.Controls.Add(this.cboDrivers);
            this.Controls.Add(this.lblNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmDrivers";
            this.Text = "GESTION DE PILOTE";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.grpDriver.ResumeLayout(false);
            this.grpDriver.PerformLayout();
            this.staState.ResumeLayout(false);
            this.staState.PerformLayout();
            this.grpPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pboPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDriver;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.ComboBox cboDrivers;
        private System.Windows.Forms.TextBox txtRacingTeam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHometown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.TextBox txtDriverNum;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.ComboBox cboClass;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip staState;
        private System.Windows.Forms.ToolStripStatusLabel tslblState;
        private System.Windows.Forms.TextBox txtCar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Timer tmrStatus;
        private System.Windows.Forms.GroupBox grpPhoto;
        private System.Windows.Forms.PictureBox pboPhoto;
        private System.Windows.Forms.Button btnSelectPhoto;
        private System.Windows.Forms.Button btnRemovePhoto;
        private System.Windows.Forms.OpenFileDialog ofdPhoto;
        private System.Windows.Forms.ToolTip tltPhoto;
        private System.Windows.Forms.Button btnResetPhoto;
        private System.Windows.Forms.TextBox txtEngine;
        private System.Windows.Forms.ComboBox cboEngine;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkRegistered;
    }
}

