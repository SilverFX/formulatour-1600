﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class frmRaces : Form
    {
        private Race _currentRace = new Race();
        private RacesController _racesController= new RacesController();

        private RaceEvent _currentRaceEvent = new RaceEvent();
        private RaceEventsController _raceEventsController = new RaceEventsController();
        private List<RaceEvent> _raceEventList = new List<RaceEvent>();

        private Season _currentSeason = new Season();
        private SeasonsController _seasonsController = new SeasonsController();
        private List<Season> _seasonsList = new List<Season>();

        private RaceSession _currentRaceSession = new RaceSession();
        private RaceSessionsController _raceSessionsController = new RaceSessionsController();
        private List<RaceSession> _sessionList = new List<RaceSession>();

        private RaceTracksController _raceTracksController = new RaceTracksController();

        /// <summary>
        /// Constructor
        /// </summary>
        public frmRaces()
        {
            InitializeComponent();

            _raceEventList = _raceEventsController.GetEvents();
            _seasonsList = _seasonsController.GetSeasons(); //_seasonAccess.GetSeasons();
            _sessionList = _raceSessionsController.GetSessions(); // _sessionAccess.GetSessions();

            cboRaceEvents.DataSource = new BindingSource(_raceEventList, null);
            cboRaceEvents.DisplayMember = "Name";
            cboRaceEvents.ValueMember = "Id";
            cboRaceEvents.SelectedIndex = -1;
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmRaces_Load(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
        }

        /// <summary>
        /// Race Event Index Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboRaceEvents_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if ((RaceEvent)thisComboBox.SelectedItem != _currentRaceEvent)
            {
                // Reset
                ResetControlsAndValues();
            }

            _currentRaceEvent = (RaceEvent)thisComboBox.SelectedItem;
            cboSeason.Enabled = true;
        }

        /// <summary>
        /// ComboBox Season Enabled changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_seasonsList, null);
                thisComboBox.DisplayMember = "SeasonYear";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex =
                    (_seasonsList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year) > 0) ? _seasonsList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year) : -1;
            }
        }

        /// <summary>
        /// Selected Index Changed Event for ComboBox Season
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.SelectedIndex < 0)
            {
                cboSession.Enabled = false;
            }
            else
            {
                cboSession.Enabled = true;
                _currentSeason = (Season)thisComboBox.SelectedItem;
            }

            ResetControlsAndValues();
        }

        /// <summary>
        /// ComboBox Session Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_sessionList, null);
                thisComboBox.DisplayMember = "LongName";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Race Session Index Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            _currentRaceSession = (RaceSession)thisComboBox.SelectedItem;
            
            _currentRace = _racesController.GetRace(_currentRaceEvent.Id, _currentSeason.Id, _currentRaceSession.Id);

            // Create if no race found
            if (_currentRace.Id == 0)
            {
                _racesController.InsertRace(
                    _currentRaceEvent.Id,
                    _currentSeason.Id,
                    _currentRaceSession.Id);

                // Inserts new data into the race object
                _currentRace.RaceEvent.Id = (cboRaceEvents.SelectedItem as RaceEvent).Id;
                _currentRace.RaceSession.Id = (thisComboBox.SelectedItem as RaceSession).Id;
            }

            chkNoDate.Enabled = true;
            chkApplyAll.Enabled = true;

            dtpEventDate.Enabled = true;

            if (_currentRace.Date.HasValue)
            {
                dtpEventDate.Value = _currentRace.Date.Value;
                chkNoDate.Checked = false;
            }
            else
            {
                dtpEventDate.Value = DateTime.Now;
                chkNoDate.Checked = true;
            }

            cboTrack.Enabled = true;

            btnSave.Enabled = true;
        }

        /// <summary>
        /// CheckBox Checked Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkNoDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpEventDate.Enabled = !chkNoDate.Checked;
        }


        /// <summary>
        /// Combobox Track Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboTrack_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_tracksList, null);
                thisComboBox.DisplayMember = "Name";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex = (_currentRace.RaceTrack.Id != 0)
                    ? _tracksList.FindIndex(t => t.Id == _currentRace.RaceTrack.Id)
                    : -1;
            }
        }

        /// <summary>
        /// CheckBox Track SelectedIndex Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboTrack_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            btnClear.Visible = (thisComboBox.SelectedIndex > -1);
        }

        /// <summary>
        /// Opens the website
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicLogo_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("http://www.siteformulatour1600.com/");
        }

        /// <summary>
        /// Reset the combo box to an empty value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClear_Click(object sender, EventArgs e)
        {
            cboTrack.SelectedIndex = -1;
        }

        /// <summary>
        /// Save the changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            _racesController.UpdateRace(GetRaceFromInputs(), chkApplyAll.Checked);
            //_raceAccess.UpdateRace(GetRaceFromInputs(), chkApplyAll.Checked);
            chkApplyAll.Checked = false;

            string message = string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString());
            SetStatus(message);
        }

        /// <summary>
        /// Timer Elapsed Time Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrStatus_Tick(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            tmrStatus.Stop();
        }

        #region -------------------- CUSTOM METHODS --------------------
        /// <summary>
        /// Get a Race object from the inputs
        /// </summary>
        /// <returns>Race Obj</returns>
        private Race GetRaceFromInputs()
        {
            Race newRace = new Race();

            newRace = _currentRace;

            // Overrides track and dates values
            newRace.Date = (chkNoDate.Checked)
                ? (DateTime?)null
                : dtpEventDate.Value;

            newRace.RaceTrack.Id = (cboTrack.SelectedItem == null)
                ? 0
                : ((RaceTrack)cboTrack.SelectedItem).Id;

            return newRace;
        }

        /// <summary>
        /// Resets the controls and values accordingly
        /// </summary>
        private void ResetControlsAndValues()
        {
            btnSave.Enabled = false;

            chkNoDate.Enabled = false;
            chkApplyAll.Enabled = false;

            cboSession.SelectedIndex = -1;

            cboTrack.SelectedIndex = -1;
            cboTrack.Enabled = false;
        }

        /// <summary>
        /// Updates the status strip label
        /// </summary>
        /// <param name="status"></param>
        private void SetStatus(string status)
        {
            if (status != string.Empty)
            {
                tmrStatus.Start();
            }
            tslblState.Text = status;
        }
        #endregion
    }
}
