﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmDrivers : Form
    {
        private bool isEdit = false;
        private string _driverPhotoFile = string.Empty;
        private string _driverPhotoPath = string.Empty;

        private DriversController _driversController = new DriversController();
        private RaceClassesController _raceClassesController = new RaceClassesController();
        private EnginesController _enginesController = new EnginesController();

        private List<Driver> _driversList = new List<Driver>();
        private List<RaceClass> _raceClassesList = new List<RaceClass>();
        private List<Engine> _engineList = new List<Engine>();

        private BindingSource _driverBindingSource;

        private Driver _currentDriver = new Driver();
        private Driver _lastDriver;


        /// <summary>
        /// Constructor
        /// </summary>
        public FrmDrivers(Driver driver = null)
        {
            InitializeComponent();

            try
            {
                _driversList = _driversController.GetDrivers();
                _raceClassesList = _raceClassesController.GetRaceClasses();
                _engineList = _enginesController.GetEngines();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            _driverBindingSource = new BindingSource(_driversList, null);

            cboEngine.DataSource = new BindingSource(_engineList, null);
            cboEngine.DisplayMember = "Name";
            cboEngine.ValueMember = "Id";

            cboClass.DataSource = new BindingSource(_raceClassesList, null);
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Id";

            cboDrivers.DataSource = _driverBindingSource;
            cboDrivers.DisplayMember = "Number";
            cboDrivers.ValueMember = "Id";
            cboDrivers.SelectedIndex = -1;

            cboDrivers.SelectedIndexChanged += OnSelectedIndexChanged;

            if (driver != null)
            {
                DisplayDriver(driver);
                isEdit = true;
            }
        }

        /// <summary>
        /// Load the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmMain_Load(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            if (isEdit) btnEdit.PerformClick();
        }
        #region -------------------------------------------------- COMBOBOX EVENTS --------------------------------------------------
        /// <summary>
        /// When the Combo Box changes value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboDrivers_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _currentDriver = (cboDrivers.SelectedIndex > -1) ? (Driver)cboDrivers.SelectedItem : new Driver();

            if (cboDrivers.SelectedIndex > -1) { DisplayDriver(_currentDriver); }
            else { ClearFields(); }

            btnEdit.Enabled = (cboDrivers.SelectedIndex > -1);
        }
        #endregion

        #region -------------------------------------------------- BUTTON CLICK EVENTS --------------------------------------------------
        /// <summary>
        /// Add a new driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            _currentDriver = new Driver();

            cboDrivers.Visible = false;
            txtDriverNum.Visible = true;

            cboClass.Visible = true;
            txtClass.Visible = false;

            cboEngine.Visible = true;
            txtEngine.Visible = false;

            ToggleReadOnly();

            ClearFields();

            btnEdit.Enabled = false;
            btnSearch.Enabled = false;

            btnCancel.Visible = true;
            btnSave.Visible = true;
            btnSelectPhoto.Enabled = true;

            (sender as Button).Enabled = false;

            AssignLastDriver();

            cboDrivers.SelectedIndex = -1;
            txtDriverNum.Focus();
        }

        /// <summary>
        /// Cancels current behavior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            // Selected Driver
            if (_lastDriver.Id != 0)
            {
                ResetDriversInfos(_lastDriver);
                btnEdit.Enabled = true;
            }

            // New Driver
            else
            {
                ResetDriversInfos();
                cboDrivers.SelectedIndex = -1;
                btnEdit.Enabled = false;
            }

            ToggleReadOnly();

            cboDrivers.Visible = true;
            txtDriverNum.Visible = false;

            txtClass.Visible = true;
            cboClass.Visible = false;

            txtEngine.Visible = true;
            cboEngine.Visible = false;

            (sender as Button).Visible = false;
            btnSave.Visible = false;
            btnSave.Enabled = false;
            btnSelectPhoto.Enabled = false;
            btnRemovePhoto.Visible = false;

            btnAdd.Enabled = true;
            btnSearch.Enabled = true;
        }

        /// <summary>
        /// Edit a driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEdit_Click(object sender, EventArgs e)
        {
            Driver driverEdit = new Driver();
            driverEdit = (Driver)cboDrivers.SelectedItem;

            ToggleReadOnly();

            cboDrivers.Visible = false;
            txtDriverNum.Visible = true;
            txtDriverNum.Text = (cboDrivers.SelectedItem as Driver).Number.ToString();

            AssignLastDriver();

            cboClass.Visible = true;
            txtClass.Visible = false;
            cboClass.SelectedIndex = cboClass.FindStringExact(driverEdit.RaceClass.Name);

            cboEngine.Visible = true;
            txtEngine.Visible = false;
            cboEngine.SelectedIndex = (driverEdit.Engine != null) ? cboEngine.FindStringExact(driverEdit.Engine.Name) : -1;

            btnSave.Visible = true;
            btnSave.Enabled = true;
            btnCancel.Visible = true;
            btnSelectPhoto.Enabled = true;

            (sender as Button).Enabled = false;
            btnAdd.Enabled = false;
            btnSearch.Enabled = false;
            btnRemovePhoto.Visible = ((Bitmap)pboPhoto.Image != null)
                ? !Extension.CompareWithDefault((Bitmap)pboPhoto.Image)
                : false;
        }

        /// <summary>
        /// Saves changes for the driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            _currentDriver.Number = Convert.ToInt32(txtDriverNum.Text);
            _currentDriver.Name = txtName.Text;
            _currentDriver.Hometown = txtHometown.Text;
            _currentDriver.RacingTeam = txtRacingTeam.Text;
            _currentDriver.Car = txtCar.Text;
            _currentDriver.Class_Id = ((RaceClass)cboClass.SelectedItem).Id;
            _currentDriver.Engine_Id = ((Engine)cboEngine.SelectedItem == null) ? new int?() : ((Engine)cboEngine.SelectedItem).Id;
            _currentDriver.IsActive = chkRegistered.Checked;

            if (_currentDriver.Id != 0)
            {
                // Update
                _driversController.UpdateDriver(_currentDriver);
                _driversList.RemoveAt(cboDrivers.SelectedIndex);

                SetStatus(string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString()));
            }
            else
            {
                // Insert
                _currentDriver.Id = _driversController.InsertDriver(_currentDriver);

                SetStatus(string.Format("[{0}] Ajout effectué avec succès", DateTime.Now.ToShortTimeString()));
            }

            UpdateDriversList();

            ToggleReadOnly();

            txtDriverNum.Visible = false;
            cboDrivers.Visible = true;

            cboDrivers.SelectedIndex = _driversList.FindIndex(drv => drv.Id == _currentDriver.Id);

            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnSearch.Enabled = true;

            txtClass.Visible = true;
            cboClass.Visible = false;

            txtEngine.Visible = true;
            cboEngine.Visible = false;

            (sender as Button).Visible = false;
            btnCancel.Visible = false;
            btnSelectPhoto.Enabled = false;
        }

        /// <summary>
        /// Select a photo for the driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelectPhoto_Click(object sender, EventArgs e)
        {
            ofdPhoto.DereferenceLinks = false;
            ofdPhoto.Multiselect = false;
            ofdPhoto.ReadOnlyChecked = true;
            ofdPhoto.ValidateNames = true;

            ofdPhoto.Filter = "Fichier image | *.jpg;*.jpeg;*.jpe;*.jfif;*.png";
            ofdPhoto.InitialDirectory = _driverPhotoPath ?? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ofdPhoto.Title = "Choisissez une image/photo";

            if (ofdPhoto.ShowDialog() == DialogResult.OK)
            {
                _driverPhotoFile = ofdPhoto.FileName;
                _driverPhotoPath = Path.GetDirectoryName(ofdPhoto.FileName);

                pboPhoto.Load(_driverPhotoFile);
            }

            btnRemovePhoto.Visible = true;
        }

        /// <summary>
        /// Search for a specific driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            using (var frm = new frmSearch(_driversList))
            {
                var result = frm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    _currentDriver = frm.ReturnDriver;

                    UpdateDriversList();

                    DisplayDriver(frm.ReturnDriver);
                }
            }
        }

        /// <summary>
        /// Resets to the previous photo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRemovePhoto_Click(object sender, EventArgs e)
        {
            pboPhoto.Image = null;
        }
        #endregion

        #region -------------------------------------------------- TEXTBOX EVENTS --------------------------------------------------
        /// <summary>
        /// Only authorize numeric values into the TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        #endregion

        #region -------------------------------------------------- OTHER EVENTS --------------------------------------------------
        /// <summary>
        /// Timer Elapsed Time Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrStatus_Tick(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            tmrStatus.Stop();
        }

        /// <summary>
        /// Adjust Remove Photo button visibility according to photo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PboPhoto_Paint(object sender, PaintEventArgs e)
        {
            PictureBox thisPictureBox = (PictureBox)sender;

            if (thisPictureBox.Image == null)
            {
                thisPictureBox.ImageLocation = null;
                thisPictureBox.Image = Properties.Resources.NoPhoto;
            }
            btnRemovePhoto.Enabled = ((Bitmap)thisPictureBox.Image != null)
                ? !Extension.CompareWithDefault((Bitmap)thisPictureBox.Image)
                : false;
        }
        #endregion

        #region -------------------------------------------------- CUSTOM METHODS --------------------------------------------------
        /// <summary>
        /// Keeps the last driver from the combo box
        /// </summary>
        private void AssignLastDriver()
        {
            _lastDriver = (cboDrivers.SelectedItem != null)
                ? (Driver)cboDrivers.SelectedItem
                : new Driver();
        }

        /// <summary>
        /// Clear the textboxes for new informations
        /// </summary>
        private void ClearFields()
        {
            txtDriverNum.Text = string.Empty;
            txtName.Text = string.Empty;
            txtHometown.Text = string.Empty;
            txtRacingTeam.Text = string.Empty;
            txtCar.Text = string.Empty;
            txtClass.Text = string.Empty;
            txtEngine.Text = string.Empty;
            pboPhoto.Image = null;
        }

        /// <summary>
        /// Displays the driver supplied in parameter
        /// </summary>
        /// <param name="driver"></param>
        private void DisplayDriver(Driver driver)
        {
            //_currentDriver = driver;
            cboDrivers.SelectedIndex = _driversList.FindIndex(drv => drv.Id == driver.Id);
            txtName.Text = driver.Name;
            txtHometown.Text = driver.Hometown;
            txtRacingTeam.Text = driver.RacingTeam;
            txtCar.Text = driver.Car;
            txtClass.Text = driver.RaceClass.Name;
            txtEngine.Text = (driver.Engine != null) ? driver.Engine.Name : string.Empty;
            chkRegistered.Checked = driver.IsActive.Value;

            //if (File.Exists(driver.ImageLink))
            //{
            //    pboPhoto.Load(driver.ImageLink);
            //}
            //else
            //{
            //    pboPhoto.Image = Properties.Resources.NoPhoto;
            //}

            btnEdit.Enabled = true;
        }

        /// <summary>
        /// Event for the selected index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDrivers.SelectedIndex < 0)
            {
                btnEdit.Enabled = false;
            }
            if (cboDrivers.SelectedItem != null)
            {
                DisplayDriver(cboDrivers.SelectedItem as Driver);
            }
            else
            {
                ClearFields();
            }
        }

        /// <summary>
        /// Event for text changed in textbox name and number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtDriverNum.Text));
        }

        /// <summary>
        /// Resets the textbox to previous driver infos or empty
        /// </summary>
        private void ResetDriversInfos(Driver driver = null)
        {
            if (driver != null)
            {
                DisplayDriver(driver);
            }
            else
            {
                txtCar.Text = string.Empty;
                txtRacingTeam.Text = string.Empty;
                txtClass.Text = string.Empty;
                txtHometown.Text = string.Empty;
                txtName.Text = string.Empty;
                pboPhoto.Image = null;
                cboDrivers.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Toggle the ReadOnly state of the textboxes
        /// </summary>
        private void ToggleReadOnly()
        {
            txtName.ReadOnly = !txtName.ReadOnly;
            txtHometown.ReadOnly = !txtHometown.ReadOnly;
            txtRacingTeam.ReadOnly = !txtRacingTeam.ReadOnly;
            txtCar.ReadOnly = !txtCar.ReadOnly;
            chkRegistered.Enabled = !chkRegistered.Enabled;
        }

        /// <summary>
        /// Updates the status message in the form
        /// </summary>
        /// <param name="status">Status Message (string)</param>
        private void SetStatus(string status)
        {
            if (status != string.Empty)
            {
                tmrStatus.Start();
            }

            tslblState.Text = status;
        }

        /// <summary>
        /// Updates the list and resets the bindings
        /// </summary>
        private void UpdateDriversList()
        {
            _driversList = _driversController.GetDrivers();
            _driverBindingSource.ResetBindings(false);

            cboDrivers.DataSource = null;
            cboDrivers.DataSource = new BindingSource(_driversList, null);
            cboDrivers.DisplayMember = "Number";
            cboDrivers.ValueMember = "Id";
        }
        #endregion
    }
}