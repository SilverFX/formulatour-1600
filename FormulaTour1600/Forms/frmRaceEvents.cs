﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmRaceEvents : Form
    {
        private BindingSource _raceEventBindingSource = new BindingSource();
        private RaceEvent _selectedEvent = new RaceEvent();
        private RaceEventsController _raceEventsController = new RaceEventsController();
        private BindingList<RaceEvent> _eventList = new BindingList<RaceEvent>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FrmRaceEvents()
        {
            InitializeComponent();

            try
            {
                _eventList = _raceEventsController.GetEvents();
                _raceEventBindingSource.DataSource = _raceEventsController.GetEvents();
                
                dgvRaceEvents.AutoGenerateColumns = false;
                dgvRaceEvents.DataSource = _raceEventBindingSource;
                dgvRaceEvents.Columns[clmName.Index].DataPropertyName = "Name";
                dgvRaceEvents.Columns[clmRunsCount.Index].DataPropertyName = "RunsCount";
                _raceEventBindingSource.ResetBindings(false);
                dgvRaceEvents.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmRaceEvents_Load(object sender, EventArgs e)
        {
            UpdateListView();
            dgvRaceEvents.CellValueChanged += DgvRaceEvents_CellValueChanged;
        }

        /// <summary>
        /// Add a new Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            using (var frm = new FrmPrompt("Ajouter un événement", "Quel est le nom de l'événement à ajouter?"))
            {
                var frmResult = frm.ShowDialog();

                if (frmResult == DialogResult.OK)
                {
                    RaceEvent newEvent = new RaceEvent
                    {
                        Name = frm.ReturnValue
                    };

                    _raceEventsController.AddRaceEvent(newEvent);

                    UpdateListView();
                }
            }
        }

        /// <summary>
        /// Delete en entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult =
                MessageBox.Show(
                    "Êtes vous certain de vouloir supprimer cet enregistrement?",
                    "Suppression",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );

            if (dialogResult == DialogResult.Yes)
            {
                _raceEventsController.DeleteRaceEvent(_selectedEvent);

                UpdateListView();
            }
        }

        /// <summary>
        /// ListView Item Selection Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwTracks_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //ListView thisListView = (ListView)sender;

            //if (thisListView.SelectedItems.Count > 0)
            //{
            //    _selectedEvent = (RaceEvent)lvwEvents.SelectedItems[0].Tag;

            //    btnDelete.Enabled = true;
            //    btnConfirm.Enabled = true;

            //    txtNewRaceEvent.Enabled = true;
            //    //txtShortName.Enabled = true;

            //    txtNewRaceEvent.Text = _selectedEvent.Name;
            //    //txtShortName.Text = _selectedEvent.ShortName;
            //}
            //else
            //{
            //    _selectedEvent = null;

            //    btnDelete.Enabled = false;
            //    btnConfirm.Enabled = false;

            //    txtNewRaceEvent.Enabled = false;
            //    //txtShortName.Enabled = false;

            //    txtNewRaceEvent.Text = string.Empty;
            //    //txtShortName.Text = string.Empty;
                
            //}
        }
        #region -------------------- CUSTOM METHODS --------------------
        /// <summary>
        /// Updates the ListView containing the tracks
        /// </summary>
        private void UpdateListView()
        {
            //lvwEvents.Items.Clear();
            //_eventList = _raceEventsController.GetEvents();

            //foreach (RaceEvent raceEvent in _eventList)
            //{
            //    ListViewItem lviRow = new ListViewItem(raceEvent.Name)
            //    {
            //        Tag = raceEvent,
            //        Name = raceEvent.Name
            //    };

            //    lvwEvents.Items.Add(lviRow);
            //}
        }

        /// <summary>
        /// Opens the website
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicFT1600_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("http://www.siteformulatour1600.com/");
        }
        #endregion

        /// <summary>
        /// Updates when DataGridViewRow Changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgvRaceEvents_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var selectedRaceEvent = (RaceEvent)dgvRaceEvents.Rows[e.RowIndex].DataBoundItem;
            _raceEventBindingSource.EndEdit();
            _raceEventsController.UpdateRaceEvent(selectedRaceEvent);
        }
    }
}
