﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmSettings : Form
    {
        private const string GESTION = "Gestion";
        private const string SEASON = "Saison";
        private const string SCORING = "Système de pointage";

        private string _startValue = null;
        private string _initialDesc = string.Empty;
        private int _initialYear = DateTime.Now.Year;

        private List<RacePosition> _rpList = new List<RacePosition>();
        private List<Season> _seasonsList = new List<Season>();

        private RacePositionsController _racePositionsController = new RacePositionsController();
        private SeasonsController _seasonsController = new SeasonsController();

        /// <summary>
        /// Constructor
        /// </summary>
        public FrmSettings()
        {
            _rpList = _racePositionsController.GetAllRacePositions();
            _seasonsList = _seasonsController.GetSeasons();

            InitializeComponent();

            InitTreeView();
            InitDataGridViews();
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmSettings_Load(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
        }

        /// <summary>
        /// Shows the correct group box according to the selection made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreSettings_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (e.Node.Text)
            {
                case GESTION:
                case SEASON:
                    pnlScoring.Visible = false;
                    break;
                case SCORING:
                    pnlScoring.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// Assign the last value of the cell into variable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgvScoring_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            _startValue = dgvScoring[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        /// <summary>
        /// Occurs when the cell changes value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgvScoring_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string editedCellValue = dgvScoring[e.ColumnIndex, e.RowIndex].Value.ToString();

            if (editedCellValue != _startValue)
            {
                int newValue;
                if (int.TryParse(editedCellValue, out  newValue))
                {
                    RacePosition rp = (RacePosition)dgvScoring.Rows[e.RowIndex].Tag;

                    rp.Points = newValue;
                    _racePositionsController.UpdateRacePosition(rp);

                    SetStatus(string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString()));
                }
                else
                {
                    // Reverts the change to old values
                    dgvScoring[e.ColumnIndex, e.RowIndex].Value = _startValue;

                    MessageBox.Show(
                        "La valeur doit être numérique!",
                        "ERREUR",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                }
            }
        }

        /// <summary>
        /// Timer tick Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrStatus_Tick(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            tmrStatus.Stop();
        }

        /// <summary>
        /// Visibility changed of GroupBox Season
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrpSeason_VisibleChanged(object sender, EventArgs e)
        {
            if (grpSeason.Visible)
            {
                UpdateSeasonListView();
            }
        }

        /// <summary>
        /// Item Selection Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwSeasons_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lvwSeasons.SelectedItems.Count > 0)
            {
                int seasonYear = int.Parse(lvwSeasons.SelectedItems[0].SubItems[0].Text);
                string seasonDesc = lvwSeasons.SelectedItems[0].SubItems[1].Text;

                numYear.Enabled = true;
                numYear.ReadOnly = false;
                numYear.Value = seasonYear;

                txtDescription.ReadOnly = false;
                txtDescription.Text = seasonDesc;

                _initialYear = seasonYear;
                _initialDesc = seasonDesc;

                btnDelete.Enabled = true;

                txtDescription.TextChanged += TxtDescription_TextChanged;
                numYear.ValueChanged += NumYear_ValueChanged;
            }
            else
            {
                numYear.Enabled = false;
                numYear.ReadOnly = true;

                txtDescription.ReadOnly = true;

                btnDelete.Enabled = false;

                txtDescription.TextChanged -= TxtDescription_TextChanged;
                numYear.ValueChanged -= NumYear_ValueChanged;
            }
        }

        /// <summary>
        /// TextBox Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtDescription_TextChanged(object sender, EventArgs e)
        {
            btnConfirm.Enabled = true;
            btnCancel.Enabled = true;

            if (numYear.Value == _initialYear)
            {
                if ((sender as TextBox).Text == _initialDesc)
                {
                    btnConfirm.Enabled = false;
                    btnCancel.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Numeric Up Down Value Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumYear_ValueChanged(object sender, EventArgs e)
        {
            btnConfirm.Enabled = true;
            btnCancel.Enabled = true;

            if ((sender as NumericUpDown).Value == _initialYear)
            {
                if (txtDescription.Text == _initialDesc)
                {
                    btnConfirm.Enabled = false;
                    btnCancel.Enabled = false;
                }
            }
        }

        #region -------------------------------------------------- BUTTON CLICK EVENTS --------------------------------------------------
        /// <summary>
        /// Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            lvwSeasons.Enabled = false;
            lvwSeasons.SelectedItems.Clear();

            (sender as Button).Enabled = false;

            btnDelete.Enabled = false;
            btnConfirm.Enabled = true;
            btnCancel.Enabled = true;

            numYear.Enabled = true;

            var last = GetLastListViewItem();
            numYear.Value = int.Parse(last.SubItems[0].Text) + 1;

            txtDescription.ReadOnly = false;
            txtDescription.Clear();
            txtDescription.Focus();
        }
        /// <summary>
        /// Confirm Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            string statusMsg = string.Empty;

            if (lvwSeasons.SelectedItems.Count > 0)
            {
                _seasonsController.UpdateSeason(GetSeasonFromInputs());
                statusMsg = string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString());
            }
            else
            {
                _seasonsController.AddSeason(GetSeasonFromInputs());
                statusMsg = string.Format("[{0}] Ajout effectué avec succès", DateTime.Now.ToShortTimeString());
            }

            SetStatus(statusMsg);
            UpdateSeasonListView();
            DisableEditingControls();
        }

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (lvwSeasons.SelectedItems.Count > 0)
            {
                DialogResult result = MessageBox.Show(
                    "Êtes vous certain de vouloir supprimer cet enregistrement?",
                    "Suppression",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                );

                if (result == DialogResult.OK)
                {
                    _seasonsController.DeleteSeason((Season)lvwSeasons.SelectedItems[0].Tag);
                    SetStatus(string.Format("[{0}] Suppression effectuée avec succès", DateTime.Now.ToShortTimeString()));
                }
            }

            UpdateSeasonListView();
            DisableEditingControls();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            numYear.Value = _initialYear;
            txtDescription.Text = _initialDesc;

            DisableEditingControls();
        }

        /// <summary>
        /// Closes the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnQuit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
        #endregion

        #region -------------------------------------------------- CUSTOM METHODS --------------------------------------------------
        /// <summary>
        /// Initialize items in the TreeView
        /// </summary>
        private void InitTreeView()
        {
            treSettings.BeginUpdate();

            treSettings.Nodes.Add(GESTION);

            TreeNode defaultTreeNode = treSettings.Nodes[0].Nodes.Add(SEASON);
            treSettings.Nodes[0].Nodes.Add(SCORING);
            treSettings.Focus();
            treSettings.SelectedNode = defaultTreeNode;

            treSettings.EndUpdate();
        }

        /// <summary>
        /// Gets the last item of the listview
        /// </summary>
        /// <returns>Last ListView Item</returns>
        private ListViewItem GetLastListViewItem()
        {
            var lastItem = Enumerable.Empty<ListViewItem>();

            if (lvwSeasons.Items.Count > 0)
            {
                lastItem = lvwSeasons.Items.OfType<ListViewItem>();
            }

            return lastItem.LastOrDefault();
        }

        /// <summary>
        /// Returns a season object from the inputs
        /// </summary>
        /// <returns>New Season Object</returns>
        private Season GetSeasonFromInputs()
        {
            Season newSeason = new Season();

            if (lvwSeasons.SelectedItems.Count > 0)
            {
                newSeason.Id = int.Parse(lvwSeasons.SelectedItems[0].Name);
            }

            newSeason.SeasonYear = (int)numYear.Value;
            newSeason.Description = txtDescription.Text;

            return newSeason;
        }

        /// <summary>
        /// Set the buttons states after editing
        /// </summary>
        private void DisableEditingControls()
        {
            btnCancel.Enabled = false;
            btnConfirm.Enabled = false;
            btnDelete.Enabled = false;

            btnAdd.Enabled = true;

            numYear.Enabled = false;
            txtDescription.ReadOnly = true;

            lvwSeasons.Enabled = true;
        }

        /// <summary>
        /// Initialize the data in the DataGridView component
        /// </summary>
        private void InitDataGridViews()
        {
            _rpList.ForEach(i =>
            {
                // Race Position DataGridView
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dgvScoring);

                row.Tag = i;
                row.Cells[0].Value = i.FinalPosition;
                row.Cells[1].Value = i.Points;

                dgvScoring.Rows.Add(row);
            });

            dgvScoring.CellValueChanged += DgvScoring_CellValueChanged;
        }
        /// <summary>
        /// Updates the ListView
        /// </summary>
        private void UpdateSeasonListView()
        {
            lvwSeasons.Items.Clear();

            _seasonsList = _seasonsController.GetSeasons();

            foreach (Season season in _seasonsList)
            {
                ListViewItem lviRow = new ListViewItem(season.SeasonYear.ToString())
                {
                    Tag = season,
                    Name = season.Id.ToString()
                };
                lviRow.SubItems.Add(season.Description);

                lvwSeasons.Items.Add(lviRow);
            }
            numYear.Minimum = 2000;
            numYear.Maximum = 2100;
            numYear.Value = DateTime.Now.Year;
        }

        /// <summary>
        /// Changes the status strip message
        /// </summary>
        /// <param name="status"></param>
        private void SetStatus(string status)
        {
            if (status != string.Empty)
            {
                tmrStatus.Start();
            }

            tslblMessage.Text = status;
        }
        #endregion

        private void LvwEvents_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

        }
    }
}
