﻿namespace FormulaTour1600
{
    partial class frmDriverDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDriverDetails));
            this.grpDriver = new System.Windows.Forms.GroupBox();
            this.txtEngine = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.grpPhoto = new System.Windows.Forms.GroupBox();
            this.pboPhoto = new System.Windows.Forms.PictureBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.txtCar = new System.Windows.Forms.TextBox();
            this.txtDriverNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.txtRacingTeam = new System.Windows.Forms.TextBox();
            this.txtHometown = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvwDetails = new System.Windows.Forms.ListView();
            this.clhEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPoints = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpDriver.SuspendLayout();
            this.grpPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDriver
            // 
            this.grpDriver.Controls.Add(this.txtEngine);
            this.grpDriver.Controls.Add(this.label6);
            this.grpDriver.Controls.Add(this.txtClass);
            this.grpDriver.Controls.Add(this.label7);
            this.grpDriver.Controls.Add(this.grpPhoto);
            this.grpDriver.Controls.Add(this.btnQuit);
            this.grpDriver.Controls.Add(this.txtCar);
            this.grpDriver.Controls.Add(this.txtDriverNum);
            this.grpDriver.Controls.Add(this.label4);
            this.grpDriver.Controls.Add(this.lblNumber);
            this.grpDriver.Controls.Add(this.txtRacingTeam);
            this.grpDriver.Controls.Add(this.txtHometown);
            this.grpDriver.Controls.Add(this.txtName);
            this.grpDriver.Controls.Add(this.label3);
            this.grpDriver.Controls.Add(this.label2);
            this.grpDriver.Controls.Add(this.label1);
            this.grpDriver.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDriver.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDriver.Location = new System.Drawing.Point(0, 0);
            this.grpDriver.Name = "grpDriver";
            this.grpDriver.Size = new System.Drawing.Size(594, 279);
            this.grpDriver.TabIndex = 0;
            this.grpDriver.TabStop = false;
            this.grpDriver.Text = "Informations du pilote";
            // 
            // txtEngine
            // 
            this.txtEngine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEngine.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEngine.Location = new System.Drawing.Point(115, 214);
            this.txtEngine.Name = "txtEngine";
            this.txtEngine.ReadOnly = true;
            this.txtEngine.Size = new System.Drawing.Size(120, 25);
            this.txtEngine.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 25);
            this.label6.TabIndex = 18;
            this.label6.Text = "Moteur : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtClass
            // 
            this.txtClass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClass.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClass.Location = new System.Drawing.Point(115, 183);
            this.txtClass.Name = "txtClass";
            this.txtClass.ReadOnly = true;
            this.txtClass.Size = new System.Drawing.Size(120, 25);
            this.txtClass.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 25);
            this.label7.TabIndex = 16;
            this.label7.Text = "Classe : ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpPhoto
            // 
            this.grpPhoto.Controls.Add(this.pboPhoto);
            this.grpPhoto.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPhoto.Location = new System.Drawing.Point(382, 2);
            this.grpPhoto.Name = "grpPhoto";
            this.grpPhoto.Size = new System.Drawing.Size(214, 219);
            this.grpPhoto.TabIndex = 15;
            this.grpPhoto.TabStop = false;
            // 
            // pboPhoto
            // 
            this.pboPhoto.Location = new System.Drawing.Point(6, 14);
            this.pboPhoto.Name = "pboPhoto";
            this.pboPhoto.Size = new System.Drawing.Size(196, 196);
            this.pboPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pboPhoto.TabIndex = 0;
            this.pboPhoto.TabStop = false;
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(496, 232);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(92, 38);
            this.btnQuit.TabIndex = 12;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // txtCar
            // 
            this.txtCar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCar.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCar.Location = new System.Drawing.Point(115, 152);
            this.txtCar.Name = "txtCar";
            this.txtCar.ReadOnly = true;
            this.txtCar.Size = new System.Drawing.Size(261, 25);
            this.txtCar.TabIndex = 9;
            // 
            // txtDriverNum
            // 
            this.txtDriverNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDriverNum.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDriverNum.Location = new System.Drawing.Point(115, 30);
            this.txtDriverNum.MaxLength = 3;
            this.txtDriverNum.Name = "txtDriverNum";
            this.txtDriverNum.ReadOnly = true;
            this.txtDriverNum.Size = new System.Drawing.Size(46, 25);
            this.txtDriverNum.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Voiture : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNumber
            // 
            this.lblNumber.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.Location = new System.Drawing.Point(0, 30);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(116, 23);
            this.lblNumber.TabIndex = 0;
            this.lblNumber.Text = "Numéro : ";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRacingTeam
            // 
            this.txtRacingTeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRacingTeam.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRacingTeam.Location = new System.Drawing.Point(115, 123);
            this.txtRacingTeam.Name = "txtRacingTeam";
            this.txtRacingTeam.ReadOnly = true;
            this.txtRacingTeam.Size = new System.Drawing.Size(261, 25);
            this.txtRacingTeam.TabIndex = 7;
            // 
            // txtHometown
            // 
            this.txtHometown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHometown.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHometown.Location = new System.Drawing.Point(115, 92);
            this.txtHometown.Name = "txtHometown";
            this.txtHometown.ReadOnly = true;
            this.txtHometown.Size = new System.Drawing.Size(261, 25);
            this.txtHometown.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(115, 61);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(261, 25);
            this.txtName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 23);
            this.label3.TabIndex = 6;
            this.label3.Text = "Équipe : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Provenance : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom du pilote : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lvwDetails
            // 
            this.lvwDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clhEvent,
            this.clhPosition,
            this.clhPoints});
            this.lvwDetails.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lvwDetails.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwDetails.FullRowSelect = true;
            this.lvwDetails.Location = new System.Drawing.Point(0, 285);
            this.lvwDetails.Name = "lvwDetails";
            this.lvwDetails.Size = new System.Drawing.Size(594, 297);
            this.lvwDetails.TabIndex = 1;
            this.lvwDetails.UseCompatibleStateImageBehavior = false;
            this.lvwDetails.View = System.Windows.Forms.View.Details;
            this.lvwDetails.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.LvwDetails_ColumnWidthChanging);
            // 
            // clhEvent
            // 
            this.clhEvent.Text = "Événement";
            this.clhEvent.Width = 381;
            // 
            // clhPosition
            // 
            this.clhPosition.Text = "Position";
            this.clhPosition.Width = 66;
            // 
            // clhPoints
            // 
            this.clhPoints.Text = "Points";
            this.clhPoints.Width = 64;
            // 
            // frmDriverDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 582);
            this.Controls.Add(this.lvwDetails);
            this.Controls.Add(this.grpDriver);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDriverDetails";
            this.Text = "frmDriverDetails";
            this.grpDriver.ResumeLayout(false);
            this.grpDriver.PerformLayout();
            this.grpPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pboPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDriver;
        private System.Windows.Forms.TextBox txtCar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRacingTeam;
        private System.Windows.Forms.TextBox txtHometown;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDriverNum;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ListView lvwDetails;
        private System.Windows.Forms.ColumnHeader clhEvent;
        private System.Windows.Forms.ColumnHeader clhPoints;
        private System.Windows.Forms.ColumnHeader clhPosition;
        private System.Windows.Forms.GroupBox grpPhoto;
        private System.Windows.Forms.PictureBox pboPhoto;
        private System.Windows.Forms.TextBox txtEngine;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.Label label7;
    }
}