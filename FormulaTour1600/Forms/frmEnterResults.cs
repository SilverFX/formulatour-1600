﻿using FormulaTour1600.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmEnterResults : Form
    {
        private bool _hasDuplicates = false;
        private bool _polePositionSet = false;

        private RaceEvent _currentRaceEvent = new RaceEvent();
        private RaceEventsController _raceEventsController = new RaceEventsController();

        private RaceSession _currentRaceSession = new RaceSession();
        private RaceSessionsController _raceSessionsController = new RaceSessionsController();

        private Season _currentSeason = new Season();
        private SeasonsController _seasonsController = new SeasonsController();

        private Race _currentRace = new Race();
        private RacesController _racesController = new RacesController();

        private Driver _currentDriver = new Driver();
        private DriversController _driversController = new DriversController();

        private RacePosition _currentPosition = new RacePosition();
        private RacePositionsController _racePositionsController = new RacePositionsController();

        private RaceResultsController _raceResultsController = new RaceResultsController();

        private List<RaceEvent> _raceEventsList = new List<RaceEvent>();
        private List<RaceSession> _sessionsList = new List<RaceSession>();
        private List<Season> _seasonList = new List<Season>();
        private List<RacePosition> _positionsList = new List<RacePosition>();
        private List<Driver> _driverList = new List<Driver>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FrmEnterResults()
        {
            InitializeComponent();

            try
            {
                _raceEventsList = _raceEventsController.GetEvents().ToList(); // _raceEventAccess.GetEvents();
                _sessionsList = _raceSessionsController.GetSessions(); // _raceSessionAccess.GetSessions();
                _seasonList = _seasonsController.GetSeasons(); // _seasonAccess.GetSeasons();
                _driverList = _driversController.GetDrivers(); // _driverAccess.GetDrivers();

                cboRaceEvents.DataSource = new BindingSource(_raceEventsList, null);
                cboRaceEvents.DisplayMember = "Name";
                cboRaceEvents.ValueMember = "Id";
                cboRaceEvents.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #region -------------------------------------------------- FORM EVENTS --------------------------------------------------
        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmEnterResults_Load(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            try
            {
                // Check for last settings
                if (Settings.Default.LastEventId != 0)
                {
                    cboRaceEvents.SelectedIndex =
                        _raceEventsList.IndexOf(_raceEventsList.Where(re => re.Id == Settings.Default.LastEventId).FirstOrDefault());
                    CboRaceEvents_SelectionChangeCommitted(cboRaceEvents, new EventArgs());
                }

                cboSeason.SelectedIndex =
                    (Settings.Default.LastEventId != 0)
                        ? _seasonList.IndexOf(_seasonList.Where(s => s.Id == Settings.Default.LastSeasonId).FirstOrDefault())
                        : -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Save last choices when closing the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmResults_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Saves the user settings
            Settings.Default.LastEventId = (cboRaceEvents.SelectedItem != null) ? ((RaceEvent)cboRaceEvents.SelectedItem).Id : 0;
            Settings.Default.LastSeasonId = (cboSeason.SelectedItem != null) ? ((Season)cboSeason.SelectedItem).Id : 0;
            Settings.Default.Save();
        }
        #endregion

        #region -------------------------------------------------- COMBOBOX METHODS --------------------------------------------------
        /// <summary>
        /// Race Event Selection Change Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboRaceEvents_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if ((RaceEvent)thisComboBox.SelectedItem != _currentRaceEvent)
            {
                // Reset
                ResetControlsAndValues();
            }

            _currentRaceEvent = (RaceEvent)thisComboBox.SelectedItem;
            CboSession_EnabledChanged(cboSession, null);
            cboSeason.Enabled = true;
        }

        /// <summary>
        /// ComboBox Season Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = new BindingSource(_seasonList, null);
                thisComboBox.DisplayMember = "SeasonYear";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex =
                    (_seasonList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year) > 0) ? _seasonList.FindIndex(sea => sea.SeasonYear == DateTime.Now.Year) : -1;
            }
        }

        /// <summary>
        /// ComboBox Season SelectedIndex Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.SelectedIndex < 0)
            {
                cboSession.Enabled = false;
            }
            else
            {
                cboSession.Enabled = true;
                _currentSeason = (Season)thisComboBox.SelectedItem;
            }

            ResetControlsAndValues();
        }

        /// <summary>
        /// ComboBox Session Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.Enabled)
            {
                thisComboBox.DataSource = (_currentRaceEvent.RunsCount.HasValue) 
                    ? new BindingSource(_sessionsList.Take(_currentRaceEvent.RunsCount.Value), null) 
                    : new BindingSource(_sessionsList, null);
                thisComboBox.DisplayMember = "LongName";
                thisComboBox.ValueMember = "Id";
                thisComboBox.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// ComboBox Session Selection Change Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboSession_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            _currentRaceSession = (RaceSession)thisComboBox.SelectedItem;

            _currentRace = _racesController.GetRace(_currentRaceEvent.Id, _currentSeason.Id, _currentRaceSession.Id);

            // Create if no race found
            if (_currentRace == null)
            {
                _currentRace = _racesController.InsertRace(_currentRaceEvent.Id, _currentSeason.Id, _currentRaceSession.Id);

            }

            UpdateDriversList();

            UpdateListView();

            if (lvwPreview.Items.Count == 0)
            {
                using (var frm = new frmSearch(_driverList, "Qui a effectué la pôle position?"))
                {
                    var frmResult = frm.ShowDialog();

                    if (frmResult == DialogResult.OK)
                    {
                        Driver driver = frm.ReturnDriver;

                        int index = _driverList.FindIndex(drv => drv.Id == driver.Id);
                        cboDrivers.SelectedIndex = index;
                        CboDrivers_SelectionChangeCommitted(cboDrivers, new EventArgs());

                        chkPole.Visible = true;
                        chkPole.Enabled = true;
                        chkPole.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// ComboBox Drivers Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboDrivers_EnabledChanged(object sender, EventArgs e)
        {
            btnSearchDriver.Enabled = cboDrivers.Enabled;
            btnAddQuick.Enabled = cboDrivers.Enabled;
        }

        /// <summary>
        /// ComboBox Drivers Selection Change Committed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboDrivers_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                ComboBox thisComboBox = (ComboBox)sender;

                _positionsList = _racePositionsController.GetAvailablePositionsForRace(_currentRace);

                _currentDriver = (Driver)thisComboBox.SelectedItem;

                if (_currentDriver == null)
                {
                    using (var frm = new FrmPromptNewDriver(cboDrivers.Text))
                    {
                        var frmResult = frm.ShowDialog();

                        if (frmResult == DialogResult.OK)
                        {
                            UpdateDriversList();

                            Driver newDriver = frm.ReturnDriver;

                            cboDrivers.SelectedIndex = _driverList.FindIndex(d => d.Number == newDriver.Number);

                            _currentDriver = newDriver;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                txtDriver.Text = _currentDriver.Name;

                cboPosition.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        /// <summary>
        /// ComboBox Drivers Selected Index Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboDrivers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            txtDriver.Text = (thisComboBox.SelectedIndex < 0)
                ? string.Empty
                : ((Driver)thisComboBox.SelectedItem).Name;
        }

        /// <summary>
        /// ComboBox Position Enabled Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboPosition_EnabledChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            thisComboBox.DataSource = new BindingSource(_positionsList, null);
            thisComboBox.DisplayMember = "FinalPosition";
            thisComboBox.ValueMember = "Id";
            thisComboBox.SelectedIndex = 0;

            btnSave.Enabled = thisComboBox.Enabled && thisComboBox.SelectedIndex > -1;
        }

        /// <summary>
        /// Selected Index Changed Event for ComboBox Position
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisComboBox = (ComboBox)sender;

            if (thisComboBox.SelectedIndex >= -1)
            {
                _currentPosition = (RacePosition)cboPosition.SelectedItem;

                chkPole.Enabled = chkPole.Visible;
                chkPole.Checked = false;
            }
        }
        #endregion

        #region -------------------------------------------------- LISTVIEW EVENT METHODS --------------------------------------------------
        /// <summary>
        /// Prevent resizing of the columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwPreview_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvwPreview.Columns[e.ColumnIndex].Width;
        }

        /// <summary>
        /// Edit the result when double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwPreview_DoubleClick(object sender, EventArgs e)
        {
            // Open edit form
            if (lvwPreview.SelectedItems.Count > 0)
            {
                using (var frm = new FrmEditResult((RaceResult)lvwPreview.SelectedItems[0].Tag))
                {
                    var frmResult = frm.ShowDialog();
                    string message = string.Empty;

                    switch (frmResult)
                    {
                        case DialogResult.OK:
                            message = string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString());
                            break;
                        case DialogResult.Yes:
                            message = string.Format("[{0}] Suppression effectuée avec succès", DateTime.Now.ToShortTimeString());
                            break;
                        default:
                            break;

                    }

                    UpdateListView();
                    SetStatus(message);
                }
            }
        }
        /// <summary>
        /// Right click event for the listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwPreview_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvwPreview.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    ctxEditResult.Show(Cursor.Position);
                }
            }
        }
        /// <summary>
        /// Click to edit the result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsmiResult_Click(object sender, EventArgs e)
        {
            LvwPreview_DoubleClick(lvwPreview, new EventArgs());
        }

        /// <summary>
        /// Click to edit the driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsmiDriver_Click(object sender, EventArgs e)
        {
            // Open edit form
            if (lvwPreview.SelectedItems.Count > 0)
            {
                RaceResult result = (RaceResult)lvwPreview.SelectedItems[0].Tag;

                using (var frm = new FrmDrivers(result.Driver))
                {
                    frm.ShowDialog();

                    UpdateListView();
                }

                //using (var frm = new frmEditResult((Result)lvwPreview.SelectedItems[0].Tag))
                //{
                //    var frmResult = frm.ShowDialog();
                //    string message = string.Empty;

                //    switch (frmResult)
                //    {
                //        case DialogResult.OK:
                //            message = string.Format("[{0}] Modification effectuée avec succès", DateTime.Now.ToShortTimeString());
                //            break;
                //        case DialogResult.Yes:
                //            message = string.Format("[{0}] Suppression effectuée avec succès", DateTime.Now.ToShortTimeString());
                //            break;
                //        default:
                //            break;

                //    }

                //    UpdateListView(true);
                //    SetStatus(message);
                //}
            }
        }
        /// <summary>
        /// Directly delete the result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsmiDelete_Click(object sender, EventArgs e)
        {
            if (lvwPreview.SelectedItems.Count > 0)
            {
                RaceResult result = (RaceResult)lvwPreview.SelectedItems[0].Tag;

                DialogResult dialogResult =
                MessageBox.Show(
                    "Êtes vous certain de vouloir supprimer cet enregistrement?",
                    "Suppression",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );

                if (dialogResult == DialogResult.Yes)
                {
                    RaceResultsController raceResultsController = new RaceResultsController();
                    raceResultsController.DeleteResult(result);

                    string message = string.Format("[{0}] Suppression effectuée avec succès", DateTime.Now.ToShortTimeString());
                    SetStatus(message);

                    UpdateListView();
                }
            }
        }
        #endregion  

        #region -------------------------------------------------- OTHER EVENT METHODS --------------------------------------------------
        /// <summary>
        /// Opens the official website by double clicking the logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicLogo_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("http://www.siteformulatour1600.com/");
        }

        /// <summary>
        /// Timer Elapsed Time Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrStatus_Tick(object sender, EventArgs e)
        {
            SetStatus(string.Empty);
            tmrStatus.Stop();
        }

        /// <summary>
        /// TextBox Text Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtDriver_TextChanged(object sender, EventArgs e)
        {
            TextBox thisTextBox = (TextBox)sender;
            btnViewProfile.Enabled = (thisTextBox.Text != string.Empty);
        }
        #endregion

        #region -------------------------------------------------- BUTTON EVENTS --------------------------------------------------
        /// <summary>
        /// Quickly add a driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddQuick_Click(object sender, EventArgs e)
        {
            using (var frm = new FrmPromptNewDriver(cboDrivers.Text))
            {
                DialogResult frmResult = frm.ShowDialog();

                if (frmResult == DialogResult.OK)
                {
                    Driver driver = frm.ReturnDriver;
                    int index = _driverList.FindIndex(drv => drv.Id == driver.Id);
                    cboDrivers.SelectedIndex = index;
                    CboDrivers_SelectionChangeCommitted(cboDrivers, new EventArgs());
                }
            }
        }
        /// <summary>
        /// Consult the race reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLookup_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(
                new FrmStandingsDetails(
                    ((RaceEvent)cboRaceEvents.SelectedItem).Id,
                    ((Season)cboSeason.SelectedItem).Id,
                    ((RaceSession)cboSession.SelectedItem).Id
                )
            );
        }

        /// <summary>
        /// Enter the info into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            ((Button)sender).Enabled = false;

            // Save
            _raceResultsController.InsertResult(_currentRace.Id, _currentDriver.Id, _currentPosition.Id, chkPole.Checked);

            string statusMessage = string.Format("[{0}] Ajout effectué avec succès", DateTime.Now.ToShortTimeString());

            SetStatus(statusMessage);

            UpdateDriversList();

            UpdateListView();
        }

        /// <summary>
        /// Search Driver button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearchDriver_Click(object sender, EventArgs e)
        {
            using (var frm = new frmSearch(_driverList, null, cboDrivers.Text))
            {
                var result = frm.ShowDialog();

                if (result == DialogResult.OK)
                {
                    Driver driver = frm.ReturnDriver;

                    UpdateDriversList();
                    int index = _driverList.FindIndex(drv => drv.Id == driver.Id);

                    cboDrivers.SelectedIndex = index;
                    CboDrivers_SelectionChangeCommitted(cboDrivers, new EventArgs());
                }
            }
        }

        /// <summary>
        /// View the profile of the driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnViewProfile_Click(object sender, EventArgs e)
        {
            using (var frm = new frmDriverDetails(cboDrivers.SelectedItem as Driver, false))
            {
                frm.ShowDialog();
            }
        }
        #endregion

        #region -------------------------------------------------- CUSTOM METHODS --------------------------------------------------
        /// <summary>
        /// Reset values and disable controls after the session combobox
        /// </summary>
        private void ResetControlsAndValues()
        {
            lvwPreview.Items.Clear();

            cboSession.SelectedIndex = -1;

            cboDrivers.SelectedIndex = -1;
            cboDrivers.Enabled = false;

            cboPosition.SelectedIndex = -1;
            cboPosition.Enabled = false;

            chkPole.Checked = false;
        }

        /// <summary>
        /// Reset all values from the drivers to the end
        /// </summary>
        private void UpdateDriversList()
        {
            try
            {
                _driverList = _driversController.GetAvailableDrivers(_currentRace);

                txtDriver.Text = string.Empty;

                cboDrivers.Enabled = true;
                cboDrivers.DataSource = new BindingSource(_driverList, null);

                cboDrivers.DisplayMember = "Number";
                cboDrivers.ValueMember = "Id";
                cboDrivers.SelectedIndex = -1;

                cboPosition.SelectedIndex = -1;
                cboPosition.Enabled = false;

                chkPole.Checked = false;
                chkPole.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Updates the status strip label
        /// </summary>
        /// <param name="status"></param>
        private void SetStatus(string status)
        {
            if (status != string.Empty)
            {
                tmrStatus.Start();
            }
            tslblState.Text = status;
        }

        /// <summary>
        /// Fills the List View
        /// </summary>
        private void UpdateListView()
        {
            _hasDuplicates = false; // Reinitialize the var
            _polePositionSet = false;
            lvwPreview.Items.Clear();

            try
            {
                List<RaceResult> results = _raceResultsController.GetResults(_currentRace); // _resultAccess.GetResults(_currentRace.Id);

                foreach (RaceResult result in results.OrderBy(r => r.RacePosition.FinalPosition.Length).ThenBy(r => r.RacePosition.FinalPosition))
                {
                    ListViewItem lviRow = new ListViewItem(result.RacePosition.FinalPosition)
                    {
                        Tag = result,
                        Name = result.Id.ToString()
                    };
                    lviRow.SubItems.Add(result.Driver.Number.ToString());
                    lviRow.SubItems.Add(result.Driver.Name);
                    lviRow.SubItems.Add(result.Driver.RaceClass.Name);

                    if (result.HasPolePosition.Value)
                    {
                        lviRow.SubItems.Add("X");
                        lviRow.BackColor = Color.Lime;
                    }
                    else
                    {
                        lviRow.SubItems.Add(string.Empty);
                    }

                    var points = result.RacePosition.Points;
                    if (result.HasPolePosition.Value) points++;
                    lviRow.SubItems.Add(points.ToString());


                    lvwPreview.Items.Add(lviRow);
                }

                // Check for duplicates and show them in red in the ListView
                foreach (ListViewItem item in lvwPreview.Items)
                {
                    RaceResult currentResult = (RaceResult)item.Tag;

                    RaceResult polePositionResult =
                        (from subItem in lvwPreview.Items.Cast<ListViewItem>()
                         where ((RaceResult)subItem.Tag).HasPolePosition.Value
                         select (RaceResult)subItem.Tag).FirstOrDefault();

                    RaceResult notClassifiedResult = (from subItem in lvwPreview.Items.Cast<ListViewItem>()
                                                      where (item.Text.Equals("DNF") || item.Text.Equals("DNS") || item.Text.Equals("DSQ"))
                                                      select (RaceResult)subItem.Tag).FirstOrDefault();

                    // Pole Position
                    if (polePositionResult != null)
                    {
                        if (_polePositionSet == false)
                        {
                            _polePositionSet = true;
                        }
                    }

                    // Not classified drivers display
                    if (notClassifiedResult != null)
                    {
                        item.BackColor = Color.Yellow;
                    }

                    string duplicates =
                        (from subItem in lvwPreview.Items.Cast<ListViewItem>()
                         where subItem.Text.Equals(item.Text) && !subItem.Text.Equals("DNF") && !subItem.Text.Equals("DNS") && !subItem.Text.Equals("DSQ")
                            || ((RaceResult)subItem.Tag).Driver.Number.Equals(currentResult.Driver.Number)
                            || ((RaceResult)subItem.Tag).Driver.Name.Equals(currentResult.Driver.Name)
                            || (((RaceResult)subItem.Tag).HasPolePosition.Value && ((RaceResult)subItem.Tag).HasPolePosition.Equals(currentResult.HasPolePosition))
                         where !lvwPreview.Items.IndexOf(subItem).Equals(lvwPreview.Items.IndexOf(item))
                         select subItem.Text).FirstOrDefault();

                    if (!string.IsNullOrEmpty(duplicates))
                    {
                        if (_hasDuplicates == false)
                        {
                            _hasDuplicates = true;
                        }

                        item.BackColor = Color.Red;
                        item.ForeColor = Color.White;
                    }

                    chkPole.Visible = (!_polePositionSet);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
