﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class frmMain : Form
    {
        private RacesController _racesController = new RacesController();
        private DriversController _driversController = new DriversController();
        private RaceResultsController _raceResultsController = new RaceResultsController();

        /// <summary>
        /// Constructor
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Access to Drivers Management
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDrivers_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmDrivers());
        }

        /// <summary>
        /// Access to Results Management
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnResults_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmEnterResults());
            //Extension.OpenForm(new frmResults());
        }

        /// <summary>
        /// Access to Standings (Read-Only)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStandings_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmStandings());
        }

        /// <summary>
        /// Access to the Results Reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReports_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmStandingsDetails());
        }

        /// <summary>
        /// Go to the Formula Tour 1600 website when double clicking on the logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicFT1600_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("http://www.siteformulatour1600.com/");
        }

        /// <summary>
        /// Exit the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Opens the Settings Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSettings_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmSettings());
        }

        /// <summary>
        /// Opens the Excel Export Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnExcelExport_Click(object sender, EventArgs e)
        {
            // Default filename
            sfdExcel.FileName = "Classeur1.xls";

            if (sfdExcel.ShowDialog() == DialogResult.OK)
            {
                FileInfo file = new FileInfo(sfdExcel.FileName);

                if (!Extension.IsFileLocked(file) || !File.Exists(sfdExcel.FileName))
                {
                    using (FrmWaitExport frm = new FrmWaitExport(ExportData))
                    {
                        frm.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show(
                         string.Format(
                             @"Le fichier {0} est déjà en cours d'utilisation!{1}Veuillez fermer le fichier afin de pouvoir le sauvegarder.",
                             sfdExcel.FileName,
                             Environment.NewLine
                         ),
                         "ERREUR: Fichier déjà en utilisation",
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error
                     );
                }
            }
        }

        /// <summary>
        /// Opens the events management
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEvents_Click(object sender, EventArgs e)
        {
            Extension.OpenForm(new FrmRaceEvents());
        }

        /// <summary>
        /// Export Data from database to Excel
        /// </summary>
        private void ExportData()
        {
            List<Race> racesList = _racesController.GetRaces();
            List<Driver> driversList = _driversController.GetActiveDrivers();
            List<RaceResult> resultsList = _raceResultsController.GetResults();

            ExcelExport excelExport = new ExcelExport();
            excelExport.CreateStandingsFile(sfdExcel.FileName, racesList, resultsList, driversList);

            // Close the dialog after work is done
            DialogResult = DialogResult.OK;
        }
    }
}
