﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmWaitExport : Form
    {
        public Action Worker { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="worker"></param>
        public FrmWaitExport(Action worker)
        {
            InitializeComponent();
            if (worker == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                Worker = worker;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
