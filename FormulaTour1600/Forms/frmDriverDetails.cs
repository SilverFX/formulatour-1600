﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class frmDriverDetails : Form
    {
        private List<RaceResult> _resultsList = new List<RaceResult>();
        private RaceResultsController _raceResultsController = new RaceResultsController();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="driver">Driver obj</param>
        public frmDriverDetails(Driver driver, bool showResultsDetails)
        {
            InitializeComponent();

            if (showResultsDetails)
            {
                Height = 620;
                _resultsList = _raceResultsController.GetDriverResults(driver);
                lvwDetails.Visible = true;
            }
            else
            {
                Height = 258;
                lvwDetails.Visible = false;
            }

            DisplayDriver(driver);
        }

        /// <summary>
        /// Displays the driver
        /// </summary>
        /// <param name="driver"></param>
        private void DisplayDriver(Driver driver)
        {
            Text = "Consultation du pilote: " + driver.Name;

            txtDriverNum.Text = driver.Number.ToString();
            txtName.Text = driver.Name;
            txtHometown.Text = driver.Hometown;
            txtRacingTeam.Text = driver.RacingTeam;
            txtCar.Text = driver.Car;
            txtEngine.Text = "< moteur >";
            txtClass.Text = driver.RaceClass.Name;
            //pboPhoto.Image = (File.Exists(driver.ImageLink))
            //    ? Image.FromFile(driver.ImageLink)
            //    : Properties.Resources.NoPhoto;

            foreach (RaceResult result in _resultsList)
            {
                ListViewItem lviRow = new ListViewItem(string.Concat(result.Race.RaceEvent.Name, " - ", result.Race.RaceSession.ShortName))
                {
                    Tag = result
                };
                lviRow.SubItems.Add(result.RacePosition.FinalPosition);
                lviRow.SubItems.Add(result.RacePosition.Points.ToString());

                lvwDetails.Items.Add(lviRow);
            }
        }
        /// <summary>
        /// Prevent resizing of the columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwDetails_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            ListView thisListView = (ListView)sender;

            e.Cancel = true;
            e.NewWidth = thisListView.Columns[e.ColumnIndex].Width;
        }
    }
}
