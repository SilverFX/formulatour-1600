﻿namespace FormulaTour1600
{
    partial class frmRaceTracks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRaceTracks));
            this.btnQuit = new System.Windows.Forms.Button();
            this.tltActions = new System.Windows.Forms.ToolTip(this.components);
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lvwTracks = new System.Windows.Forms.ListView();
            this.grpTracks = new System.Windows.Forms.GroupBox();
            this.txtNewTrack = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.picFT1600 = new System.Windows.Forms.PictureBox();
            this.grpTracks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).BeginInit();
            this.SuspendLayout();
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(251, 255);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(88, 42);
            this.btnQuit.TabIndex = 35;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // tltActions
            // 
            this.tltActions.IsBalloon = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(299, 24);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(32, 32);
            this.btnAdd.TabIndex = 38;
            this.tltActions.SetToolTip(this.btnAdd, "Ajouter");
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(299, 62);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.TabIndex = 40;
            this.tltActions.SetToolTip(this.btnDelete, "Supprimer");
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Enabled = false;
            this.btnConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Image")));
            this.btnConfirm.Location = new System.Drawing.Point(299, 204);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(32, 32);
            this.btnConfirm.TabIndex = 39;
            this.tltActions.SetToolTip(this.btnConfirm, "Appliquer");
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.BtnConfirm_Click);
            // 
            // lvwTracks
            // 
            this.lvwTracks.Location = new System.Drawing.Point(6, 24);
            this.lvwTracks.Name = "lvwTracks";
            this.lvwTracks.Size = new System.Drawing.Size(287, 181);
            this.lvwTracks.TabIndex = 37;
            this.lvwTracks.UseCompatibleStateImageBehavior = false;
            this.lvwTracks.View = System.Windows.Forms.View.List;
            this.lvwTracks.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwTracks_ItemSelectionChanged);
            // 
            // grpTracks
            // 
            this.grpTracks.Controls.Add(this.txtNewTrack);
            this.grpTracks.Controls.Add(this.btnConfirm);
            this.grpTracks.Controls.Add(this.label3);
            this.grpTracks.Controls.Add(this.lvwTracks);
            this.grpTracks.Controls.Add(this.btnAdd);
            this.grpTracks.Controls.Add(this.btnDelete);
            this.grpTracks.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTracks.Location = new System.Drawing.Point(2, 2);
            this.grpTracks.Name = "grpTracks";
            this.grpTracks.Size = new System.Drawing.Size(337, 247);
            this.grpTracks.TabIndex = 41;
            this.grpTracks.TabStop = false;
            this.grpTracks.Text = "Liste des circuits";
            // 
            // txtNewTrack
            // 
            this.txtNewTrack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewTrack.Enabled = false;
            this.txtNewTrack.Location = new System.Drawing.Point(53, 209);
            this.txtNewTrack.Name = "txtNewTrack";
            this.txtNewTrack.Size = new System.Drawing.Size(240, 25);
            this.txtNewTrack.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 41;
            this.label3.Text = "Nom:";
            // 
            // picFT1600
            // 
            this.picFT1600.Image = ((System.Drawing.Image)(resources.GetObject("picFT1600.Image")));
            this.picFT1600.Location = new System.Drawing.Point(2, 249);
            this.picFT1600.Name = "picFT1600";
            this.picFT1600.Size = new System.Drawing.Size(175, 50);
            this.picFT1600.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFT1600.TabIndex = 42;
            this.picFT1600.TabStop = false;
            this.picFT1600.DoubleClick += new System.EventHandler(this.PicFT1600_DoubleClick);
            // 
            // frmRaceTracks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 303);
            this.Controls.Add(this.picFT1600);
            this.Controls.Add(this.grpTracks);
            this.Controls.Add(this.btnQuit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRaceTracks";
            this.Text = "GESTION DES CIRCUITS";
            this.Load += new System.EventHandler(this.FrmRaceTracks_Load);
            this.grpTracks.ResumeLayout(false);
            this.grpTracks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ToolTip tltActions;
        private System.Windows.Forms.ListView lvwTracks;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.GroupBox grpTracks;
        private System.Windows.Forms.TextBox txtNewTrack;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picFT1600;
    }
}