﻿namespace FormulaTour1600
{
    partial class FrmStandingsDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStandingsDetails));
            this.cboRaceEvents = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lvwResults = new System.Windows.Forms.ListView();
            this.clhPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhDriverNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhBestLap = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.cboSession = new System.Windows.Forms.ComboBox();
            this.cboSeason = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // cboRaceEvents
            // 
            this.cboRaceEvents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRaceEvents.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRaceEvents.FormattingEnabled = true;
            this.cboRaceEvents.Location = new System.Drawing.Point(115, 13);
            this.cboRaceEvents.Name = "cboRaceEvents";
            this.cboRaceEvents.Size = new System.Drawing.Size(228, 25);
            this.cboRaceEvents.TabIndex = 26;
            this.cboRaceEvents.SelectionChangeCommitted += new System.EventHandler(this.CboRaceEvents_SelectionChangeCommitted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lvwResults);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 109);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(496, 521);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // lvwResults
            // 
            this.lvwResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clhPosition,
            this.clhDriverNum,
            this.clhName,
            this.clhBestLap});
            this.lvwResults.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwResults.FullRowSelect = true;
            this.lvwResults.Location = new System.Drawing.Point(7, 24);
            this.lvwResults.MultiSelect = false;
            this.lvwResults.Name = "lvwResults";
            this.lvwResults.Size = new System.Drawing.Size(466, 483);
            this.lvwResults.TabIndex = 0;
            this.lvwResults.UseCompatibleStateImageBehavior = false;
            this.lvwResults.View = System.Windows.Forms.View.Details;
            this.lvwResults.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.LvwResults_ColumnWidthChanging);
            this.lvwResults.DoubleClick += new System.EventHandler(this.LvwResults_DoubleClick);
            // 
            // clhPosition
            // 
            this.clhPosition.Text = "Pos.";
            this.clhPosition.Width = 42;
            // 
            // clhDriverNum
            // 
            this.clhDriverNum.Text = "No.";
            this.clhDriverNum.Width = 33;
            // 
            // clhName
            // 
            this.clhName.Text = "Nom";
            this.clhName.Width = 241;
            // 
            // clhBestLap
            // 
            this.clhBestLap.Text = "Meilleur tour";
            this.clhBestLap.Width = 125;
            // 
            // picLogo
            // 
            this.picLogo.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.picLogo.Location = new System.Drawing.Point(7, 638);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(396, 62);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 32;
            this.picLogo.TabStop = false;
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.Location = new System.Drawing.Point(409, 636);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(64, 64);
            this.btnQuit.TabIndex = 31;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // cboSession
            // 
            this.cboSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSession.Enabled = false;
            this.cboSession.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSession.FormattingEnabled = true;
            this.cboSession.Location = new System.Drawing.Point(115, 85);
            this.cboSession.Name = "cboSession";
            this.cboSession.Size = new System.Drawing.Size(228, 25);
            this.cboSession.TabIndex = 27;
            this.cboSession.SelectionChangeCommitted += new System.EventHandler(this.CboSession_SelectionChangeCommitted);
            this.cboSession.EnabledChanged += new System.EventHandler(this.CboSession_EnabledChanged);
            // 
            // cboSeason
            // 
            this.cboSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeason.Enabled = false;
            this.cboSeason.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSeason.FormattingEnabled = true;
            this.cboSeason.Location = new System.Drawing.Point(115, 51);
            this.cboSeason.Name = "cboSeason";
            this.cboSeason.Size = new System.Drawing.Size(72, 25);
            this.cboSeason.TabIndex = 30;
            this.cboSeason.SelectionChangeCommitted += new System.EventHandler(this.CboSeason_SelectionChangeCommitted);
            this.cboSeason.EnabledChanged += new System.EventHandler(this.CboSeason_EnabledChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 33);
            this.label2.TabIndex = 25;
            this.label2.Text = "Session:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 33);
            this.label6.TabIndex = 29;
            this.label6.Text = "Saison:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 34);
            this.label1.TabIndex = 24;
            this.label1.Text = "Événement:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmStandingsDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 712);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.cboSeason);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboRaceEvents);
            this.Controls.Add(this.cboSession);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmStandingsDetails";
            this.Text = "FEUILLE DE COURSE";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cboRaceEvents;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView lvwResults;
        private System.Windows.Forms.ColumnHeader clhPosition;
        private System.Windows.Forms.ColumnHeader clhDriverNum;
        private System.Windows.Forms.ColumnHeader clhName;
        private System.Windows.Forms.ColumnHeader clhBestLap;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ComboBox cboSession;
        private System.Windows.Forms.ComboBox cboSeason;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
    }
}