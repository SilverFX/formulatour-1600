﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class frmRaceTracks : Form
    {
        private RaceTrack _selectedTrack = new RaceTrack();
        private RaceTracksController _raceTracksController= new RaceTracksController();
        private List<RaceTrack> _trackList = new List<RaceTrack>();

        /// <summary>
        /// Constructor
        /// </summary>
        public frmRaceTracks()
        {
            InitializeComponent();

            try
            {
                _trackList = _raceTracksController.GetRaceTracks();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Load Form Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmRaceTracks_Load(object sender, EventArgs e)
        {
            UpdateListView();
        }

        /// <summary>
        /// Add a track
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            using (var frm = new frmPrompt("Ajouter un circuit", "Quel est le nom du circuit à ajouter?"))
            {
                var frmResult = frm.ShowDialog();

                if (frmResult == DialogResult.OK)
                {
                    _raceTracksController.AddRaceTrack(frm.ReturnValue);

                    UpdateListView();
                }
            }
        }

        /// <summary>
        /// Remove a track from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult =
                MessageBox.Show(
                    "Êtes vous certain de vouloir supprimer cet enregistrement?",
                    "Suppression",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );

            if (dialogResult == DialogResult.Yes)
            {
                _raceTracksController.DeleteRaceTrack(_selectedTrack);

                UpdateListView();
            }
        }

        /// <summary>
        /// Apply changes (Track)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            if (_selectedTrack.Name != txtNewTrack.Text)
            {
                _selectedTrack.Name = txtNewTrack.Text;
                _raceTracksController.UpdateRaceTrack(_selectedTrack);

                UpdateListView();
            }
        }

        /// <summary>
        /// ListView Item Selection Changed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwTracks_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListView thisListView = (ListView)sender;

            if (thisListView.SelectedItems.Count > 0)
            {
                _selectedTrack = (RaceTrack)lvwTracks.SelectedItems[0].Tag;

                btnDelete.Enabled = true;
                btnConfirm.Enabled = true;

                txtNewTrack.Enabled = true;
                txtNewTrack.Text = _selectedTrack.Name;
            }
            else
            {
                _selectedTrack = null;

                btnDelete.Enabled = false;
                btnConfirm.Enabled = false;

                txtNewTrack.Enabled = false;
                txtNewTrack.Text = string.Empty;
            }
        }
        #region -------------------- CUSTOM METHODS --------------------
        /// <summary>
        /// Updates the ListView containing the tracks
        /// </summary>
        private void UpdateListView()
        {
            lvwTracks.Items.Clear();
            _trackList = _raceTracksController.GetRaceTracks();

            foreach (RaceTrack track in _trackList)
            {
                ListViewItem lviRow = new ListViewItem(track.Name)
                {
                    Tag = track,
                    Name = track.Name
                };

                lvwTracks.Items.Add(lviRow);
            }
        }

        /// <summary>
        /// Opens the website
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicFT1600_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("http://www.siteformulatour1600.com/");
        }
        #endregion
    }
}
