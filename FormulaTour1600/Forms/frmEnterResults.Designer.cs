﻿namespace FormulaTour1600
{
    partial class FrmEnterResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEnterResults));
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            this.tltActions = new System.Windows.Forms.ToolTip(this.components);
            this.btnViewProfile = new System.Windows.Forms.Button();
            this.btnSearchDriver = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLookup = new System.Windows.Forms.Button();
            this.btnAddQuick = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.chkPole = new System.Windows.Forms.CheckBox();
            this.cboSeason = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.grpInformations = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboRaceEvents = new System.Windows.Forms.ComboBox();
            this.cboSession = new System.Windows.Forms.ComboBox();
            this.cboDrivers = new System.Windows.Forms.ComboBox();
            this.cboPosition = new System.Windows.Forms.ComboBox();
            this.txtDriver = new System.Windows.Forms.TextBox();
            this.tslblState = new System.Windows.Forms.ToolStripStatusLabel();
            this.staInformations = new System.Windows.Forms.StatusStrip();
            this.clhName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhDriverNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvwPreview = new System.Windows.Forms.ListView();
            this.clhClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPolePosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhPoints = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.ctxEditResult = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiResult = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiDriver = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.grpInformations.SuspendLayout();
            this.staInformations.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.ctxEditResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrStatus
            // 
            this.tmrStatus.Interval = 5000;
            this.tmrStatus.Tick += new System.EventHandler(this.TmrStatus_Tick);
            // 
            // tltActions
            // 
            this.tltActions.AutoPopDelay = 5000;
            this.tltActions.InitialDelay = 500;
            this.tltActions.IsBalloon = true;
            this.tltActions.ReshowDelay = 100;
            // 
            // btnViewProfile
            // 
            this.btnViewProfile.Enabled = false;
            this.btnViewProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewProfile.Image = ((System.Drawing.Image)(resources.GetObject("btnViewProfile.Image")));
            this.btnViewProfile.Location = new System.Drawing.Point(387, 130);
            this.btnViewProfile.Name = "btnViewProfile";
            this.btnViewProfile.Size = new System.Drawing.Size(32, 32);
            this.btnViewProfile.TabIndex = 25;
            this.tltActions.SetToolTip(this.btnViewProfile, "Consulter le profil du pilote");
            this.btnViewProfile.UseVisualStyleBackColor = true;
            this.btnViewProfile.Click += new System.EventHandler(this.BtnViewProfile_Click);
            // 
            // btnSearchDriver
            // 
            this.btnSearchDriver.Enabled = false;
            this.btnSearchDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchDriver.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchDriver.Image")));
            this.btnSearchDriver.Location = new System.Drawing.Point(349, 130);
            this.btnSearchDriver.Name = "btnSearchDriver";
            this.btnSearchDriver.Size = new System.Drawing.Size(32, 32);
            this.btnSearchDriver.TabIndex = 24;
            this.tltActions.SetToolTip(this.btnSearchDriver, "Rechercher un pilote");
            this.btnSearchDriver.UseVisualStyleBackColor = true;
            this.btnSearchDriver.Click += new System.EventHandler(this.BtnSearchDriver_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(349, 171);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(32, 30);
            this.btnSave.TabIndex = 15;
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltActions.SetToolTip(this.btnSave, "Ajouter");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnLookup
            // 
            this.btnLookup.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLookup.Image = ((System.Drawing.Image)(resources.GetObject("btnLookup.Image")));
            this.btnLookup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLookup.Location = new System.Drawing.Point(387, 538);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(107, 40);
            this.btnLookup.TabIndex = 34;
            this.btnLookup.Text = "&Consulter";
            this.btnLookup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltActions.SetToolTip(this.btnLookup, "Consulter la feuille de course");
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Visible = false;
            this.btnLookup.Click += new System.EventHandler(this.BtnLookup_Click);
            // 
            // btnAddQuick
            // 
            this.btnAddQuick.Enabled = false;
            this.btnAddQuick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddQuick.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddQuick.Image = ((System.Drawing.Image)(resources.GetObject("btnAddQuick.Image")));
            this.btnAddQuick.Location = new System.Drawing.Point(425, 130);
            this.btnAddQuick.Name = "btnAddQuick";
            this.btnAddQuick.Size = new System.Drawing.Size(32, 32);
            this.btnAddQuick.TabIndex = 27;
            this.btnAddQuick.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltActions.SetToolTip(this.btnAddQuick, "Ajout rapide de pilote");
            this.btnAddQuick.UseVisualStyleBackColor = true;
            this.btnAddQuick.Click += new System.EventHandler(this.BtnAddQuick_Click);
            // 
            // picLogo
            // 
            this.picLogo.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.picLogo.Location = new System.Drawing.Point(0, 533);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(381, 86);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 29;
            this.picLogo.TabStop = false;
            this.picLogo.DoubleClick += new System.EventHandler(this.PicLogo_DoubleClick);
            // 
            // chkPole
            // 
            this.chkPole.Enabled = false;
            this.chkPole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkPole.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPole.Location = new System.Drawing.Point(193, 171);
            this.chkPole.Name = "chkPole";
            this.chkPole.Size = new System.Drawing.Size(110, 25);
            this.chkPole.TabIndex = 26;
            this.chkPole.Text = "Pôle Position";
            this.chkPole.UseVisualStyleBackColor = true;
            this.chkPole.Visible = false;
            // 
            // cboSeason
            // 
            this.cboSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeason.Enabled = false;
            this.cboSeason.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSeason.FormattingEnabled = true;
            this.cboSeason.Location = new System.Drawing.Point(115, 63);
            this.cboSeason.Name = "cboSeason";
            this.cboSeason.Size = new System.Drawing.Size(72, 25);
            this.cboSeason.TabIndex = 23;
            this.cboSeason.SelectedIndexChanged += new System.EventHandler(this.CboSeason_SelectedIndexChanged);
            this.cboSeason.EnabledChanged += new System.EventHandler(this.CboSeason_EnabledChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 58);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 33);
            this.label6.TabIndex = 22;
            this.label6.Text = "Saison:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpInformations
            // 
            this.grpInformations.Controls.Add(this.btnAddQuick);
            this.grpInformations.Controls.Add(this.chkPole);
            this.grpInformations.Controls.Add(this.btnViewProfile);
            this.grpInformations.Controls.Add(this.btnSearchDriver);
            this.grpInformations.Controls.Add(this.cboSeason);
            this.grpInformations.Controls.Add(this.label6);
            this.grpInformations.Controls.Add(this.label1);
            this.grpInformations.Controls.Add(this.label2);
            this.grpInformations.Controls.Add(this.label3);
            this.grpInformations.Controls.Add(this.label4);
            this.grpInformations.Controls.Add(this.cboRaceEvents);
            this.grpInformations.Controls.Add(this.btnSave);
            this.grpInformations.Controls.Add(this.cboSession);
            this.grpInformations.Controls.Add(this.cboDrivers);
            this.grpInformations.Controls.Add(this.cboPosition);
            this.grpInformations.Controls.Add(this.txtDriver);
            this.grpInformations.Font = new System.Drawing.Font("Microsoft NeoGothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpInformations.Location = new System.Drawing.Point(0, 1);
            this.grpInformations.Name = "grpInformations";
            this.grpInformations.Size = new System.Drawing.Size(492, 211);
            this.grpInformations.TabIndex = 32;
            this.grpInformations.TabStop = false;
            this.grpInformations.Text = "Informations";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Événement:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 33);
            this.label2.TabIndex = 1;
            this.label2.Text = "Session:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 130);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 34);
            this.label3.TabIndex = 2;
            this.label3.Text = "Pilote:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 167);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 34);
            this.label4.TabIndex = 3;
            this.label4.Text = "Position:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboRaceEvents
            // 
            this.cboRaceEvents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRaceEvents.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRaceEvents.FormattingEnabled = true;
            this.cboRaceEvents.Location = new System.Drawing.Point(115, 29);
            this.cboRaceEvents.Name = "cboRaceEvents";
            this.cboRaceEvents.Size = new System.Drawing.Size(228, 25);
            this.cboRaceEvents.TabIndex = 5;
            this.cboRaceEvents.SelectionChangeCommitted += new System.EventHandler(this.CboRaceEvents_SelectionChangeCommitted);
            // 
            // cboSession
            // 
            this.cboSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSession.Enabled = false;
            this.cboSession.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSession.FormattingEnabled = true;
            this.cboSession.Location = new System.Drawing.Point(115, 97);
            this.cboSession.Name = "cboSession";
            this.cboSession.Size = new System.Drawing.Size(228, 25);
            this.cboSession.TabIndex = 6;
            this.cboSession.SelectionChangeCommitted += new System.EventHandler(this.CboSession_SelectionChangeCommitted);
            this.cboSession.EnabledChanged += new System.EventHandler(this.CboSession_EnabledChanged);
            // 
            // cboDrivers
            // 
            this.cboDrivers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cboDrivers.Enabled = false;
            this.cboDrivers.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDrivers.FormattingEnabled = true;
            this.cboDrivers.Location = new System.Drawing.Point(115, 134);
            this.cboDrivers.MaxLength = 3;
            this.cboDrivers.Name = "cboDrivers";
            this.cboDrivers.Size = new System.Drawing.Size(39, 25);
            this.cboDrivers.TabIndex = 7;
            this.cboDrivers.SelectedIndexChanged += new System.EventHandler(this.CboDrivers_SelectedIndexChanged);
            this.cboDrivers.SelectionChangeCommitted += new System.EventHandler(this.CboDrivers_SelectionChangeCommitted);
            this.cboDrivers.EnabledChanged += new System.EventHandler(this.CboDrivers_EnabledChanged);
            // 
            // cboPosition
            // 
            this.cboPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPosition.Enabled = false;
            this.cboPosition.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPosition.FormattingEnabled = true;
            this.cboPosition.Location = new System.Drawing.Point(115, 171);
            this.cboPosition.Name = "cboPosition";
            this.cboPosition.Size = new System.Drawing.Size(72, 25);
            this.cboPosition.TabIndex = 9;
            this.cboPosition.SelectedIndexChanged += new System.EventHandler(this.CboPosition_SelectedIndexChanged);
            this.cboPosition.EnabledChanged += new System.EventHandler(this.CboPosition_EnabledChanged);
            // 
            // txtDriver
            // 
            this.txtDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDriver.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDriver.Location = new System.Drawing.Point(160, 133);
            this.txtDriver.Name = "txtDriver";
            this.txtDriver.ReadOnly = true;
            this.txtDriver.Size = new System.Drawing.Size(183, 25);
            this.txtDriver.TabIndex = 8;
            this.txtDriver.TextChanged += new System.EventHandler(this.TxtDriver_TextChanged);
            // 
            // tslblState
            // 
            this.tslblState.Font = new System.Drawing.Font("Microsoft NeoGothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblState.Name = "tslblState";
            this.tslblState.Size = new System.Drawing.Size(483, 17);
            this.tslblState.Spring = true;
            this.tslblState.Text = "< Message >";
            // 
            // staInformations
            // 
            this.staInformations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblState});
            this.staInformations.Location = new System.Drawing.Point(0, 620);
            this.staInformations.Name = "staInformations";
            this.staInformations.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.staInformations.Size = new System.Drawing.Size(498, 22);
            this.staInformations.TabIndex = 31;
            this.staInformations.Text = "statusStrip1";
            // 
            // clhName
            // 
            this.clhName.Text = "Nom";
            this.clhName.Width = 185;
            // 
            // clhDriverNum
            // 
            this.clhDriverNum.Text = "No.";
            this.clhDriverNum.Width = 40;
            // 
            // clhPosition
            // 
            this.clhPosition.Text = "Pos.";
            this.clhPosition.Width = 40;
            // 
            // lvwPreview
            // 
            this.lvwPreview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clhPosition,
            this.clhDriverNum,
            this.clhName,
            this.clhClass,
            this.clhPolePosition,
            this.clhPoints});
            this.lvwPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwPreview.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwPreview.FullRowSelect = true;
            this.lvwPreview.Location = new System.Drawing.Point(3, 25);
            this.lvwPreview.MultiSelect = false;
            this.lvwPreview.Name = "lvwPreview";
            this.lvwPreview.Size = new System.Drawing.Size(486, 301);
            this.lvwPreview.TabIndex = 0;
            this.lvwPreview.UseCompatibleStateImageBehavior = false;
            this.lvwPreview.View = System.Windows.Forms.View.Details;
            this.lvwPreview.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.LvwPreview_ColumnWidthChanging);
            this.lvwPreview.DoubleClick += new System.EventHandler(this.LvwPreview_DoubleClick);
            this.lvwPreview.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LvwPreview_MouseClick);
            // 
            // clhClass
            // 
            this.clhClass.Text = "Classe";
            this.clhClass.Width = 100;
            // 
            // clhPolePosition
            // 
            this.clhPolePosition.Text = "Pole?";
            this.clhPolePosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.clhPolePosition.Width = 45;
            // 
            // clhPoints
            // 
            this.clhPoints.Text = "Points";
            this.clhPoints.Width = 50;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lvwPreview);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 203);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 329);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Aperçu";
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(387, 579);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(107, 40);
            this.btnQuit.TabIndex = 28;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // ctxEditResult
            // 
            this.ctxEditResult.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxEditResult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiResult,
            this.tsmiDelete,
            this.toolStripSeparator1,
            this.tsmiDriver});
            this.ctxEditResult.Name = "ctxEditResult";
            this.ctxEditResult.Size = new System.Drawing.Size(199, 76);
            // 
            // tsmiResult
            // 
            this.tsmiResult.Image = ((System.Drawing.Image)(resources.GetObject("tsmiResult.Image")));
            this.tsmiResult.Name = "tsmiResult";
            this.tsmiResult.Size = new System.Drawing.Size(198, 22);
            this.tsmiResult.Text = "Modifier le résultat";
            this.tsmiResult.Click += new System.EventHandler(this.TsmiResult_Click);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsmiDelete.Image")));
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.Size = new System.Drawing.Size(198, 22);
            this.tsmiDelete.Text = "Supprimer le résultat";
            this.tsmiDelete.Click += new System.EventHandler(this.TsmiDelete_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // tsmiDriver
            // 
            this.tsmiDriver.Image = ((System.Drawing.Image)(resources.GetObject("tsmiDriver.Image")));
            this.tsmiDriver.Name = "tsmiDriver";
            this.tsmiDriver.Size = new System.Drawing.Size(198, 22);
            this.tsmiDriver.Text = "Modifier le pilote";
            this.tsmiDriver.Click += new System.EventHandler(this.TsmiDriver_Click);
            // 
            // FrmEnterResults
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 642);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.staInformations);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.grpInformations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmEnterResults";
            this.Text = "ENTRÉE DES RÉSULTATS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmResults_FormClosing);
            this.Load += new System.EventHandler(this.FrmEnterResults_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.grpInformations.ResumeLayout(false);
            this.grpInformations.PerformLayout();
            this.staInformations.ResumeLayout(false);
            this.staInformations.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ctxEditResult.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrStatus;
        private System.Windows.Forms.ToolTip tltActions;
        private System.Windows.Forms.Button btnViewProfile;
        private System.Windows.Forms.Button btnSearchDriver;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLookup;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.CheckBox chkPole;
        private System.Windows.Forms.ComboBox cboSeason;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox grpInformations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboRaceEvents;
        private System.Windows.Forms.ComboBox cboSession;
        private System.Windows.Forms.ComboBox cboDrivers;
        private System.Windows.Forms.ComboBox cboPosition;
        private System.Windows.Forms.TextBox txtDriver;
        private System.Windows.Forms.ToolStripStatusLabel tslblState;
        private System.Windows.Forms.StatusStrip staInformations;
        private System.Windows.Forms.ColumnHeader clhName;
        private System.Windows.Forms.ColumnHeader clhDriverNum;
        private System.Windows.Forms.ColumnHeader clhPosition;
        private System.Windows.Forms.ListView lvwPreview;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ColumnHeader clhPolePosition;
        private System.Windows.Forms.ColumnHeader clhClass;
        private System.Windows.Forms.ContextMenuStrip ctxEditResult;
        private System.Windows.Forms.ToolStripMenuItem tsmiResult;
        private System.Windows.Forms.ToolStripMenuItem tsmiDriver;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDelete;
        private System.Windows.Forms.Button btnAddQuick;
        private System.Windows.Forms.ColumnHeader clhPoints;
    }
}