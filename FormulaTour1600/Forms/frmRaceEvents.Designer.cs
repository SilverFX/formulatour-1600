﻿namespace FormulaTour1600
{
    partial class FrmRaceEvents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRaceEvents));
            this.tltActions = new System.Windows.Forms.ToolTip(this.components);
            this.grpTracks = new System.Windows.Forms.GroupBox();
            this.dgvRaceEvents = new System.Windows.Forms.DataGridView();
            this.clmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRunsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnQuit = new System.Windows.Forms.Button();
            this.picFT1600 = new System.Windows.Forms.PictureBox();
            this.grpTracks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRaceEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).BeginInit();
            this.SuspendLayout();
            // 
            // tltActions
            // 
            this.tltActions.IsBalloon = true;
            // 
            // grpTracks
            // 
            this.grpTracks.Controls.Add(this.dgvRaceEvents);
            this.grpTracks.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTracks.Location = new System.Drawing.Point(3, 3);
            this.grpTracks.Name = "grpTracks";
            this.grpTracks.Size = new System.Drawing.Size(429, 272);
            this.grpTracks.TabIndex = 44;
            this.grpTracks.TabStop = false;
            this.grpTracks.Text = "Liste des événements";
            // 
            // dgvRaceEvents
            // 
            this.dgvRaceEvents.AllowUserToAddRows = false;
            this.dgvRaceEvents.AllowUserToDeleteRows = false;
            this.dgvRaceEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRaceEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmName,
            this.clmRunsCount});
            this.dgvRaceEvents.Location = new System.Drawing.Point(10, 24);
            this.dgvRaceEvents.Name = "dgvRaceEvents";
            this.dgvRaceEvents.RowHeadersVisible = false;
            this.dgvRaceEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvRaceEvents.Size = new System.Drawing.Size(409, 240);
            this.dgvRaceEvents.TabIndex = 43;
            // 
            // clmName
            // 
            this.clmName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmName.HeaderText = "Nom";
            this.clmName.Name = "clmName";
            // 
            // clmRunsCount
            // 
            this.clmRunsCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.clmRunsCount.DefaultCellStyle = dataGridViewCellStyle2;
            this.clmRunsCount.HeaderText = "Nb. courses";
            this.clmRunsCount.Name = "clmRunsCount";
            this.clmRunsCount.Width = 103;
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuit.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuit.Location = new System.Drawing.Point(344, 281);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(88, 50);
            this.btnQuit.TabIndex = 43;
            this.btnQuit.Text = "&Quitter";
            this.btnQuit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuit.UseVisualStyleBackColor = true;
            // 
            // picFT1600
            // 
            this.picFT1600.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.picFT1600.Location = new System.Drawing.Point(3, 281);
            this.picFT1600.Name = "picFT1600";
            this.picFT1600.Size = new System.Drawing.Size(335, 50);
            this.picFT1600.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFT1600.TabIndex = 45;
            this.picFT1600.TabStop = false;
            // 
            // FrmRaceEvents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 337);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.grpTracks);
            this.Controls.Add(this.picFT1600);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRaceEvents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GESTION DES ÉVÉNEMENTS";
            this.Load += new System.EventHandler(this.FrmRaceEvents_Load);
            this.grpTracks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRaceEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ToolTip tltActions;
        private System.Windows.Forms.GroupBox grpTracks;
        private System.Windows.Forms.PictureBox picFT1600;
        private System.Windows.Forms.DataGridView dgvRaceEvents;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRunsCount;
    }
}