﻿using System;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmPromptNewDriver : Form
    {
        private string _driverNumber;

        public Driver ReturnDriver { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="drvNumber">Driver Number to display</param>
        public FrmPromptNewDriver(string drvName = null, string drvNumber = null)
        {
            ReturnDriver = new Driver();

            _driverNumber = drvNumber;

            InitializeComponent();

            if (!string.IsNullOrEmpty(drvNumber))
            {
                txtNumber.Visible = false;

                lblNumber.Visible = true;
                lblNumber.Text = drvNumber.ToString();
            }
            else
            {
                txtNumber.Visible = true;

                lblNumber.Visible = false;
                lblNumber.Text = string.Empty;
            }

            cboClass.DataSource = new BindingSource(new RaceClassesController().GetRaceClasses(), null);
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Name";
            cboClass.SelectedIndex = 0;

            cboEngines.DataSource = new BindingSource(new EnginesController().GetEngines(), null);
            cboEngines.DisplayMember = "Name";
            cboEngines.ValueMember = "Name";
            cboEngines.SelectedIndex = 0;
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmPromptNewDriver_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_driverNumber))
                txtDriverName.Select();
            else
                txtNumber.Select();
        }

        /// <summary>
        /// Change the Button Save Enabled state according to text in the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtNewDriver_TextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = (!string.IsNullOrEmpty(txtDriverName.Text) && (!string.IsNullOrEmpty(txtNumber.Text) || !string.IsNullOrEmpty(lblNumber.Text)));
        }

        /// <summary>
        /// Saves the changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            ReturnDriver.Number = (txtNumber.Visible) ? Convert.ToInt32(txtNumber.Text) : Convert.ToInt32(lblNumber.Text);
            ReturnDriver.Name = txtDriverName.Text;
            ReturnDriver.RaceClass = (RaceClass)cboClass.SelectedItem;
            ReturnDriver.Engine = (Engine)cboEngines.SelectedItem;

            DriversController driversController = new DriversController();
            ReturnDriver.Id = driversController.InsertDriver(ReturnDriver);
        }
    }
}
