﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class frmSearch : Form
    {
        private const string DEFAULT_CAPTION = "Rechercher un pilote";

        [DllImport("user32")]
        private static extern long ShowScrollBar(IntPtr hWnd, int wBar, [MarshalAs(UnmanagedType.Bool)] bool bShow);
        int SB_HORZ = 0;

        private List<Driver> _driverList;
        private string _statusMessage = string.Empty;

        public Driver ReturnDriver { get; set; }
        public List<Driver> UpdatedDriverList { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="driverList">List of Driver</param>
        public frmSearch(List<Driver> driverList, string caption = null, string driverNumber = null)
        {
            InitializeComponent();

            _driverList = driverList;
            int driverCount = driverList.Count;
            lblCaption.Text = (!string.IsNullOrEmpty(caption))
                ? caption
                : DEFAULT_CAPTION;

            if (!string.IsNullOrEmpty(driverNumber))
            {
                txtSearchNum.Text = driverNumber;
                txtSearchNum.Select();
                SendKeys.Send(driverNumber);
            }

            foreach (Driver driver in driverList)
            {
                string[] row = { driver.Number.ToString(), driver.Name };
                ListViewItem lvwItem = new ListViewItem(row)
                {
                    Tag = driver,
                    Name = driver.Id.ToString()
                };
                lvwSearch.Items.Add(lvwItem);
            }

            tslblFound.Text = GetStatus(driverCount);
        }

        /// <summary>
        /// Form Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmSearch_Load(object sender, EventArgs e)
        {
            HideHorizontalScrollBar();
            txtSearchNum.Select();
        }

        #region -------------------------------------------------- BUTTON CLICK METHODS --------------------------------------------------
        /// <summary>
        /// Clears the textbox's text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClearSearchBox_Click(object sender, EventArgs e)
        {
            txtSearchNum.Clear();
            txtSearch.Clear();
        }

        /// <summary>
        /// Select the highlighted driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelect_Click(object sender, EventArgs e)
        {
            ReturnDriver = (Driver)lvwSearch.SelectedItems[0].Tag;
        }
        #endregion

        #region -------------------------------------------------- LISTVIEW EVENT METHODS --------------------------------------------------
        /// <summary>
        /// Double click event when choosing a driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwSearch_DoubleClick(object sender, EventArgs e)
        {
            if (lvwSearch.SelectedItems.Count > 0)
            {
                ReturnDriver = (Driver)lvwSearch.SelectedItems[0].Tag;
                DialogResult = DialogResult.OK;
            }
        }

        /// <summary>
        /// When changing the selection in the listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwSearch_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            btnSelect.Enabled = (lvwSearch.SelectedItems.Count > 0);
        }

        /// <summary>
        /// Enable the escape key to close the form when the focus is in listview control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        /// <summary>
        /// Prevent listview from resizing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvwSearch_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvwSearch.Columns[e.ColumnIndex].Width;
        }
        #endregion

        #region -------------------------------------------------- TEXTBOX EVENTS METHODS --------------------------------------------------
        /// <summary>
        /// Disables the Select button when going into the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_Enter(object sender, EventArgs e)
        {
            btnSelect.Enabled = false;
        }

        /// <summary>
        /// Enable the escape key to close the form when the focus is in textbox control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (btnClearSearchBox.Visible)
                {
                    btnClearSearchBox.PerformClick();
                }
                else
                {
                    btnCancel.PerformClick();
                }
            }
        }

        /// <summary>
        /// When the textbox changes value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox thisTextBox = (TextBox)sender;

            btnClearSearchBox.Visible = (thisTextBox.Text != string.Empty);
            lvwSearch.Items.Clear();

            bool searchByString = (thisTextBox.Name == txtSearch.Name);

            LoadListView(_driverList, searchByString, thisTextBox.Text);
        }

        /// <summary>
        /// Only use number in the search box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtSearchNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        #endregion

        #region -------------------------------------------------- CUSTOM METHODS --------------------------------------------------
        /// <summary>
        /// Filters the ListView with the entered text
        /// </summary>
        /// <param name="drivers">List of Drivers as Enumerable</param>
        /// <param name="searchString">Search string</param>
        public void LoadListView(List<Driver> drivers, bool searchByString, string searchString = "")
        {
            int driverCount = 0;
            //IEnumerable<Driver> query;
            List<Driver> query;

            // Search by string
            if (searchByString)
            {
                query = !(searchString.Length > 1)
                    ? drivers
                    : (from driver in drivers
                       where driver.Name.ToLowerInvariant().Contains(searchString.ToLowerInvariant())
                       orderby driver.Name
                       select driver).ToList();
            }

            // Search by numbers
            else
            {
                query = !(searchString.Length >= 1)
                    ? drivers
                    : (from driver in drivers
                       where driver.Number.ToString().StartsWith(searchString)
                       orderby driver.Number.ToString()
                       select driver).ToList();
            }

            foreach (Driver filteredDriver in query)
            {
                string[] row = { filteredDriver.Number.ToString(), filteredDriver.Name };
                ListViewItem lvwItem = new ListViewItem(row)
                {
                    Tag = filteredDriver,
                    Name = filteredDriver.Id.ToString()
                };
                lvwSearch.Items.Add(lvwItem);
                driverCount++;
            }

            tslblFound.Text = GetStatus(driverCount);
        }

        /// <summary>
        /// Hide the horizontal scrollbar
        /// </summary>
        private void HideHorizontalScrollBar()
        {
            ShowScrollBar(lvwSearch.Handle, SB_HORZ, false);
        }
        /// <summary>
        /// Returns the correct status corresponding to the driverCount supplied
        /// </summary>
        /// <param name="driverCount">The DriverCount</param>
        /// <returns>Status Message to Display</returns>
        private string GetStatus(int driverCount)
        {
            switch (driverCount)
            {
                case 0:
                    return "Aucun pilote trouvé";
                case 1:
                    return string.Format("{0} pilote trouvé", driverCount);
                default:
                    return string.Format("{0} pilotes trouvés", driverCount);
            }
        }
        #endregion

        /// <summary>
        /// Add a temporary driver to the database (prompt)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            using (var frm = new FrmPromptNewDriver(txtSearchNum.Text))
            {
                var frmResult = frm.ShowDialog();

                if (frmResult == DialogResult.OK)
                {
                    Driver newDriver = frm.ReturnDriver;

                    txtSearch.Text = newDriver.Name;

                    _driverList.Add(newDriver);

                    UpdatedDriverList = _driverList;

                    LoadListView(_driverList, true, newDriver.Name);
                }
            }
        }
    }
}
