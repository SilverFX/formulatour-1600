﻿namespace FormulaTour1600
{
    partial class frmSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearch));
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lvwSearch = new System.Windows.Forms.ListView();
            this.chNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.staSearch = new System.Windows.Forms.StatusStrip();
            this.tslblFound = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnClearSearchBox = new System.Windows.Forms.Button();
            this.txtSearchNum = new System.Windows.Forms.TextBox();
            this.grpSearch = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblCaption = new System.Windows.Forms.Label();
            this.staSearch.SuspendLayout();
            this.grpSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSearch
            // 
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(53, 24);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(191, 25);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            this.txtSearch.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // lvwSearch
            // 
            this.lvwSearch.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chNum,
            this.chName});
            this.lvwSearch.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwSearch.FullRowSelect = true;
            this.lvwSearch.Location = new System.Drawing.Point(7, 60);
            this.lvwSearch.Name = "lvwSearch";
            this.lvwSearch.Size = new System.Drawing.Size(268, 385);
            this.lvwSearch.TabIndex = 3;
            this.lvwSearch.UseCompatibleStateImageBehavior = false;
            this.lvwSearch.View = System.Windows.Forms.View.Details;
            this.lvwSearch.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.LvwSearch_ColumnWidthChanging);
            this.lvwSearch.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwSearch_ItemSelectionChanged);
            this.lvwSearch.DoubleClick += new System.EventHandler(this.LvwSearch_DoubleClick);
            this.lvwSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvwSearch_KeyDown);
            // 
            // chNum
            // 
            this.chNum.Text = "Numéro";
            // 
            // chName
            // 
            this.chName.Text = "Pilote";
            this.chName.Width = 186;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(191, 490);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 29);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Annuler";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSelect
            // 
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(6, 490);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(87, 29);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "&Choisir";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // staSearch
            // 
            this.staSearch.AutoSize = false;
            this.staSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblFound});
            this.staSearch.Location = new System.Drawing.Point(0, 521);
            this.staSearch.Name = "staSearch";
            this.staSearch.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.staSearch.Size = new System.Drawing.Size(290, 22);
            this.staSearch.TabIndex = 3;
            this.staSearch.Text = "statusStrip1";
            // 
            // tslblFound
            // 
            this.tslblFound.Font = new System.Drawing.Font("Microsoft NeoGothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblFound.Name = "tslblFound";
            this.tslblFound.Size = new System.Drawing.Size(275, 17);
            this.tslblFound.Spring = true;
            this.tslblFound.Text = "< Nombre de pilotes trouvés >";
            // 
            // btnClearSearchBox
            // 
            this.btnClearSearchBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearSearchBox.Image = ((System.Drawing.Image)(resources.GetObject("btnClearSearchBox.Image")));
            this.btnClearSearchBox.Location = new System.Drawing.Point(250, 24);
            this.btnClearSearchBox.Name = "btnClearSearchBox";
            this.btnClearSearchBox.Size = new System.Drawing.Size(25, 25);
            this.btnClearSearchBox.TabIndex = 3;
            this.btnClearSearchBox.UseVisualStyleBackColor = true;
            this.btnClearSearchBox.Visible = false;
            this.btnClearSearchBox.Click += new System.EventHandler(this.BtnClearSearchBox_Click);
            // 
            // txtSearchNum
            // 
            this.txtSearchNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchNum.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchNum.Location = new System.Drawing.Point(7, 24);
            this.txtSearchNum.MaxLength = 3;
            this.txtSearchNum.Name = "txtSearchNum";
            this.txtSearchNum.Size = new System.Drawing.Size(40, 25);
            this.txtSearchNum.TabIndex = 1;
            this.txtSearchNum.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            this.txtSearchNum.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtSearchNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtSearchNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSearchNum_KeyPress);
            // 
            // grpSearch
            // 
            this.grpSearch.Controls.Add(this.txtSearchNum);
            this.grpSearch.Controls.Add(this.txtSearch);
            this.grpSearch.Controls.Add(this.btnClearSearchBox);
            this.grpSearch.Controls.Add(this.lvwSearch);
            this.grpSearch.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.Location = new System.Drawing.Point(0, 32);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(283, 452);
            this.grpSearch.TabIndex = 0;
            this.grpSearch.TabStop = false;
            this.grpSearch.Text = "Pilotes";
            // 
            // btnAdd
            // 
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(99, 490);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(86, 29);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Ajouter";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // lblCaption
            // 
            this.lblCaption.Font = new System.Drawing.Font("Microsoft NeoGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Location = new System.Drawing.Point(3, 8);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(280, 23);
            this.lblCaption.TabIndex = 5;
            this.lblCaption.Text = "< Quel pilote a effectué la pôle ? >";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmSearch
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(290, 543);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpSearch);
            this.Controls.Add(this.staSearch);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recherche de pilote";
            this.Load += new System.EventHandler(this.FrmSearch_Load);
            this.staSearch.ResumeLayout(false);
            this.staSearch.PerformLayout();
            this.grpSearch.ResumeLayout(false);
            this.grpSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ListView lvwSearch;
        private System.Windows.Forms.ColumnHeader chNum;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.StatusStrip staSearch;
        private System.Windows.Forms.ToolStripStatusLabel tslblFound;
        private System.Windows.Forms.Button btnClearSearchBox;
        private System.Windows.Forms.TextBox txtSearchNum;
        private System.Windows.Forms.GroupBox grpSearch;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblCaption;
    }
}