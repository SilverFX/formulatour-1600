﻿namespace FormulaTour1600
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tltHints = new System.Windows.Forms.ToolTip(this.components);
            this.btnEvents = new System.Windows.Forms.Button();
            this.btnExcelExport = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnResults = new System.Windows.Forms.Button();
            this.btnDrivers = new System.Windows.Forms.Button();
            this.picFT1600 = new System.Windows.Forms.PictureBox();
            this.sfdExcel = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).BeginInit();
            this.SuspendLayout();
            // 
            // tltHints
            // 
            this.tltHints.IsBalloon = true;
            this.tltHints.StripAmpersands = true;
            // 
            // btnEvents
            // 
            this.btnEvents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEvents.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEvents.Image = ((System.Drawing.Image)(resources.GetObject("btnEvents.Image")));
            this.btnEvents.Location = new System.Drawing.Point(156, 185);
            this.btnEvents.Name = "btnEvents";
            this.btnEvents.Size = new System.Drawing.Size(72, 72);
            this.btnEvents.TabIndex = 11;
            this.btnEvents.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnEvents, "Gestion des événements");
            this.btnEvents.UseVisualStyleBackColor = true;
            this.btnEvents.Click += new System.EventHandler(this.BtnEvents_Click);
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcelExport.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcelExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExcelExport.Image")));
            this.btnExcelExport.Location = new System.Drawing.Point(404, 185);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(72, 72);
            this.btnExcelExport.TabIndex = 8;
            this.btnExcelExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnExcelExport, "Exportation des résultats sous Excel");
            this.btnExcelExport.UseVisualStyleBackColor = true;
            this.btnExcelExport.Click += new System.EventHandler(this.BtnExcelExport_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.Location = new System.Drawing.Point(482, 185);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(72, 72);
            this.btnSettings.TabIndex = 6;
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnSettings, "Paramètres");
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.BtnSettings_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(560, 185);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(72, 72);
            this.btnExit.TabIndex = 5;
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnExit, "Quitter");
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnResults
            // 
            this.btnResults.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResults.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResults.Image = ((System.Drawing.Image)(resources.GetObject("btnResults.Image")));
            this.btnResults.Location = new System.Drawing.Point(78, 185);
            this.btnResults.Name = "btnResults";
            this.btnResults.Size = new System.Drawing.Size(72, 72);
            this.btnResults.TabIndex = 1;
            this.btnResults.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnResults, "Entrée des Résultats");
            this.btnResults.UseVisualStyleBackColor = true;
            this.btnResults.Click += new System.EventHandler(this.BtnResults_Click);
            // 
            // btnDrivers
            // 
            this.btnDrivers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDrivers.Font = new System.Drawing.Font("Microsoft NeoGothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDrivers.Image = ((System.Drawing.Image)(resources.GetObject("btnDrivers.Image")));
            this.btnDrivers.Location = new System.Drawing.Point(0, 185);
            this.btnDrivers.Name = "btnDrivers";
            this.btnDrivers.Size = new System.Drawing.Size(72, 72);
            this.btnDrivers.TabIndex = 0;
            this.btnDrivers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tltHints.SetToolTip(this.btnDrivers, "Gestion des Pilotes");
            this.btnDrivers.UseVisualStyleBackColor = true;
            this.btnDrivers.Click += new System.EventHandler(this.BtnDrivers_Click);
            // 
            // picFT1600
            // 
            this.picFT1600.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFT1600.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.picFT1600.Location = new System.Drawing.Point(0, 0);
            this.picFT1600.Name = "picFT1600";
            this.picFT1600.Size = new System.Drawing.Size(632, 179);
            this.picFT1600.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFT1600.TabIndex = 4;
            this.picFT1600.TabStop = false;
            this.picFT1600.DoubleClick += new System.EventHandler(this.PicFT1600_DoubleClick);
            // 
            // sfdExcel
            // 
            this.sfdExcel.DefaultExt = "xls";
            this.sfdExcel.Filter = "Fichier Excel|*.xls;*.xlsx;*.xlsm";
            this.sfdExcel.RestoreDirectory = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(632, 258);
            this.Controls.Add(this.btnEvents);
            this.Controls.Add(this.btnExcelExport);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnResults);
            this.Controls.Add(this.btnDrivers);
            this.Controls.Add(this.picFT1600);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "MENU PRINCIPAL";
            ((System.ComponentModel.ISupportInitialize)(this.picFT1600)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDrivers;
        private System.Windows.Forms.Button btnResults;
        private System.Windows.Forms.ToolTip tltHints;
        private System.Windows.Forms.PictureBox picFT1600;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnExcelExport;
        private System.Windows.Forms.Button btnEvents;
        private System.Windows.Forms.SaveFileDialog sfdExcel;
    }
}