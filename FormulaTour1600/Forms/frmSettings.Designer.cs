﻿namespace FormulaTour1600
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettings));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tltActions = new System.Windows.Forms.ToolTip(this.components);
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.grpSeason = new System.Windows.Forms.GroupBox();
            this.pnlScoring = new System.Windows.Forms.Panel();
            this.grpScoring = new System.Windows.Forms.GroupBox();
            this.dgvScoring = new System.Windows.Forms.DataGridView();
            this.clmPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPoints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvwSeasons = new System.Windows.Forms.ListView();
            this.clhYear = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clhDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.treSettings = new System.Windows.Forms.TreeView();
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            this.staMessage = new System.Windows.Forms.StatusStrip();
            this.tslblMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.grpSeason.SuspendLayout();
            this.pnlScoring.SuspendLayout();
            this.grpScoring.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvScoring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.staMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(104, 94);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(32, 32);
            this.btnAdd.TabIndex = 4;
            this.tltActions.SetToolTip(this.btnAdd, "Ajouter une nouvelle saison");
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(189, 94);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.TabIndex = 6;
            this.tltActions.SetToolTip(this.btnDelete, "Supprimer");
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Enabled = false;
            this.btnConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Image")));
            this.btnConfirm.Location = new System.Drawing.Point(142, 94);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(32, 32);
            this.btnConfirm.TabIndex = 5;
            this.tltActions.SetToolTip(this.btnConfirm, "Confirmer les changements");
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.BtnConfirm_Click);
            // 
            // grpSeason
            // 
            this.grpSeason.Controls.Add(this.pnlScoring);
            this.grpSeason.Controls.Add(this.btnAdd);
            this.grpSeason.Controls.Add(this.btnDelete);
            this.grpSeason.Controls.Add(this.btnCancel);
            this.grpSeason.Controls.Add(this.btnConfirm);
            this.grpSeason.Controls.Add(this.numYear);
            this.grpSeason.Controls.Add(this.txtDescription);
            this.grpSeason.Controls.Add(this.label2);
            this.grpSeason.Controls.Add(this.label1);
            this.grpSeason.Controls.Add(this.lvwSeasons);
            this.grpSeason.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSeason.Location = new System.Drawing.Point(200, 0);
            this.grpSeason.Name = "grpSeason";
            this.grpSeason.Size = new System.Drawing.Size(270, 306);
            this.grpSeason.TabIndex = 1;
            this.grpSeason.TabStop = false;
            this.grpSeason.Text = "Gestion des saisons";
            this.grpSeason.VisibleChanged += new System.EventHandler(this.GrpSeason_VisibleChanged);
            // 
            // pnlScoring
            // 
            this.pnlScoring.Controls.Add(this.grpScoring);
            this.pnlScoring.Location = new System.Drawing.Point(0, 0);
            this.pnlScoring.Name = "pnlScoring";
            this.pnlScoring.Size = new System.Drawing.Size(270, 306);
            this.pnlScoring.TabIndex = 22;
            this.pnlScoring.Visible = false;
            // 
            // grpScoring
            // 
            this.grpScoring.Controls.Add(this.dgvScoring);
            this.grpScoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpScoring.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpScoring.Location = new System.Drawing.Point(0, 0);
            this.grpScoring.Name = "grpScoring";
            this.grpScoring.Size = new System.Drawing.Size(270, 306);
            this.grpScoring.TabIndex = 2;
            this.grpScoring.TabStop = false;
            this.grpScoring.Text = "Gestion du système de pointage";
            // 
            // dgvScoring
            // 
            this.dgvScoring.AllowUserToAddRows = false;
            this.dgvScoring.AllowUserToDeleteRows = false;
            this.dgvScoring.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvScoring.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmPosition,
            this.clmPoints});
            this.dgvScoring.Location = new System.Drawing.Point(3, 21);
            this.dgvScoring.Name = "dgvScoring";
            this.dgvScoring.Size = new System.Drawing.Size(256, 272);
            this.dgvScoring.TabIndex = 0;
            this.dgvScoring.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvScoring_CellBeginEdit);
            // 
            // clmPosition
            // 
            this.clmPosition.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.clmPosition.DefaultCellStyle = dataGridViewCellStyle1;
            this.clmPosition.Frozen = true;
            this.clmPosition.HeaderText = "Position";
            this.clmPosition.Name = "clmPosition";
            this.clmPosition.ReadOnly = true;
            this.clmPosition.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmPosition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.clmPosition.Width = 79;
            // 
            // clmPoints
            // 
            this.clmPoints.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmPoints.HeaderText = "Points";
            this.clmPoints.Name = "clmPoints";
            this.clmPoints.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmPoints.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(227, 94);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(32, 32);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // numYear
            // 
            this.numYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numYear.Enabled = false;
            this.numYear.Location = new System.Drawing.Point(104, 34);
            this.numYear.Name = "numYear";
            this.numYear.ReadOnly = true;
            this.numYear.Size = new System.Drawing.Size(62, 25);
            this.numYear.TabIndex = 1;
            this.numYear.ValueChanged += new System.EventHandler(this.NumYear_ValueChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(104, 63);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(155, 25);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.TxtDescription_TextChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Description : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Année : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lvwSeasons
            // 
            this.lvwSeasons.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clhYear,
            this.clhDesc});
            this.lvwSeasons.FullRowSelect = true;
            this.lvwSeasons.Location = new System.Drawing.Point(6, 132);
            this.lvwSeasons.Name = "lvwSeasons";
            this.lvwSeasons.Size = new System.Drawing.Size(253, 160);
            this.lvwSeasons.TabIndex = 8;
            this.lvwSeasons.UseCompatibleStateImageBehavior = false;
            this.lvwSeasons.View = System.Windows.Forms.View.Details;
            this.lvwSeasons.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwSeasons_ItemSelectionChanged);
            // 
            // clhYear
            // 
            this.clhYear.Text = "Année";
            this.clhYear.Width = 70;
            // 
            // clhDesc
            // 
            this.clhDesc.Text = "Description";
            this.clhDesc.Width = 152;
            // 
            // picLogo
            // 
            this.picLogo.Image = global::FormulaTour1600.Properties.Resources.F1600Canada;
            this.picLogo.Location = new System.Drawing.Point(200, 312);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(200, 62);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 20;
            this.picLogo.TabStop = false;
            // 
            // btnQuit
            // 
            this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
            this.btnQuit.Location = new System.Drawing.Point(406, 312);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(64, 64);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.BtnQuit_Click);
            // 
            // treSettings
            // 
            this.treSettings.Dock = System.Windows.Forms.DockStyle.Left;
            this.treSettings.Font = new System.Drawing.Font("Microsoft NeoGothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treSettings.FullRowSelect = true;
            this.treSettings.Location = new System.Drawing.Point(0, 0);
            this.treSettings.Name = "treSettings";
            this.treSettings.Size = new System.Drawing.Size(191, 399);
            this.treSettings.TabIndex = 0;
            this.treSettings.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreSettings_AfterSelect);
            // 
            // tmrStatus
            // 
            this.tmrStatus.Interval = 1000;
            this.tmrStatus.Tick += new System.EventHandler(this.TmrStatus_Tick);
            // 
            // staMessage
            // 
            this.staMessage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblMessage});
            this.staMessage.Location = new System.Drawing.Point(191, 377);
            this.staMessage.Name = "staMessage";
            this.staMessage.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.staMessage.Size = new System.Drawing.Size(285, 22);
            this.staMessage.TabIndex = 21;
            this.staMessage.Text = "statusStrip1";
            // 
            // tslblMessage
            // 
            this.tslblMessage.Font = new System.Drawing.Font("Microsoft NeoGothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblMessage.Name = "tslblMessage";
            this.tslblMessage.Size = new System.Drawing.Size(270, 17);
            this.tslblMessage.Spring = true;
            this.tslblMessage.Text = "< Message >";
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 399);
            this.Controls.Add(this.staMessage);
            this.Controls.Add(this.treSettings);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.grpSeason);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSettings";
            this.Text = "Paramètres";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.grpSeason.ResumeLayout(false);
            this.grpSeason.PerformLayout();
            this.pnlScoring.ResumeLayout(false);
            this.grpScoring.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvScoring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.staMessage.ResumeLayout(false);
            this.staMessage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip tltActions;
        private System.Windows.Forms.GroupBox grpSeason;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvwSeasons;
        private System.Windows.Forms.ColumnHeader clhYear;
        private System.Windows.Forms.ColumnHeader clhDesc;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.GroupBox grpScoring;
        private System.Windows.Forms.DataGridView dgvScoring;
        private System.Windows.Forms.TreeView treSettings;
        private System.Windows.Forms.Timer tmrStatus;
        private System.Windows.Forms.StatusStrip staMessage;
        private System.Windows.Forms.ToolStripStatusLabel tslblMessage;
        private System.Windows.Forms.Panel pnlScoring;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPoints;
    }
}