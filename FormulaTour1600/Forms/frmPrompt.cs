﻿using System.Windows.Forms;

namespace FormulaTour1600
{
    public partial class FrmPrompt : Form
    {
        public string ReturnValue { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">The text to display</param>
        /// <param name="caption">The title to display</param>
        public FrmPrompt(string caption, string title)
        {
            InitializeComponent();

            lblText.Text = title;
            Text = caption;
        }

        /// <summary>
        /// Confirms the text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, System.EventArgs e)
        {
            ReturnValue = txtElement.Text;
        }

        private void TxtElement_TextChanged(object sender, System.EventArgs e)
        {
            btnSave.Enabled = (txtElement.Text != string.Empty);
        }
    }
}
